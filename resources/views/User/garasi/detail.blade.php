<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="page-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-sm-12 md-12">
                                                                    <div class="float-left">
                                                                        <a href="{{route('garasi')}}"> <i class="ti-angle-left"></i>Kembali ke garasi</a>
                                                                    </div>
                                                                    <div class="float-right">
                                                                        <a href="#" type="button" onclick="hapusMobil({{$kendaraan->id}})" class="btn waves-effect waves-light btn-danger btn-outline-danger mb-4 float-right"><i class="ti-trash"></i>Hapus Mobil</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-6 col-12">
                                                                    <img src="{{url('model-mobil/'.$kendaraan->models->photo)}}" style="height: 200px; width: 210px;" alt="user image" class="img-radius img-110 align-top m-r-25">
                                                                </div>
                                                                <div class="col-lg-3 col-md-6 col-12">
                                                                    @if(!$kendaraan->oli_mesin)
                                                                        <div class="canvas-wrap">
                                                                            <canvas id="canvas_{{$kendaraan->id}}" data-id="{{$kendaraan->id}}"  class="canvas" width="300" height="200"></canvas>
                                                                            <span id="procent_{{$kendaraan->id}}" data-id="{{$kendaraan->id}}" data-oli="0" class="procent"></span>
                                                                        </div>
                                                                    @else
                                                                        <div class="canvas-wrap">
                                                                            <canvas id="canvas_{{$kendaraan->id}}" data-id="{{$kendaraan->id}}" class="canvas" width="300" height="200"></canvas>
                                                                            <span id="procent_{{$kendaraan->id}}" data-id="{{$kendaraan->id}}" data-oli="1" class="procent"></span>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-5 col-md-12 col-12 pt-4">
                                                                    <div class="align-middle" >
                                                                        <p style="font-size: 16px; line-height:0.25px">{{$kendaraan->tahun_keluar}}</p>
                                                                        <h3> {{$kendaraan->merks->nama_merk}} {{$kendaraan->models->nama_model}}</h3>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <!-- Row start -->
                                                            <div class="row m-b-50">
                                                                <div class="col-lg-4 col-xl-6">
                                                                    <ul class="nav nav-tabs  tabs" role="tablist">
                                                                        <li class="nav-item">
                                                                            <a class="nav-link active" data-toggle="tab" href="#home1" role="tab"><i class="ti-clipboard mr-2"></i>Informasi Mobil</a>
                                                                        </li>
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" data-toggle="tab" href="#profile1" role="tab"><i class="ti-pencil-alt mr-2"></i>Update Informasi Mobil</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="row m-b-50">
                                                                
                                                            <div class="col-lg-12 col-xl-12" >
                                                                <!-- Tab panes -->
                                                                <div class="tab-content card-block" style="margin-top: -5%">
                                                                    <div class="tab-pane active" id="home1" role="tabpanel">
                                                                        <div class="row mb-3">
                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                                                <p class="card-text font-weight-bold">Plat Nomor</p>
                                                                            </div>
                                                                            <div class="col-1 d-none d-md-block d-lg-block">
                                                                            :
                                                                            </div>
                                                                            <div class="col-8">
                                                                            <p class="card-text text-primary">{{$kendaraan->plat_nomor}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                                                <p class="card-text font-weight-bold">Kilometer</p>
                                                                            </div>
                                                                            <div class="col-1 d-none d-md-block d-lg-block">
                                                                            :
                                                                            </div>
                                                                            <div class="col-8">
                                                                            <p class="card-text text-primary"> {{$kendaraan->kilometer}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                                                <p class="card-text font-weight-bold">Bahan Bakar</p>
                                                                            </div>
                                                                            <div class="col-1 d-none d-md-block d-lg-block">
                                                                            :
                                                                            </div>
                                                                            <div class="col-8">
                                                                            <p class="card-text text-primary"> {{$kendaraan->bahan_bakar}}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                                                <p class="card-text font-weight-bold">Oli Mesin</p>
                                                                            </div>
                                                                            <div class="col-1 d-none d-md-block d-lg-block">
                                                                            :
                                                                            </div>
                                                                            <div class="col-8">
                                                                            <p class="card-text text-primary"> {{$kendaraan->oli_mesin}}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="tab-pane" id="profile1" role="tabpanel">
                                                                        <form action="{{route('garasi.updatedata', $kendaraan->id)}}" method="POST" enctype="multipart/form-data">
                                                                            @csrf
                                                                            @method('patch')
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Kilometer</label>
                                                                                <div class="col-sm-10">
                                                                                    <input type="text" name="kilometer" value="{{$kendaraan->kilometer}}" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Bahan Bakar</label>
                                                                                <div class="col-sm-10">
                                                                                    <select name="bahan_bakar" class="form-control">
                                                                                        <option value="opt1">Pilih Bahan Bakar</option>
                                                                                        <option value="Pertamina Premium">Pertamina Premium</option>
                                                                                        <option value="Pertamina Pertalite">Pertamina Pertalite</option>
                                                                                        <option value="Pertamina Pertamax">Pertamina Pertamax</option>
                                                                                        <option value="Pertamina Solar">Pertamina Solar</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Oli Mesin</label>
                                                                                <div class="col-sm-10">
                                                                                    <select name="oli_mesin" class="form-control">
                                                                                        <option value="opt1">Pilih Oli Mesin</option>
                                                                                        <option value="Regular">Reguler</option>
                                                                                        <option value="Sintetik">Sintetik</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            {{-- <div class="form-group row">
                                                                                <label class="col-sm-2 col-form-label">Transmisi</label>
                                                                                <div class="col-sm-10">
                                                                                    <select name="transmisi" class="form-control">
                                                                                        <option value="opt1">Pilih Transmisi</option>
                                                                                        <option value="Otomatic">Otomatic</option>
                                                                                        <option value="Manual">Manual</option>
                                                                                        <option value="Hybrid">Hybrid</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div> --}}
                                                                            <button type="submit" class="btn waves-effect waves-light btn-primary  mb-4 float-right">Simpan Perubahan</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // hapusmobil
    const hapusMobil = (id) => {
        Swal.fire({
            title: 'Apakah anda yakin menghapus kendaraan?',
             icon: 'question',
            showCancelButton: true,
            confirmButtonText: `Hapus`,
            cancelButtonText: `Tidak`,
        }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    var url = '{{ url("garasi/delete") }}/'+id;
                    $.ajax({
                        url: url,
                        type: "GET",
                        success: function () {
                            Swal.fire(
                                'Good job!',
                                'You clicked the button!',
                                'success'
                            ).then(window.location = "{{route('garasi')}}");
                            
                        },
                        error: function (err) {
                            console.log(err);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            });
                        }
                    });
                }
            });
    };
</script>
{{-- progresbar circle --}}
<script>
    window.onload = function() {
        $('.procent').each(function(){
            let id = $(this).data('id');
            let oli = $(this).data('oli');
            let amount = null;
            if(oli == "1"){
                amount = 100;
            }else{
                amount = 35;
            }

            console.log(id);
            var can = document.getElementById('canvas_'+id),
                    spanProcent = document.getElementById('procent_'+id),
                    c = can.getContext('2d');
                
                var posX = can.width / 2,
                    posY = can.height / 2,
                    fps = 1000 / 200,
                    procent = 0,
                    oneProcent = 360 / 100,
                    result = oneProcent * amount;
                
                c.lineCap = 'round';
                arcMove();
                
                function arcMove(){
                    var deegres = 0;
                    var acrInterval = setInterval (function() {
                    deegres += 1;
                    c.clearRect( 0, 0, can.width, can.height );
                    procent = deegres / oneProcent;

                    spanProcent.innerHTML = procent.toFixed();

                    c.beginPath();
                    c.arc( posX, posY, 70, (Math.PI/180) * 270, (Math.PI/180) * (270 + 360) );
                    c.strokeStyle = '#edeef3';
                    c.lineWidth = '10';
                    c.stroke();

                    c.beginPath();
                    c.strokeStyle = '#049dff';
                    c.lineWidth = '10';
                    c.arc( posX, posY, 70, (Math.PI/180) * 270, (Math.PI/180) * (270 + deegres) );
                    c.stroke();
                    if( deegres >= result ) clearInterval(acrInterval);
                    }, fps);
                    
                }
        });
    }
    </script>
</body>

</html>
