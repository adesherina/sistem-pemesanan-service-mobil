<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
    
</head>
  <body>
    <!-- Pre-loader start -->
    {{-- <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="col-sm-12 md-12">
                                                                <div class="float-left">
                                                                    <h4>Tambah Mobil</h4>
                                                                </div>
                                                                <div class="float-right">
                                                                    <a href="{{route('garasi')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary btn-sm mb-4 float-right">Kembali</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                             <form action="{{route('garasi.simpandata')}}" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('post')
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Merk Mobil</label>
                                                                    <div class="col-sm-10">
                                                                        @php
                                                                        $merk = DB::table('merk_kendaraan')->get();    
                                                                        @endphp
                                                                        <select name="merk_id" id="merk-select" class="form-control">
                                                                            <option selected>Pilih Merek</option>
                                                                            @foreach ($merk as $item)
                                                                                <option value="{{$item->id}}">{{$item->nama_merk}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        @if ($errors->has('merk_id'))
                                                                        <span class="text-danger">{{ $errors->first('merk_id') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Model Mobil</label>
                                                                    <div class="col-sm-10">
                                                                        @php
                                                                        $model = DB::table('model_kendaraan')->get();    
                                                                        @endphp
                                                                        <select name="model_id" id="model-select" class="form-control">
                                                                            <option selected>Pilih Model</option>
                                                                        </select>
                                                                        @if ($errors->has('model_id'))
                                                                        <span class="text-danger">{{ $errors->first('model_id') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Tahun Keluar</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="tahun_keluar" class="form-control">
                                                                        @if ($errors->has('tahun_keluar'))
                                                                        <span class="text-danger">{{ $errors->first('tahun_keluar') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-2 col-form-label">Plat Nomor</label>
                                                                    <div class="col-sm-10">
                                                                        <input type="text" name="plat_nomor" class="form-control">
                                                                        @if ($errors->has('plat_nomor'))
                                                                        <span class="text-danger">{{ $errors->first('plat_nomor') }}</span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-4 float-right">Simpan</button>
                                                             </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Add car model from merk id
    $('#merk-select').on('change', function(){
        var merk_id = $(this).val();
        var url = '{{ route("garasi.getmodel") }}';

        $.ajax({
            url: url,
            type: "POST",
            data:{
                merk_id:merk_id
            },
            success: function (data) {
                console.log(data);
                if(data.status == 200){
                    printModel(data.data);
                }else if(data.status == 404){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Model Tidak Tersedia',
                    });
                    printModel(data.data);
                }
            },
            error: function (error) {
                console.log(error);
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Error',
                });
            }
        });
    });

    function printModel(arr){
        let parseArr = JSON.parse(arr);
        let options = ``;
        jQuery.each(parseArr, function(){
            options += `<option value="${this.id}">${this.nama_model}</option>`;
        });
        $("#model-select").html(options);
    }   
</script>
</body>

</html>
