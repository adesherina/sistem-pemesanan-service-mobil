<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="page-body">
                                            @if (session()->has('status1'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status') }}
                                                </div>
                                            @elseif(session()->has('status2'))
                                                <div class="alert bg-danger text-white alert-danger" role="alert">
                                                    {{ Session::get('status2') }}
                                                </div>
                                            @endif
                                            <div class="row">
                                                @if($kendaraan->isEmpty())
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="card-header-right">
                                                                <ul class="list-unstyled card-option">
                                                                    <li><i class="icofont icofont-simple-left "></i></li>
                                                                    <li><i class="icofont icofont-maximize full-card"></i></li>
                                                                    <li><i class="icofont icofont-minus minimize-card"></i></li>
                                                                    <li><i class="icofont icofont-refresh reload-card"></i></li>
                                                                    <li><i class="icofont icofont-error close-card"></i></li>
                                                                </ul>
                                                            </div>
                                                            {{-- <hr> --}}
                                                        </div>
                                                        
                                                        <div class="card-block">
                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                <img src="{{url('assets/images/car.svg')}}" class="mb-3" style="width: 100px; height:100px;">
                                                            </div>
                                                            <p class="m-0 text-center">Halaman ini berisi daftar mobil anda</p>
                                                            <p class="m-0 text-center">Tambah Mobil Sekarang</p>
                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                <a href="{{route('garasi.tambahdata')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 mt-3 text-center"><i class="fa fa-plus"></i>Service Sekarang</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                <div class="col-sm-12 md-12">
                                                    <div class="float-left">
                                                        <h3>Garasi</h3>
                                                    </div>
                                                    <div class="float-right">
                                                        <a href="{{url('garasi/tambah')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-4 float-right"><i class="ti-plus"></i>Tambah Mobil</a>
                                                    </div>
                                                </div>
                                                @foreach ($kendaraan as $item)
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="card-header-right">
                                                                <ul class="list-unstyled card-option">
                                                                    <li><i class="icofont icofont-simple-left "></i></li>
                                                                    <li><i class="icofont icofont-maximize full-card"></i></li>
                                                                    <li><i class="icofont icofont-minus minimize-card"></i></li>
                                                                    <li><i class="icofont icofont-refresh reload-card"></i></li>
                                                                    <li><i class="icofont icofont-error close-card"></i></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="row">
                                                                <div class="col-lg-3 col-md-6 col-12">
                                                                    <img src="{{url('model-mobil/'.$item->models->photo)}}" style="height: 200px; width: 210px;" alt="user image" class="img-radius img-110 align-top m-r-25">
                                                                    <a href="{{route('garasi.detaildata', $item->id)}}" style="border-radius:50px; width:180px" class="btn btn-outline-primary btn-sm ml-3 mt-3 mb-3">Lihat Informasi</a>
                                                                </div>
                                                                <div class="col-lg-3 col-md-6 col-12">
                                                                    @if(!$item->oli_mesin)
                                                                        <div class="canvas-wrap">
                                                                            <canvas id="canvas_{{$item->id}}" data-id="{{$item->id}}"  class="canvas" width="300" height="200"></canvas>
                                                                            <span id="procent_{{$item->id}}" data-id="{{$item->id}}" data-oli="0" class="procent"></span>
                                                                        </div>
                                                                    @else
                                                                        <div class="canvas-wrap">
                                                                            <canvas id="canvas_{{$item->id}}" data-id="{{$item->id}}" class="canvas" width="300" height="200"></canvas>
                                                                            <span id="procent_{{$item->id}}" data-id="{{$item->id}}" data-oli="1" class="procent"></span>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="col-lg-5 col-md-12 col-12 pt-4">
                                                                    <div class="align-middle" >
                                                                        <p style="font-size: 16px; line-height:0.25px">{{$item->tahun_keluar}}</p>
                                                                        <h3>{{$item->merks->nama_merk}} {{$item->models->nama_model}}</h3>
                                                                    </div>
                                                                    <div class="card mt-4">
                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-6">
                                                                                    <p>Oli Mesin</p>
                                                                                </div>
                                                                                <div class="col-6 text-right font-weight-bold">
                                                                                    <p>
                                                                                        @if(!$item->oli_mesin)
                                                                                        Belum Melengkapi
                                                                                        @else
                                                                                            {{$item->oli_mesin}}
                                                                                        @endif
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-6">                
                                                                                    <p>Total Kilometer</p>
                                                                                </div>
                                                                                <div class="col-6 text-right font-weight-bold">
                                                                                    <p>
                                                                                        @if(!$item->kilometer)
                                                                                        0
                                                                                        @else
                                                                                            {{$item->kilometer}}
                                                                                        @endif
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-6">                
                                                                                    <p>Bahan Bakar</p>
                                                                                </div>
                                                                                <div class="col-6 text-right font-weight-bold">
                                                                                    @if(!$item->bahan_bakar)
                                                                                        Belum Melengkapi
                                                                                        @else
                                                                                            {{$item->bahan_bakar}}
                                                                                        @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')

<script>
window.onload = function() {
    $('.procent').each(function(){
        let id = $(this).data('id');
        let oli = $(this).data('oli');
        let amount = null;
        if(oli == "1"){
            amount = 100;
        }else{
            amount = 35;
        }

        console.log(id);
          var can = document.getElementById('canvas_'+id),
                spanProcent = document.getElementById('procent_'+id),
                c = can.getContext('2d');
            
            var posX = can.width / 2,
                posY = can.height / 2,
                fps = 1000 / 200,
                procent = 0,
                oneProcent = 360 / 100,
                result = oneProcent * amount;
            
            c.lineCap = 'round';
            arcMove();
            
            function arcMove(){
                var deegres = 0;
                var acrInterval = setInterval (function() {
                deegres += 1;
                c.clearRect( 0, 0, can.width, can.height );
                procent = deegres / oneProcent;

                spanProcent.innerHTML = procent.toFixed();

                c.beginPath();
                c.arc( posX, posY, 70, (Math.PI/180) * 270, (Math.PI/180) * (270 + 360) );
                c.strokeStyle = '#edeef3';
                c.lineWidth = '10';
                c.stroke();

                c.beginPath();
                c.strokeStyle = '#049dff';
                c.lineWidth = '10';
                c.arc( posX, posY, 70, (Math.PI/180) * 270, (Math.PI/180) * (270 + deegres) );
                c.stroke();
                if( deegres >= result ) clearInterval(acrInterval);
                }, fps);
                
            }
    });  
}
</script>
</body>

</html>
