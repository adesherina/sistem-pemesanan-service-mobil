<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')

    <style>
        .figure-modal{
            float: left;
            border: solid 1px #e4e8ec;
            border-radius: 2px;
            width: 70px;
            height: 70px;
            padding: 5px;
            margin: 15px 15px 15px 5px;
        }

        .figure-modal img{
                display: block;
                max-width: 100%;
                height: auto;
        }
        .part-details h3{
            font-size: 13.5px;
            font-weight: 600;
            color: #373842;
            line-height: 15px;
            margin: 16px 12px 11px 0;
            letter-spacing: .2px;
        }
        .part-details p{
            font-size: 12px;
            line-height: 1.42;
            color: #868792;
        }
        .price{
            font-size: 13.5px;
            font-weight: 600;
            color: #303549;
            line-height: 15px;
            text-align: center;
        }

        .btn-chart-detail{
            margin: 20px auto 0;
            width: 110px;
            height: 27px;
            border: solid 1px #dfe0e6;
            border-radius: 2px;
            text-align: center;
            font-size: 12px;
            font-weight: 600;
            line-height: 12px;
            padding: 1px 0 0;
            display: block;
            letter-spacing: normal;
            box-shadow: 0 0 0 2px #ff8519;
            animation: orange-outline-pulses 1.4s infinite linear;
        }
        .new-booked-product{
            border-top: solid 1px #dfdfe6;
            margin: 0;
            padding: 16px 5px 0;
            background-color: #fff;
        }
        .new-booked-product h6{
            font-size: 12px;
            font-weight: 600;
            line-height: 15px;
            color: #373842;
            margin: 0;
        }
        .new-booked-product .remove{
                display: block;
                float: right;
                font-size: 12px;
                font-weight: 600;
                color: #ec5e5e;
                line-height: 12px;
                padding: 2px 5px;
        }

        .booked-part{
            margin: 8px 0 16px;
        }

        .booked-part .price{
                float: right;
                padding-right: 5px;
                font-size: 12px;
                font-weight: 600;
                color: #373842;
                line-height: 15px;
        }

        .booked-part .name{
            margin-right: 90px;
            font-size: 12px;
            font-weight: normal;
            color: #868792;
            line-height: 15px;
            word-wrap: break-word;
        }
    </style>
</head>
  <body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                            <div class="row">
                                                <div class="col-lg-8 col-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <h4>Pilih Mobil Anda</h4>
                                                            <hr>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label mb-2">Pilih Mobil</label>
                                                                <div class="col-sm-8">
                                                                    @php
                                                                    $kendaraan = App\Model\Kendaraan::with('models')->where('user_id', Session::get('id'))->get(); 
                                                                    @endphp
                                                                    <select name="kendaraan_id" onchange="getValueKendaraan()" id="merk-select" class="form-control">
                                                                        <option selected>Pilih Model Mobil</option>
                                                                        @foreach ($kendaraan as $item)
                                                                            <option value="{{$item->id}}">{{$item->merks->nama_merk}} {{$item->models->nama_model}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <h4>Pilih Layanan</h4>
                                                            <hr>
                                                            <div class="col-lg-12 col-xl-12">
                                                                <!-- Nav tabs -->
                                                                <ul class="nav nav-tabs md-tabs tabs-left b-none" role="tablist">
                                                                    @foreach ($kategori_jasa as $kategori)
                                                                        <li class="nav-item">
                                                                            <a class="nav-link" onclick="getJasa({{$kategori->id}})" data-toggle="tab" href="#service_{{$kategori->id}}" role="tab"><small>{{$kategori->nama_kategori}}</small></a>
                                                                            <div class="slide"></div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                                <!-- Tab panes -->
                                                                <div class="tab-content tabs-left-content card-block col-lg-8 col-xl-10">
                                                                    @foreach ($kategori_jasa as $kategori2)
                                                                    <div class="tab-pane" id="service_{{$kategori2->id}}" role="tabpanel">
                                                                        <div class="row">
                                                                            <div class="table-responsive ">
                                                                                <table class="table">
                                                                                    <tbody id="table_service_{{$kategori2->id}}">
                                                                                       
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    @endforeach
                                                                    {{-- form --}}
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="card pesanan-card">
                                                        <div class="card-header text-white" style="padding: 10px 15px">
                                                            <h5 class="text-white">Daftar Pesanan</h5>
                                                            <div>
                                                                <small class="text-white" id="cart-kendaraan">Pilih Kendaraan</small>
                                                            </div>
                                                        </div>
                                                        <div class="card-body px-3 py-3" id="card-cart" style="padding:0">
                                                            {{-- <div class="text-center">
                                                                <img src="{{url('assets/custom-icon/shopping-cart.svg')}}" style="max-width: 45px" alt="" class="img-fluid">
                                                                <div class="mt-2">
                                                                    <small class="text-dark" style="font-size: 12px !important">Keranjang masihi kosong</small>
                                                                </div>
                                                            </div> --}}
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="new-booked-product d-none" id="div-total">
                                                                <div class="booked-part mb-2">
                                                                    <div class="price" id="total-text"></div>
                                                                    <div class="name" >Total</div>
                                                                </div>
                                                                <div id="btn-trans-div" class="d-none new-booked-product px-4 my-4">
                                                                    <form action="{{route('user.services.save')}}" method="POST" enctype="multipart/form-data" class="">
                                                                        @csrf
                                                                        <input type="hidden" name="kendaraan" id="kendaraan-inp">
                                                                        <input type="hidden" name="value" id="value-inp">
                                                                        <input type="submit" class="btn-trans" value="Lanjutkan Pesanan">
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        <!-- Modal -->
        <div class="modal fade" id="sparepartModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header" id="modal-header-sparepart" style="padding: 0; border:none">
                        
                    </div>
                    <div class="modal-body" id="alert-sparepart">

                    </div>
                    <div class="modal-body px-4" id="modal-body-sparepart">
                        
                    </div>
                    <div class="modal-footer" style="border: none">
                        <a href="#" class="add-cart-btn" data-dismiss="modal" onclick="closeSparepartModal()" aria-label="Close" style="border-radius: 20px; padding:10px 20px; background-color:#fb6f34">Tutup</a>
                        <a href="#" class="add-cart-btn" onclick="saveSparepartModal()" style="border-radius: 20px; padding:10px 20px">Pesan Layanan ini</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Required Jquery -->
@include('User.layout.js')

<script>

    const getValueKendaraan = () => {
        let getText = $('#merk-select').find("option:selected").text();
        let getValue = $('#merk-select').find("option:selected").val();

        $('#cart-kendaraan').text(getText);
        $('#kendaraan-inp').val(getValue);
    }

    let cart_items = [];

    function addCart(id){
        event.preventDefault();

        let nama = $('#jasa_button_'+id).data('nama');
        let harga = $('#jasa_button_'+id).data('harga');
        let button = '';

        let json_value = `{"id":"${id}", "nama_jasa":"${nama}", "sparepart_id":"null", "sparepart_name":"null", "harga_jasa":"${harga}"}`;

        cart_items.push(json_value);
       
        update_cart(cart_items);

        if(!$("#jasa_button_"+id).hasClass("selected")){
            button = `<a href="#" class="add-cart-btn float-right selected" data-nama="${nama}" data-harga="${harga}" onclick="removeCart(${id});" id="jasa_button_${id}">Batal</a>`;
            document.getElementById('button_jasa_'+id).innerHTML = button;
        }

    }

    // Hapus keranjang
    function removeCart(id){
        event.preventDefault();

        let nama = $('#jasa_button_'+id).data('nama');
        let harga = $('#jasa_button_'+id).data('harga');
        let search = ``;
        cart_items.forEach(function (items, index){
            let item = JSON.parse(items);
            if( id  == item.id){
                cart_items.splice(index, 1);
                search = JSON.stringify(item);
            };
        });

        console.log(search);
        search = JSON.parse(search);
        string_search = JSON.stringify(search)
        update_cart(cart_items);
        if(search.sparepart_id != 'null'){
            if($("#jasa_button_"+id).hasClass("selected")){
                button = `<a href="#" class="d-inline add-cart-btn float-right" id="jasa_button_${search.id}" data-nama="${search.nama_jasa}" data-harga="${search.harga_jasa}" onclick='getSparepart(${string_search})'>Tambahkan</a>`;
                document.getElementById('button_jasa_'+id).innerHTML = button;
            }
        }else{
            if($("#jasa_button_"+id).hasClass("selected")){
                button = `<a href="#" class="add-cart-btn float-right" data-nama="${nama}" data-harga="${harga}" onclick="addCart('${id}')" id="jasa_button_${id}">Tambahkan</a>`;
                document.getElementById('button_jasa_'+id).innerHTML = button;
            }
        }
        
    }

    function update_cart(values){
        let html = ``;
        let total = 0;
        
        if(values.length != 0){
            // jika keranjang tidak kosong
            values.forEach(function (items, index){
                let item = JSON.parse(items);
                total += parseInt(item.harga_jasa);
                if(item.sparepart_id != "null"){
                    html += `
                        <div class="new-booked-product">
                            <a href="javascript:void(0)" class="remove fas fa-trash" onclick="removeCart(${item.id});" data-product-id="${item.id}"></a>
                            <h6>${item.nama_jasa}</h6>
                            <div class="booked-part">
                                <div class="price">Rp ${item.harga_jasa}</div>
                                <div class="name">${item.sparepart_name}</div>
                            </div>
                        </div>
                    `;
                }else{
                    html += `
                        <div class="new-booked-product">
                            <a href="javascript:void(0)" class="remove fas fa-trash" onclick="removeCart(${item.id});" data-product-id="${item.id}"></a>
                            <h6>${item.nama_jasa}</h6>
                            <div class="booked-part">
                                <div class="price">Rp ${item.harga_jasa}</div>
                                <div class="name">harga</div>
                            </div>
                        </div>
                    `;
                }
            });

            $('#div-total').removeClass('d-none');
            $('#btn-trans-div').removeClass('d-none');
        }else{
            //jika keranjang kosong
            html = `
                <div class="text-center">
                    <img src="{{url('assets/custom-icon/shopping-cart.svg')}}" style="max-width: 45px" alt="" class="img-fluid">
                    <div class="mt-2">
                        <small class="text-dark" style="font-size: 12px !important">Keranjang masih kosong</small>
                    </div>
                </div>
            `;

            $('#div-total').addClass('d-none');
            $('#btn-trans-div').addClass('d-none');
        }

        $json_cart = JSON.stringify(values);
        $('#value-inp').val($json_cart);        
        $('#total-text').text('Rp.'+total);
        
        document.getElementById('card-cart').innerHTML = html;
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Add car model from merk id
    const getJasa = (id) => {
        var url = '{{ route("services.getservice") }}';

        $.ajax({
            url: url,
            type: "POST",
            data:{
                kategori_id:id
            },
            success: function (data) {
                if(data.status == 200){
                    printJasa(data.data, id);
                }else if(data.status == 404){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Jasa Tidak Tersedia',
                    });
                    printJasa(data.data, id);
                }
            },
            error: function (error) {
                // Swal.fire({
                //     icon: 'error',
                //     title: 'Oops...',
                //     text: 'Error',
                // });
            }
        });
    };

    function printJasa(arr, kategori_id){
        
        let parseArr = JSON.parse(arr);
        let html = ``;
        if(cart_items.length > 0){
            cart_items.forEach(function (item, index){
                item = JSON.parse(item);
                jQuery.each(parseArr, function(){
                    let jsonParse = JSON.stringify(this);
                    if( this.id  == item.id){
                        
                        html += `
                            <tr>
                                <td><small class="font-weight-bold">${this.nama_jasa}</small> </td>
                                <td>
                                    <div id="button_jasa_${this.id}" data-id="${this.id}">
                                        <a href="#" class="add-cart-btn float-right selected" data-nama="${this.nama_jasa}" data-harga="${this.harga_jasa}" onclick="removeCart(${this.id});" id="jasa_button_${this.id}">Batal</a>
                                    </div>
                                </td>
                            </tr>
                        `;
                    }else{
                        // cek jika ada sparepart pada jasa
                        if(this.is_sparepart == 0){
                            html += `
                                <tr>
                                    <td><small class="font-weight-bold">${this.nama_jasa}</small> </td>
                                    <td>
                                        <div id="button_jasa_${this.id}" data-id="${this.id}">
                                            <a href="#" class="d-inline add-cart-btn float-right" id="jasa_button_${this.id}" data-nama="${this.nama_jasa}" data-harga="${this.harga_jasa}" onclick="addCart(${this.id})" >Tambahkan</a>
                                        </div>
                                    </td>
                                </tr>
                            `;
                        }else{
                            html += `
                                <tr>
                                    <td><small class="font-weight-bold">${this.nama_jasa}</small> </td>
                                    <td>
                                        <div id="button_jasa_${this.id}" data-id="${this.id}">
                                            <a href="#" class="d-inline add-cart-btn float-right" id="jasa_button_${this.id}" data-nama="${this.nama_jasa}" data-harga="${this.harga_jasa}" onclick='getSparepart(${jsonParse})'>Tambahkan</a>
                                        </div>
                                    </td>
                                </tr>
                            `;
                        }
                    };
                });
            });
        }else{
            // Jika baru pertama kali get Jasa
            jQuery.each(parseArr, function(){
                // cek sparepart 

                let jsonParse = JSON.stringify(this);
                if(this.is_sparepart == 0){
                    html += `
                        <tr>
                            <td><small class="font-weight-bold">${this.nama_jasa}</small> </td>
                            <td>
                                <div id="button_jasa_${this.id}" data-id="${this.id}">
                                    <a href="#" class="d-inline add-cart-btn float-right" id="jasa_button_${this.id}" data-nama="${this.nama_jasa}" data-harga="${this.harga_jasa}" onclick="addCart(${this.id})" >Tambahkan</a>
                                </div>
                            </td>
                        </tr>
                    `;
                }else{
                    html += `
                        <tr>
                            <td><small class="font-weight-bold">${this.nama_jasa}</small> </td>
                            <td>
                                <div id="button_jasa_${this.id}" data-id="${this.id}">
                                    <a href="#" class="d-inline add-cart-btn float-right" id="jasa_button_${this.id}" data-nama="${this.nama_jasa}" data-harga="${this.harga_jasa}" onclick='getSparepart(${jsonParse})' >Tambahkan</a>
                                </div>
                            </td>
                        </tr>
                    `;
                }
            });
        }
        
        document.getElementById('table_service_'+kategori_id).innerHTML = html;
    }

    // ================== sparepart ================

    let json_sparepart = ``;

    const getSparepart = (item) => {
        var url = '{{ route("services.sparepart") }}';

        $.ajax({
            url: url,
            type: "POST",
            data:{
                jasa_id:item.id
            },
            success: function (data) {
                if(data.status == 200){
                    console.log(data.data);
                    printSparepart(data.data, item);
                }else if(data.status == 404){
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Model Tidak Tersedia',
                    });
                    printSparepart(data.data, item);
                }
            },
            error: function (error) {
            }
        });
    }

    const printSparepart = (spareparts, jasas) => {
        // event.preventDefault();
        $('#alert-sparepart').html('');
        $('#sparepartModal').modal('show');

        let sparepart = JSON.parse(spareparts);
        let jasaJson = JSON.stringify(jasas);
        let html = ``;

        jQuery.each(sparepart, function(){
            let sparepartString = JSON.stringify(this);
            let url_photo = "{{url('foto-sparepart/')}}/"+this.photo;
            html += `
                <div class="row ">
                    <div class="col-9 border">
                        <figure class="figure-modal">
                            <img src="${url_photo}" class="img-responsive">
                        </figure>
                        <div class="part-details">
                            <h3>${this.nama}</h3>
                            <p>stok ${this.stok}</p>
                        </div>
                    </div>
                    <div class="col-3 border">
                        <div class="price mt-4">Rp ${this.harga_jual}</div>
                        <div id="sparepart-divbutton__${this.id}">
                            <a href="#" class="sparepart-btn mt-3" id="sparepart-button__${this.id}" onclick='addSparepart(${sparepartString}, ${jasaJson})'>Pilih</a>
                        </div>
                    </div>
                </div>
            `;
        });
    

        let header = `
            <img src="{{url('assets/images/2.png')}}" style="width: 100%;
                object-fit: cover;
                object-position: center;" class="img-fluid" alt="" style="min-height:100px">
            <h3 id="modal-title-sparepart" class="font-weight-bold text-white" style="position: absolute;left: 30px;
            ">${jasas.nama_jasa}</h3>
        `;

        document.getElementById('modal-header-sparepart').innerHTML = header;
        document.getElementById('modal-body-sparepart').innerHTML = '';
        document.getElementById('modal-body-sparepart').innerHTML = html;
    }   

    // tambah sparepart ke JSON
    function addSparepart(spareparts, jasas){
        if(spareparts.stok > 0){
            let nama_jasa = jasas.nama_jasa;
            let harga_jasa = jasas.harga_jasa;
            let sparepart_id = spareparts.id;
            let html = '';
            let jsonJasa = JSON.stringify(jasas);

            let total_harga = parseInt(harga_jasa) + parseInt(spareparts.harga_jual);
            

            json_sparepart = `{"id":"${jasas.id}", "nama_jasa":"${nama_jasa}", "sparepart_name":"${spareparts.nama}", "sparepart_id":"${sparepart_id}", "harga_jasa":"${total_harga}"}`;
        
            let url_photo = "{{url('foto-sparepart/')}}/"+spareparts.photo;
            
            html = `
                <div class="row ">
                    <div class="col-9 border">
                        <figure class="figure-modal">
                            <img src="${url_photo}" class="img-responsive">
                        </figure>
                        <div class="part-details">
                            <h3>${spareparts.nama}</h3>
                            <p>stok ${spareparts.stok}</p>
                        </div>
                    </div>
                    <div class="col-3 border">
                        <div class="price mt-4">Rp ${spareparts.harga_jual}</div>
                        <div id="sparepart-divbutton__${spareparts.id}">
                            <a href="#" class="sparepart-btn mt-3" id="sparepart-button__${spareparts.id}" onclick='getSparepart(${jsonJasa})'>hapus</a>
                        </div>
                    </div>
                </div>
            `;

            document.getElementById('modal-body-sparepart').innerHTML = html;
        }else{
            let alert = `
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Upps, </strong> Stok Habis.
                </div>
            `;
            
            $('#alert-sparepart').html(alert);

        }
        
    }

    const saveSparepartModal = () => {
        $('#sparepartModal').modal('hide');

        cart_items.push(json_sparepart);
        let jsonParse = JSON.parse(json_sparepart);
        update_cart(cart_items);

        json_sparepart = ``;

        button = `<a href="#" class="add-cart-btn float-right selected" data-nama="${jsonParse.nama_jasa}"  onclick="removeCart(${jsonParse.id});" id="jasa_button_${jsonParse.id}">Batal</a>`;
        document.getElementById('button_jasa_'+jsonParse.id).innerHTML = button;
        
    }

    const closeSparepartModal = () => {
        $('#sparepartModal').modal('hide');
        json_sparepart = ``;
    }


</script>
</body>

</html>
