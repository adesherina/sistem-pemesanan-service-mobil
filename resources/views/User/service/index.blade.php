<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Material tab card start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <!-- Row start -->
                                                        <div class="row m-b-50">
                                                            <div class="col-lg-4 col-xl-6">
                                                                <ul class="nav nav-tabs  tabs" role="tablist">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"><i class="ti-clipboard mr-3"></i>Booking Service</a>
                                                                        
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"><i class="ti-receipt mr-3"></i>Riwayat Service</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row m-b-50">
                                                            <div class="col-lg-12 col-xl-12">
                                                                    <!-- Tab panes -->
                                                                    <div class="tab-content card-block" style="margin-top: -5%;" >
                                                                        
                                                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                                                        @if($service->isEmpty())
                                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                                <img src="{{url('assets/images/calendar.svg')}}" class="mb-3" style="width: 100px; height:100px;">
                                                                            </div>
                                                                            <p class="m-0 text-center">Halaman ini berisi Booking Service anda</p>
                                                                            <p class="m-0 text-center"> Pesan Sekarang!</p>
                                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                                <a href="{{route('services.tambah')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 mt-3 text-center"><i class="fa fa-plus"></i>Service Sekarang</a>
                                                                            </div>
                                                                        @else
                                                                            <div class="row">
                                                                                <div class="col-lg-6 col-12">
                                                                                    <h5 class="font-weight-bold float-left">Service Aktif Semua Mobil Anda</h5>
                                                                                </div>
                                                                                <div class="col-lg-6 col-12">
                                                                                    <a href="{{route('services.tambah')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary btn-sm float-right"><i class="fa fa-plus"></i>Service Sekarang</a>
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                            <div class="d-block ">
                                                                                @foreach ($service as $item)
                                                                                <a href="{{route('checkout.detail', $item->kd_service)}}" style="text-decoration: none; color: inherit;">
                                                                                    <div class="service-div mt-3 px-3 py-3">
                                                                                    <p class="font-weight-bold">{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</p>
                                                                                    <p class="font-weight-bold">Kode Pesanan {{$item->kd_service}}</p>
                                                                                    <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                                                        <div class="vertical-timeline-item vertical-timeline-element">
                                                                                            <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                                <div class="vertical-timeline-element-content bounce-in">
                                                                                                    <h4 class="timeline-title">Anda memesan {{$item->detailServiceRef->count()}} layanan untuk mobil {{$item->kendaraanRef->merks->nama_merk}} {{$item->kendaraanRef->models->nama_model}}</h4>
                                                                                                    <p class="font-weight-bold">Jenis Layanan : <br>
                                                                                                        @foreach ($item->detailServiceRef as $detail)
                                                                                                            <span class="text-primary">
                                                                                                                {{$detail->jasaRef->nama_jasa}}
                                                                                                            </span><br>
                                                                                                        @endforeach
                                                                                                        </p>
                                                                                                    <span class="vertical-timeline-element-date"><img src="{{url('assets/images/user.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="vertical-timeline-item vertical-timeline-element">
                                                                                            <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                                <div class="vertical-timeline-element-content bounce-in">
                                                                                                @if($item->status == "0")
                                                                                                    <h4 class="timeline-title">Kami akan segera menghubungi Anda untuk mengkonfirmasikan pemesanan service Anda</h4>
                                                                                                @elseif($item->status == '1')
                                                                                                    <h4 class="timeline-title">Pemesanan service anda telah terkonfirmasi harap datang ketika mendekati tiga antrian</h4>
                                                                                                @elseif($item->status == '2')
                                                                                                    <h4 class="timeline-title">Mobil anda berhasil di service</h4>
                                                                                                @endif
                                                                                                <span class="vertical-timeline-element-date"><img src="{{url('assets/images/admin.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>          
                                                                                    </div>
                                                                                </div>
                                                                                </a>
                                                                                <hr>
                                                                            @endforeach
                                                                            </div>
                                                                        @endif
                                                                        </div>
                                                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                                                        @if($riwayat->isEmpty())
                                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                                <img src="{{url('assets/images/history.svg')}}" class="mb-3" style="width: 100px; height:100px;">
                                                                            </div>
                                                                            <p class="m-0 text-center">Halaman ini berisi riwayat service anda</p>
                                                                            <p class="m-0 text-center">Belum Ada riwayat. Pesan Sekarang!</p>
                                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                                <a href="{{route('services.tambah')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 mt-3 text-center"><i class="fa fa-plus"></i>Service Sekarang</a>
                                                                            </div>
                                                                        @else
                                                                            <h5 class="font-weight-bold">Riwayat Service Mobil Anda</h5>
                                                                            <hr>
                                                                            
                                                                            @foreach ($riwayat as $item2)
                                                                                <a href="{{route('checkout.detail', $item2->kd_service)}}" style="text-decoration: none; color: inherit;">
                                                                                    <div class="service-div mt-3 px-3 py-3">
                                                                                    <p class="font-weight-bold">{{$item2->created_at->isoFormat('dddd, D MMMM Y')}}</p>
                                                                                    <p class="font-weight-bold">Kode Pesanan {{$item2->kd_service}}</p>
                                                                                    <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                                                        <div class="vertical-timeline-item vertical-timeline-element">
                                                                                            <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                                <div class="vertical-timeline-element-content bounce-in">
                                                                                                    <h4 class="timeline-title">Anda memesan {{$item2->detailServiceRef->count()}} layanan untuk mobil {{$item2->kendaraanRef->merks->nama_merk}} {{$item2->kendaraanRef->models->nama_model}}</h4>
                                                                                                    <p class="font-weight-bold">Jenis Layanan : <br>
                                                                                                        @php
                                                                                                            $total_estimasi = 0;
                                                                                                        @endphp
                                                                                                        @foreach ($item2->detailServiceRef as $detail2)
                                                                                                        @php
                                                                                                            $total_estimasi += (int)$detail2->jasaRef->estimasi;
                                                                                                        @endphp
                                                                                                            <span class="text-primary">
                                                                                                                {{$detail2->jasaRef->nama_jasa}}
                                                                                                            </span><br>
                                                                                                        @endforeach
                                                                                                        
                                                                                                        @foreach ($item2->detailServiceRef as $item)
                                                                                                            @if ($item->sparepartRef != null)
                                                                                                                <span class="text-secondary" style="font-size: 10px;">{{$item->sparepartRef->nama}}</span>
                                                                                                            @endif
                                                                                                        @endforeach 
                                                                                                    </p>
                                                                                                    <p class="font-weight-bold">Total Waktu Estimasi Servis<br>    
                                                                                                        <span class="text-primary">
                                                                                                            @php
                                                                                                                $hours = floor($total_estimasi / 60);
                                                                                                                $minutes = $total_estimasi % 60;
                                                                                                            @endphp                                             
                                                                                                            {{$hours . " jam ".$minutes." menit"}}
                                                                                                        </span><br>
                                                                                                    </p>
                                                                                                    <span class="vertical-timeline-element-date"><img src="{{url('assets/images/user.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="vertical-timeline-item vertical-timeline-element">
                                                                                            <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                                <div class="vertical-timeline-element-content bounce-in">
                                                                                                @if($item2->status == "0")
                                                                                                    <h4 class="timeline-title">Mobil Anda Sedang Dalam Antrian</h4>
                                                                                                @elseif($item2->status == "1" )
                                                                                                    <h4 class="timeline-title">Service Telah Terkonfirmasi. Harap datang ke bengkel ketika mendekati tiga antrian</h4>
                                                                                                @elseif($item2->status == "2" )
                                                                                                    <h4 class="timeline-title">Mobil Anda Sedang di Service</h4>
                                                                                                @elseif($item2->status == "3")
                                                                                                    <h4 class="timeline-title">Mobil Anda Telah Selesai di Service</h4>
                                                                                                @endif
                                                                                                <span class="vertical-timeline-element-date"><img src="{{url('assets/images/admin.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>          
                                                                                    </div>
                                                                                </div>
                                                                                </a>
                                                                                <hr>
                                                                            @endforeach
                                                                        @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')

<script>
    $('.service-div').each(function(){
        $(this).hover(function(){
            $(this).css("border", "1px solid #e0e0e0");
        }, function(){
            $(this).css("border", "none");
        });
    });
</script>
</body>

</html>
