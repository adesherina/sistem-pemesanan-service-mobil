<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="container-fluid mt-100 mb-100">
                                                <div id="ui-view">
                                                    <div>
                                                        <div class="card">
                                                            <div class="card-header"> Invoice : <strong>  {{$invoice->kd_service}}</strong>
                                                                <div class="pull-right float-right"> 
                                                                    <a class="btn btn-sm btn-info" href="{{route('chechout.cetak.invoice', $invoice->kd_service)}}" data-abc="true"><i class="ti-download"></i>Unduh</a> 
                                                                </div>
                                                            </div>
                                                            <div class="card-body">
                                                                <div class="row mb-4">
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-3">Dari :</h6>
                                                                        <div><strong>Bengkel Tiga Sudara.</strong></div>
                                                                        <div>Bangkaloa Ilir, Widasari</div>
                                                                        <div>Indramayu, 45271</div>
                                                                        <div>Email: bengkeltigasaudara@gmail.com</div>
                                                                        <div>Nomor Telepon: 081222045689</div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-3">Untuk:</h6>
                                                                        <div><strong>Nama :{{$invoice->userRef->nama_lengkap}}</strong></div>
                                                                        <div>Nomer Telepon: {{$invoice->userRef->no_telp}}</div>
                                                                    </div>
                                                                    <div class="col-sm-4">
                                                                        <h6 class="mb-3">Detail :</h6>
                                                                        <div>Invoice<strong> {{$invoice->kd_service}}</strong></div>
                                                                        <div>{{$invoice->created_at->isoFormat('dddd, D MMMM Y')}}</div>
                                                                        <div><strong>Nota: {{$invoice->pembayaranRef->nota}}</strong></div>
                                                                    </div>
                                                                </div>
                                                                <div class="table-responsive-sm">
                                                                    <table class="table table-striped">
                                                                        <thead>
                                                                            <tr>
                                                                                <th class="center">#</th>
                                                                                <th>Layanan</th>
                                                                                <th class="center">Jasa</th>
                                                                                <th>Sparepart</th>
                                                                                <th>Qty</th>
                                                                                <th class="right">Harga Sparepart</th>
                                                                                <th class="right">subtotal</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @foreach ($invoice->detailServiceRef as $item)
                                                                            <tr>
                                                                                <td class="center">{{$loop->iteration}}</td>
                                                                                <td class="left">{{$item->jasaRef->nama_jasa}}</td>
                                                                                <td class="right">Rp. {{number_format($item->jasaRef->harga_jasa,'0','.','.')}}</td>
                                                                                <td class="left"> 
                                                                                    @if ($item->sparepartRef != null)
                                                                                        {{$item->sparepartRef->nama}}
                                                                                    @else
                                                                                    -
                                                                                    @endif
                                                                                </td>
                                                                                <td>
                                                                                    @if ($item->sparepartRef != null)
                                                                                    1
                                                                                    @else
                                                                                    -
                                                                                    @endif
                                                                                </td>
                                                                                <td class="center">
                                                                                    @if ($item->sparepartRef != null)
                                                                                    Rp. {{number_format($item->sparepartRef->harga_jual,'0','.','.')}}
                                                                                    @else
                                                                                    -
                                                                                    @endif
                                                                                </td>
                                                                                <td class="right">Rp.{{number_format($item->subtotal,'0','.','.')}}</td>
                                                                            </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-lg-6 col-sm-5">
                                                                       <small class="font-weight-bold">
                                                                        Note : <br>
                                                                        Untuk Harga Jasa Service sewaktu - waktu bisa berubah.
                                                                        </small>
                                                                    </div>
                                                                    <div class="col-lg-4 col-sm-5 ml-auto">
                                                                        <table class="table table-clear">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="left"><strong>Total</strong></td>
                                                                                    <td class="right"><strong>Rp.{{number_format($invoice->pembayaranRef->total,'0','.','.')}}</strong></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
</body>

</html>
