<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Material tab card start -->
                                                <div class="card">
                                                    <div class="card-block">
                                                        <!-- Row start -->
                                                        <div class="row m-b-50">
                                                            <div class="col-lg-4 col-xl-6">
                                                                <ul class="nav nav-tabs  tabs" role="tablist">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link active" data-toggle="tab" href="#home3" role="tab"><i class="ti-clipboard mr-3"></i>Pesanan Aktif</a>
                                                                        
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" data-toggle="tab" href="#profile3" role="tab"><i class="ti-receipt mr-3"></i>Riwayat Service</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="row m-b-50">
                                                            <div class="col-lg-12 col-xl-12">
                                                                <a href="" id="pesanan-aktif-anchor" style="text-decoration: none !important">
                                                                        <!-- Tab panes -->
                                                                    <div class="tab-content card-block" style="margin-top: -5%; ">
                                                                        <div class="tab-pane active" id="home3" role="tabpanel">
                                                                            <h5 class="font-weight-bold">Pesanan Aktif Semua Mobil Anda</h5>
                                                                            <hr>
                                                                            <p class="font-weight-bold">05 Juni 2021</p>
                                                                            <p class="font-weight-bold">Pesanan No.8039WEb</p>
                                                                            <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                                                <div class="vertical-timeline-item vertical-timeline-element">
                                                                                    <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                                            <h4 class="timeline-title">Anda memesan 1 layanan untuk mobil 2017 Toyota Alphard GRAND NEW 2500cc Automatic</h4>
                                                                                            <p class="font-weight-bold">Jenis Layanan : <br><span class="text-primary">Ganti Oli</span><br><span class="text-primary">Tone Up Mesin</span></p>
                                                                                             <span class="vertical-timeline-element-date"><img src="{{url('assets/images/user.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="vertical-timeline-item vertical-timeline-element">
                                                                                    <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                                        <h4 class="timeline-title">Kami akan segera menghubungi Anda untuk mengkonfirmasikan pesanan Anda</h4>
                                                                                        <span class="vertical-timeline-element-date"><img src="{{url('assets/images/admin.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>          
                                                                            </div>
                                                                        </div>
                                                                        <div class="tab-pane" id="profile3" role="tabpanel">
                                                                            <h5 class="font-weight-bold">Pesanan Aktif Semua Mobil Anda</h5>
                                                                            <hr>
                                                                            <p class="font-weight-bold">05 Juni 2021</p>
                                                                            <p class="font-weight-bold">Pesanan No.8039WEb</p>
                                                                            <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                                                <div class="vertical-timeline-item vertical-timeline-element">
                                                                                    <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                                            <h4 class="timeline-title">Anda memesan 2 layanan untuk mobil 2014 Honda City 1500cc Automatic</h4>
                                                                                            <p class="font-weight-bold">Jenis Layanan : <br><span class="text-primary">Ganti Oli</span></p>
                                                                                             <span class="vertical-timeline-element-date"><img src="{{url('assets/images/user.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="vertical-timeline-item vertical-timeline-element">
                                                                                    <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                                        <div class="vertical-timeline-element-content bounce-in">
                                                                                        <h4 class="timeline-title">Anda membatalkan service ini</h4>
                                                                                            {{-- <p>Lorem</p>  --}}
                                                                                            <span class="vertical-timeline-element-date"><img src="{{url('assets/images/admin.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>          
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
</body>

</html>
