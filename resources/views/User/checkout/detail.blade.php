<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12 md-12">
                                                <div class="float-left">
                                                    <a href="{{route('services')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-4 float-right"> <i class="ti-angle-left"></i>Kembali ke Daftar Pesanan</a>
                                                </div>
                                                <div class="float-right">
                                                    @if($detail->status == "0")
                                                        <a href="#" onclick="hapusService('{{$detail->kd_service}}')" class="btn waves-effect waves-light btn-danger btn-outline-danger mb-4 float-right"><i class="ti-close mr-2"></i>Batalkan Pesanan</a>
                                                    @elseif($detail->status == "1")
                                                    
                                                    @elseif($detail->status == "3")
                                                    <a href="{{route('checkout.invoice', $detail->kd_service)}}" class="btn waves-effect waves-light btn-info btn-outline-info mb-4 float-right"><i class="ti-clipboard mr-2"></i>Cek Invoice</a>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <h5 class="font-weight-bold">Detail Service Mobil {{$detail->kendaraanRef->merks->nama_merk}} {{$detail->kendaraanRef->models->nama_model}}</h5>
                                                        <hr>
                                                        <p class="font-weight-bold">{{$detail->created_at->isoFormat('dddd, D MMMM Y')}}</p>
                                                        <p class="font-weight-bold">Kode Pesanan {{$detail->kd_service}}</p>
                                                        <div class="vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                                                            <div class="vertical-timeline-item vertical-timeline-element">
                                                                <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                    <div class="vertical-timeline-element-content bounce-in">
                                                                        <h4 class="timeline-title mb-3">Anda Memesan {{$detail->detailServiceRef->count()}} Layanan untuk mobil {{$detail->kendaraanRef->merks->nama_merk}} {{$detail->kendaraanRef->models->nama_model}}</h4>
                                                                        <table class="table table-hover table-bordered col-md-6 tabel-responsive">
                                                                            <tbody>
                                                                                <tr style="background-color:#6ba9ff;" class="text-white">
                                                                                    <td>
                                                                                      <p class="font-weight-bold text-center mt-2">Daftar Pesanan <br> {{$detail->kendaraanRef->merks->nama_merk}} {{$detail->kendaraanRef->models->nama_model}}</p> 
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="font-weight-bold">Total Harga <br>
                                                                                            @php
                                                                                                $total = 0;
                                                                                            @endphp
                                                                                            @foreach ($detail->detailServiceRef as $item)
                                                                                                    @php
                                                                                                        
                                                                                                        $total += (int)$item->subtotal ;
                                                                                                    @endphp
                                                                                            @endforeach
                                                                                            <span class="text-primary">
                                                                                                Rp. {{number_format($total,'0','.','.')}}
                                                                                            </span><br>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="font-weight-bold">Jenis Layanan <br>
                                                                                            @php
                                                                                                $total_estimasi = 0;
                                                                                            @endphp
                                                                                        @foreach ($detail->detailServiceRef as $item)
                                                                                            @php
                                                                                                $total_estimasi += (int)$item->jasaRef->estimasi;
                                                                                            @endphp
                                                                                            <span class="text-primary">
                                                                                                {{$item->jasaRef->nama_jasa}}
                                                                                            </span><br>
                                                                                            <p>
                                                                                            @if ($item->sparepartRef != null)
                                                                                                <span class="text-secondary" style="font-size: 10px;">{{$item->sparepartRef->nama}}</span>
                                                                                            @endif
                                                                                            </p>
                                                                                        @endforeach
                                                                                        </p>
                                                                                        <p class="font-weight-bold">Waktu Estimasi Servis<br>    
                                                                                            <span class="text-primary">
                                                                                                @php
                                                                                                    $hours = floor($total_estimasi / 60);
                                                                                                    $minutes = $total_estimasi % 60;
                                                                                                @endphp                                             
                                                                                                {{$hours . " jam ".$minutes." menit"}}
                                                                                            </span><br>
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="font-weight-bold">Nomor Antrian : 
                                                                                            @if($detail->antrianRef->status === "Menunggu")
                                                                                                <span class="text-primary">{{$detail->antrianRef->no_antrian}}</span>
                                                                                            @elseif($detail->antrianRef->status === "Proses")
                                                                                                <span class="text-primary">{{$detail->antrianRef->no_antrian}}</span>
                                                                                            @elseif($detail->antrianRef->status === "Selesai")
                                                                                                <span class="text-primary">Selesai</span>
                                                                                            @endif
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <p class="font-weight-bold">Status : 
                                                                                            @if($detail->status == "0")
                                                                                                <span class="text-primary">Mohon Tunggu Konfirmasi dari Admin</span>
                                                                                            @elseif($detail->status == "1")
                                                                                                <span class="text-primary">Pemesanan service anda telah terkonfirmasi </span>
                                                                                            @elseif($detail->status == "2")
                                                                                                <span class="text-primary">Mobil Anda Sedang di Service</span>
                                                                                            @elseif($detail->status == "3")
                                                                                                <span class="text-primary">Mobil Anda Telah Selesai di Service</span>
                                                                                            @endif
                                                                                        </p>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                        <span class="vertical-timeline-element-date"><img src="{{url('assets/images/user.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="vertical-timeline-item vertical-timeline-element">
                                                                <div> <span class="vertical-timeline-element-icon bounce-in"> <i class="badge-timeline"> </i> </span>
                                                                    <div class="vertical-timeline-element-content bounce-in">
                                                                        @if($detail->status == "0")
                                                                            <h4 class="timeline-title">Kami akan segera menghubungi Anda untuk mengkonfirmasikan pemesanan service Anda</h4>
                                                                        @elseif($detail->status == '1')
                                                                            <h4 class="timeline-title">Pemesanan service anda telah terkonfirmasi harap datang ketika mendekati tiga antrian</h4>
                                                                        @elseif($detail->status == '2')
                                                                            <h4 class="timeline-title">Mobil Anda Sedang di Service </h4>
                                                                        @elseif($detail->status == '3')
                                                                            <h4 class="timeline-title">Mobil anda berhasil di service</h4>
                                                                        @endif
                                                                        <span class="vertical-timeline-element-date"><img src="{{url('assets/images/admin.svg')}}" alt="" style="width: 25px; height:25px;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    const hapusService = (kd_service) => {
        Swal.fire({
            title: 'Apakah anda yakin batalkan pesanan?',
             icon: 'question',
            showCancelButton: true,
            confirmButtonText: `Ya`,
            cancelButtonText: `Tidak`,
        }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    var url = '{{ url("checkout/delete") }}/'+kd_service;
                    $.ajax({
                        url: url,
                        type: "GET",
                        success: function () {
                            Swal.fire(
                                'Berhasil di batalkan!',
                            ).then(window.location = "{{route('services')}}");
                            
                        },
                        error: function (err) {
                            console.log(err);
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            });
                        }
                    });
                }
            });
    };
</script>
</body>

</html>
