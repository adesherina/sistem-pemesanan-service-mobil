<!DOCTYPE html>
<html lang="en">
    
<head>
    @include('User.layout.head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .figure-modal{
            float: left;
            border: solid 1px #e4e8ec;
            border-radius: 2px;
            width: 70px;
            height: 70px;
            padding: 5px;
            margin: 15px 15px 15px 5px;
        }

        .figure-modal img{
                display: block;
                max-width: 100%;
                height: auto;
        }
        .part-details h3{
            font-size: 13.5px;
            font-weight: 600;
            color: #373842;
            line-height: 15px;
            margin: 16px 12px 11px 0;
            letter-spacing: .2px;
        }
        .part-details p{
            font-size: 12px;
            line-height: 1.42;
            color: #868792;
        }
        .price{
            font-size: 13.5px;
            font-weight: 600;
            color: #303549;
            line-height: 15px;
            text-align: center;
        }

        .btn-chart-detail{
            margin: 20px auto 0;
            width: 110px;
            height: 27px;
            border: solid 1px #dfe0e6;
            border-radius: 2px;
            text-align: center;
            font-size: 12px;
            font-weight: 600;
            line-height: 12px;
            padding: 1px 0 0;
            display: block;
            letter-spacing: normal;
            box-shadow: 0 0 0 2px #ff8519;
            animation: orange-outline-pulses 1.4s infinite linear;
        }
        .new-booked-product{
            border-top: solid 1px #dfdfe6;
            margin: 0;
            padding: 16px 5px 0;
            background-color: #fff;
        }
        .new-booked-product h6{
            font-size: 12px;
            font-weight: 600;
            line-height: 15px;
            color: #373842;
            margin: 0;
        }
        .new-booked-product .remove{
                display: block;
                float: right;
                font-size: 12px;
                font-weight: 600;
                color: #ec5e5e;
                line-height: 12px;
                padding: 2px 5px;
        }

        .booked-part{
            margin: 8px 0 16px;
        }

        .booked-part .price{
                float: right;
                padding-right: 5px;
                font-size: 12px;
                font-weight: 600;
                color: #373842;
                line-height: 15px;
        }

        .booked-part .name{
            margin-right: 90px;
            font-size: 12px;
            font-weight: normal;
            color: #868792;
            line-height: 15px;
            word-wrap: break-word;
        }
    </style>
</head>
  <body>
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                            <div class="row">
                                                <div class="col-lg-8 col-12">
                                                    <div class="card">
                                                        <div class="card-block">
                                                            <h4>Pilih Mobil Anda</h4>
                                                            <hr>
                                                            <div class="form-group row">
                                                                <label class="col-sm-4 col-form-label mb-2">Pilih Mobil</label>
                                                                <div class="col-sm-8">
                                                                    <select name="select" disabled class="form-control">
                                                                        <option value="opt1">{{$kendaraan->models->nama_model}}</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                     <div class="card mt-3">
                                                        <div class="card-block">
                                                            <h4>Tentukan Tanggal Service Anda</h4>
                                                            <hr>
                                                            <form action="{{route('checkout.save', $kd_service)}}" method="POST">
                                                                @csrf
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Tanggal Service</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" autocomplete="off" name="tanggal" id="tanggal-inp" class="form-control">
                                                                    </div>
                                                                </div>
                                                        </div>
                                                    </div>

                                                    <div class="card mt-3">
                                                        <div class="card-block">
                                                            <h4>Informasi Kontak</h4>
                                                            <hr>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Nama Lengkap</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" name="nama" value="{{$user->nama_lengkap}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Nomor Telepon</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="text" name="no_hp" value="{{$user->no_telp}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Catatan Tambahan</label>
                                                                    <div class="col-sm-8">
                                                                        <textarea rows="5" name="catatan" cols="5" class="form-control" placeholder="Default textarea"></textarea>
                                                                    </div>
                                                                </div>
                                                                <input type="submit" disabled id="submit-antrian" class="btn waves-effect waves-light btn-primary  float-right" value="Service Sekarang">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4 col-md-4 col-12">
                                                    <div class="card pesanan-card">
                                                        <div class="card-header text-white" style="padding: 10px 15px">
                                                            <h5 class="text-white">Daftar Pesanan</h5>
                                                            <div>
                                                                <small class="text-white">{{$kendaraan->models->nama_model}}</small>
                                                            </div>
                                                        </div>
                                                        <div class="card-body px-3 py-3" id="card-cart" style="padding:0">
                                                            <div class="daftar-pesanan">
                                                                @php
                                                                    $values = json_decode($tmp->value);
                                                                    $total_harga = 0;
                                                                @endphp
                                                                @foreach ($values as $value)
                                                                    @php
                                                                        $decodeValue = json_decode($value);
                                                                        $total_harga += (int)$decodeValue->harga_jasa;
                                                                    @endphp
                                                                    <div class="new-booked-product">
                                                                        <a href="#" class="remove far fa-check-circle" data-product-id=""></a>
                                                                        <h6>{{$decodeValue->nama_jasa}}</h6>
                                                                        <div class="booked-part">
                                                                            <div class="price">Rp {{$decodeValue->harga_jasa}}</div>
                                                                            @if ($decodeValue->sparepart_id != "null")
                                                                                <div class="name">{{$decodeValue->sparepart_name}}</div>
                                                                            @else
                                                                                <div class="name">harga</div>
                                                                            @endif
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                                <div class="new-booked-product">
                                                                    <div class="booked-part">
                                                                        <div class="price">Rp {{$total_harga}}</div>
                                                                        <div class="name"><h6> Total Harga</h6></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!-- Required Jquery -->
@include('User.layout.js')

{{-- <script src="https://unpkg.com/@develoka/angka-terbilang-js/index.min.js"> --}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>

     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#tanggal-inp').datepicker({
        startDate: "now()",
        todayBtn: "linked",
        format: "yyyy-mm-dd",
    });


    $('#tanggal-inp').on('change', function(){
        let tanggal = $(this).val();
        console.log(tanggal);
        if(tanggal){
            getAntrian(tanggal);
        }
    });


    //get antrian
    const getAntrian = (tanggal) => {
        var url = '{{ route("checkout.getantrian") }}';
        let kd_service = "{{$kd_service}}";
        $.ajax({
            url: url,
            type: "POST",
            data:{
                kd_service:kd_service,
                tanggal:tanggal
            },
            success: function (data) {
                if(data.antrian < 9 ){
                    Swal.fire({
                        icon: 'success',
                        title: data.antrian,
                        text: 'Nomor antrian Anda',
                    });
                    $('#submit-antrian').prop('disabled', false);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oppss',
                        text: 'Antrian sudah penuh, silahkan pilih hari lain',
                    });
                    $('#submit-antrian').prop('disabled', true);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
</script>
</body>

</html>
