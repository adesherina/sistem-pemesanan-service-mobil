<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>

<body>

<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-12">
                <div class="invoice-title">
                    <h2>Bengkel Mobil Tiga Saudara</h2>
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="invoice-title2" >
                    <p class="text-right">Nota : {{$invoice->pembayaranRef->nota}}</p>
                </div>
				<div class="invoice-title2 " >
                    <p class="text-right"><strong>Nama : {{$invoice->userRef->nama_lengkap}}</strong></p>
                </div>
                <div style="invoice-title2 ">
                    <p class="text-right">Tanggal Pemesanan: {{$invoice->created_at->isoFormat('dddd, D MMMM Y')}}</p>
                </div>
            </div>
        </div>
        
        <div class="row mt-2">
            <div class="col-xl-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p class="panel-title"><strong>Detail Pemesanan</strong></p>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
										<td ><strong>No</strong></td>
                                        <td ><strong>Layanan</strong></td>
                                        <td ><strong>Harga</strong></td>
                                        <td ><strong>Sparepart</strong></td>
										<td ><strong>Qty</strong></td>
                                        <td ><strong>Harga</strong></td>
                                        <td ><strong>Total</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
									@foreach ($invoice->detailServiceRef as $item)
                                    <tr>
										<td style="font-size: 15px;">{{$loop->iteration}}</td>
                                        <td style="font-size: 15px;">{{$item->jasaRef->nama_jasa}}</td>
                                        <td style="font-size: 15px;">Rp. {{number_format($item->jasaRef->harga_jasa,'0','.','.')}}</td>
                                        <td style="font-size: 12px;">
											@if ($item->sparepartRef != null)
												{{$item->sparepartRef->nama}}
											@else
											-
											@endif
										</td>
										<td style="font-size: 15px;">
											@if ($item->sparepartRef != null)
											1
											@else
											-
											@endif
										</td>
										<td style="font-size: 15px;">
										@if ($item->sparepartRef != null)
										Rp. {{number_format($item->sparepartRef->harga_jual,'0','.','.')}}
										@else
										-
										@endif
										</td>
                                        <td  style="font-size: 15px;">Rp.{{number_format($item->subtotal,'0','.','.')}}</td>
                                    </tr>
									@endforeach
                                    <tr>
										<td class="thick-line"></td>
										<td class="thick-line"></td>
										<td class="thick-line"></td>
										<td class="thick-line"></td>
										<td class="thick-line"></td>
										<td class="thick-line text-center"><strong>Total</strong></td>
										{{-- @php                                              
											$total =  ;
										@endphp --}}
										<td class="thick-line"> Rp. {{number_format($invoice->pembayaranRef->total,'0','.','.')}}</td>
    								</tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <script src="{{url('assets/bootstrap/js/bootstrap.min.js')}}"></script> -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>