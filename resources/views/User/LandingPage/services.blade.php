
<!DOCTYPE html>
<html lang="en">
  <head>
@include('User.LandingPage.layouts.head')
  </head>
  <body>
{{-- NAVBAR --}}
@include('User.LandingPage.layouts.navbar')
    <!-- END nav -->
   <section class="hero-wrap hero-wrap-2" style="background-image: url('/assets/landingPage/images/mesin2.jpeg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Services <i class="fa fa-chevron-right"></i></span></p>
            <h1 class="mb-0 bread">Services</h1>
          </div>
        </div>
      </div>
    </section>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5 mb-3">
          <div class="col-md-7 heading-section text-center ftco-animate">
          	<span class="subheading">Kami Menawarkan Beberapa </span>
					<h2>Layanan Service</h2>
          </div>
        </div>
<div class="row">
				<div class="col-md-6 services ftco-animate">
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-car-service"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Oli</h3>
						<p>Ganti oli adalah servis rutin terpenting untuk setiap mobil. Oli mesin berfungsi sebagai pelumas untuk semua komponen didalam mesin.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>
					</div>
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-battery"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Aki</h3>
						<p>Aki mobil Anda adalah sumber daya listrik utama untuk pengoperasian seluruh komponen elektrikal mobil seperti: starter, radio, lampu dll. Pada saat mobil diperasikan, alternator akan mengisi atau mencharge tenaga aki agar aki mobil bisa bekerja dengan baik.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>
					</div>
				</div>
				<div class="col-md-6 services ftco-animate">
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-car-engine"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Kampas Rem</h3>
						<p>Kampas rem adalah plat yang digesekkan terhadap piringan rem pada saat rem diaplikasikan, sehingga kecepatan mobil melambat.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>
					</div> 
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-repair"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Tone Up Mesin</h3>
						<p>Kampas rem adalah plat yang digesekkan terhadap piringan rem pada saat rem diaplikasikan, sehingga kecepatan mobil melambat.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>   
				</div>
        	</div>
    	</div>
        <div class="row mt-4 mt-md-5 justify-content-between">
        	<div class="col-md-7 ftco-animate">
				<h5>JELASKAN KEBUTUHAN ANDA</h5>
        	<p>
			1. Masukan informasi mobil Anda bedasarkan merek, tahun, dan tipe
			</p>
			<p>
			2. Pilih jenis servis dan sparepart yang Anda butuhkan
			</p>
			3.Berikan informasi Anda (nama, lokasi, nomor telpon, dll)<p>
			</p>	
			</div>
        	<div class="col-md-4 ftco-animate">
        		<h5 class="font-weight-bold">Layanan Kami</h5>
                  <ul class="services-2">
                    <li><span class="fa fa-check"></span>Ganti Oli</li>
                    <li><span class="fa fa-check"></span>Tone Up</li>
                    <li><span class="fa fa-check"></span>Service Rem</li>
                    <li><span class="fa fa-check"></span>Ganti Kampas Rem</li>
                  </ul>
        	</div>
        </div>
    	</div>
    </section>



  {{-- FOOTER --}}
    @include('User.LandingPage.layouts.footer')
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


{{-- JS --}}
    @include('User.LandingPage.layouts.js')
  </body>
</html>