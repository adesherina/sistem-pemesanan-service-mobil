<div class="wrap">
	    <div class="container">
				<div class="row justify-content-between">
					<div class="col-md-3 d-flex align-items-center">
						<a class="navbar-brand" href="index.html">Bengkel Tiga Saudara<span>.</span></a>
					</div>
					<div class="col-md-7">
						<div class="row">
							<div class="col">
								<div class="top-wrap d-flex">
									<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-location-arrow"></span></div>
									<div class="text"><span>Alamat </span><span>Jl.Bangkaloa Ilir - Cantung</span></div>
								</div>
							</div>
							<div class="col">
								<div class="top-wrap d-flex">
									<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-location-arrow"></span></div>
									<div class="text"><span>Telepon</span><span>081222045689</span></div>
								</div>
							</div>
							{{-- <div class="col-md-3 d-flex justify-content-end align-items-center">
								<div class="social-media">
					    		<p class="mb-0 d-flex">
					    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-facebook"><i class="sr-only">Facebook</i></span></a>
					    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-twitter"><i class="sr-only">Twitter</i></span></a>
					    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-instagram"><i class="sr-only">Instagram</i></span></a>
					    			<a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a>
					    		</p>
				        		</div>
							</div> --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	    
	      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="fa fa-bars"></span> Menu
	      </button>
		<form action="#" class="searchform order-lg-last">
        	<div class="form-group d-flex">
				<a href="{{url('register')}}" class="btn btn-primary mr-3">Register</a>
				<a href="{{url('login')}}" class="btn btn-primary">Login</a>
			</div>
        </form>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav mr-auto">
	        	<li class="nav-item"><a href="{{url('/')}}" class="nav-link">Home</a></li>
	        	<li class="nav-item"><a href="{{url('tentangKami')}}" class="nav-link">Tentang Kami</a></li>
	        	<li class="nav-item"><a href="{{route('landing.service')}}" class="nav-link">Services</a></li>
	          <li class="nav-item"><a href="{{url('kontak')}}" class="nav-link">Kontak</a></li>
	        </ul>
	      </div>
	    </div>
	  </nav>