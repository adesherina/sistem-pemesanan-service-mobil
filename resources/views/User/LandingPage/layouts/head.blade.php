<title>Bengkel Tiga Sudara</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
 
    <link rel="stylesheet" href="{{url('assets/landingPage/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{url('assets/landingPage/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/landingPage/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{url('assets/landingPage/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{url('assets/landingPage/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('assets/landingPage/css/jquery.timepicker.css')}}">

    <link rel="stylesheet" href="{{url('assets/landingPage/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{url('assets/landingPage/css/style.css')}}">