<footer class="footer ftco-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6 col-lg">
            <div class="ftco-footer-widget mb-4">
              <h2 class="logo"><a href="#">Bengkel Tiga Saudara<span>.</span></a></h2>
              <p>Bengkel Tiga Saudara melayani berbagai macam service mobil yang sesuai dengan kebutuhan anda.</p>
              {{-- <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-4">
                <li class="ftco-animate"><a href="#"><span class="fa fa-twitter"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="fa fa-facebook"></span></a></li>
                <li class="ftco-animate"><a href="#"><span class="fa fa-instagram"></span></a></li>
              </ul> --}}
            </div>
          </div>
          <div class="col-md-6 col-lg">
            <div class="ftco-footer-widget mb-4 ml-md-5">
              <h2 class="ftco-heading-2">Services</h2>
              <ul class="list-unstyled">
                <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Ganti Oli</a></li>
                <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Tone Up</a></li>
                <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Kuras Air Radiator</a></li>
                <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Ganti Oli Transmisi</a></li>
                <li><a href="#" class="py-1 d-block"><span class="fa fa-check mr-3"></span>Kuras Minyak Rem</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-6 col-lg">
            <div class="ftco-footer-widget mb-4">
            	<h2 class="ftco-heading-2">Informasi Kontak</h2>
            	<div class="block-23 mb-3">
	              <ul>
	                <li><span class="icon fa fa-map-marker"></span><span class="text">Jl.Bangkaloa Ilir - Cantung</span></li>
	                <li><a href="#"><span class="icon fa fa-phone"></span><span class="text">081222045689</span></a></li>
	              </ul>
	            </div>
            </div>
          </div>
          <div class="col-md-6 col-lg">
             <div class="ftco-footer-widget mb-4">
              <h2 class="ftco-heading-2">Jam Buka</h2>
              <div class="opening-hours">
              	<h4>Buka Pada Hari:</h4>
              	<p class="pl-3">
              		<span>Senin – Jumat : 08.00 - 17.00</span>
              		<span>Sabtu - Minggu : 08.30 - 16.00</span>
              	</p>
              	{{-- <h4>Vacations:</h4>
              	<p class="pl-3">
              		<span>All Sunday Days</span>
              		<span>All Official Holidays</span>
              	</p> --}}
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 text-center">

            <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Bengkel Tiga Saudara
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          </div>
        </div>
      </div>
    </footer>