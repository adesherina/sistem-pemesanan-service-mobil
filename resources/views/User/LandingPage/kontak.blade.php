
<!DOCTYPE html>
<html lang="en">
  <head>
@include('User.LandingPage.layouts.head')
  </head>
  <body>
{{-- NAVBAR --}}
@include('User.LandingPage.layouts.navbar')
    <!-- END nav -->
<section class="ftco-section bg-light">
    <div class="container">
    	<div class="row justify-content-center">
	    	<div class="col-md-12">
				<div class="wrapper">
		    		<div class="row no-gutters">
			    		<div class="col-md-7 d-flex">
				    		<div class="contact-wrap w-100 p-md-5 p-4">
								@if (session()->has('status'))
								<div class="alert bg-success text-white alert-success" role="alert">
								{{ Session::get('status') }}
								</div>
								@endif
					    	<h3 class="mb-4">Kritik dan Saran</h3>
							<form action="{{route('user.ks.simpan')}}" method="POST" id="contactForm" class="contactForm" enctype="multipart/form-data">
								@method('post')
								@csrf
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
									    	<input type="text" class="form-control" name="nama_lengkap"  placeholder="Nama Lengkap">
										</div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="no_telp"  placeholder="Nomor Telepon ">
										</div>
                                    </div>
									<div class="col-md-12">
										<div class="form-group">
											<textarea name="ks" class="form-control" id="message" cols="30" rows="7" placeholder="Kritik dan Saran"></textarea>
										</div>
									</div>
									<div class="col-md-12">
								    	<div class="form-group">
											<button type="submit" value="Kirim" class="btn btn-primary">Kirim</button>
                                            {{-- <input type="submit" value="Kirim" class="btn btn-primary">
                                            <div class="submitting"></div> --}}
										</div>
									</div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5 d-flex align-items-stretch">
                        <div class="info-wrap bg-primary w-100 p-lg-5 p-4">
                            <h3 class="mb-4 mt-md-4">Contact us</h3>
					        	<div class="dbox w-100 d-flex align-items-start">
					        		<div class="icon d-flex align-items-center justify-content-center">
					        			<span class="fa fa-map-marker"></span>
					        		</div>
					        		<div class="text pl-3">
						            <p><span>Alamat:</span> Jl.Bangkaloa Ilir - Cantung</p>
						          </div>
					            </div>
					        	<div class="dbox w-100 d-flex align-items-center">
					        		<div class="icon d-flex align-items-center justify-content-center">
					        			<span class="fa fa-phone"></span>
					        		</div>
					        		<div class="text pl-3">
						            <p><span>Telepon</span> <a href="tel://1234567920">081222045689</a></p>
						          </div>
					            </div>
					        	<div class="dbox w-100 d-flex align-items-center">
					        		<div class="icon d-flex align-items-center justify-content-center">
					        			<span class="fa fa-paper-plane"></span>
					        		</div>
					        		<div class="text pl-3">
						            <p><span>Email:</span> <a href="mailto:bengkeltigasaudara@gmail.com">bengkeltigasaudara@gmail.com</a></p>
						          </div>
					            </div>
				          </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <iframe class="mt-3" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3964.211117715364!2d108.29490481414042!3d-6.494933265305223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6ec427d58221e9%3A0x586d02ae4e6bb9ac!2sBengkel%20Tiga%20Saudara!5e0!3m2!1sid!2sid!4v1624508976897!5m2!1sid!2sid" width="1100" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
        </div>
    </div>
</section>
  {{-- FOOTER --}}
    @include('User.LandingPage.layouts.footer')
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


{{-- JS --}}
    @include('User.LandingPage.layouts.js')
  </body>
</html>