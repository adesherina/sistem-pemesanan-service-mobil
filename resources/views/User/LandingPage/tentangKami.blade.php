
<!DOCTYPE html>
<html lang="en">
  <head>
@include('User.LandingPage.layouts.head')
  </head>
  <body>
{{-- NAVBAR --}}
@include('User.LandingPage.layouts.navbar')
    <!-- END nav -->
    <section class="hero-wrap hero-wrap-2" style="background-image: url('assets/landingPage/images/mesin2.jpeg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-end">
          <div class="col-md-9 ftco-animate pb-5">
          	<p class="breadcrumbs mb-2"><span class="mr-2"><a href="index.html">Home <i class="fa fa-chevron-right"></i></a></span> <span>Tentang Kami <i class="fa fa-chevron-right"></i></span></p>
            <h1 class="mb-0 bread">Tentag Kami</h1>
          </div>
        </div>
      </div>
    </section>
    <section class="ftco-section ftco-no-pt ftco-no-pb bg-light">
    	<div class="container">
    		<div class="row d-flex no-gutters">
    			<div class="col-md-12 pl-md-5">
    				<div class="row justify-content-start py-5">
		          <div class="col-md-12 heading-section ftco-animate">
		          	<span class="subheading">Bengkel Tiga Saudara</span>
		            <h2 class="mb-4">Melayani Berbagai Macam Service Mobil</h2>
		            <p>
                  Bengkel Tiga Saudara adalah usaha yang bergerak dibidang jasa yaitu perbaikan mobil. Kami Melayani Berbagai Macam
                  Service Mobil.
                <div class="row mt-4 mt-md-5 justify-content-between">
                  <div class="col-md-7 ftco-animate">
                    <h5>JELASKAN KEBUTUHAN ANDA</h5>
                    <p>
                    1. Masukan informasi mobil Anda bedasarkan merek, tahun, dan tipe
                    </p>
                    <p>
                    2. Pilih jenis servis dan sparepart yang Anda butuhkan
                    </p>
                    3.Berikan informasi Anda (nama, lokasi, nomor telpon, dll)<p>
                    </p>	
                </div>
                <div class="col-md-4 ftco-animate">
                  <h5 class="font-weight-bold">Layanan Kami</h5>
                        <ul class="services-2">
                          <li><span class="fa fa-check"></span>Ganti Oli</li>
                          <li><span class="fa fa-check"></span>Tone Up</li>
                          <li><span class="fa fa-check"></span>Service Rem</li>
                          <li><span class="fa fa-check"></span>Ganti Kampas Rem</li>
                        </ul>
                </div>
              </div>
                </p>
                </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>
    <section class="ftco-section testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-center pb-5 mb-3">
          <div class="col-md-7 heading-section heading-section-white text-center ftco-animate">
          	<span class="subheading">Kritik dan Saran</span>
            <h2>Pelanggan</h2>
          </div>
        </div>
        <div class="row ftco-animate">
          <div class="col-md-12">
            <div class="carousel-testimony owl-carousel ftco-owl">
              @foreach ($ks as $item)
              <div class="item">
                <div class="testimony-wrap py-4">
                	<div class="icon d-flex align-items-center justify-content-center"><span class="fa fa-quote-left"></span></div>
                  <div class="text">
                    <p class="mb-4">{{$item->ks}}</p>
                    <div class="d-flex align-items-center">
                    	<div class="pl-3">
		                    <p class="name">{{$item->nama_lengkap}}</p>
		                  </div>
	                  </div>
                  </div>
                </div>
              </div> 
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>


  {{-- FOOTER --}}
    @include('User.LandingPage.layouts.footer')
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


{{-- JS --}}
    @include('User.LandingPage.layouts.js')
  </body>
</html>