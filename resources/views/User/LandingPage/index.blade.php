<!DOCTYPE html>
<html lang="en">
  <head>
    @include('User.LandingPage.layouts.head')
  </head>
  <body>
	@include('User.LandingPage.layouts.navbar')
    <!-- END nav -->
    <div class="hero-wrap">
	    <div class="home-slider owl-carousel">
	      <div class="slider-item" style="background-image:url('assets/landingPage/images/image_3.jpg');">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row no-gutters slider-text align-items-center justify-content-start">
		          <div class="col-md-6 ftco-animate">
		          	<div class="text w-100">
		          		<h2>Bengkel Tiga Saudara</h2>
			            <h1 class="mb-4">Membuat mobil Anda lebih tahan lama</h1>
			            <p><a href="{{url('login')}}" class="btn btn-primary">Service Sekarang</a></p>
		            </div>
		          </div>
		        </div>
	        </div>
	      </div>
	      <div class="slider-item" style="background-image:url('assets/landingPage/images/bg_2.jpg');">
	      	<div class="overlay"></div>
	        <div class="container">
	          <div class="row no-gutters slider-text align-items-center justify-content-start">
		          <div class="col-md-6 ftco-animate">
		          	<div class="text w-100">
		          		<h2>Bengkel Tiga Saudara</h2>
			            <h1 class="mb-4">Membuat mobil Anda lebih tahan lama</h1>
			            <p><a href="#" class="btn btn-primary">Service Sekarang</a></p>
		            </div>
		          </div>
		        </div>
	        </div>
	      </div>
	    </div>
	  </div>
		
		<section class="intro">
			<div class="container intro-wrap">
				<div class="row no-gutters">
					<div class="col-md-12 col-lg-9 bg-intro d-sm-flex align-items-center align-items-stretch">
						<div class="intro-box d-flex align-items-center">
							<div class="icon d-flex align-items-center justify-content-center">
								<i class="flaticon-repair"></i>
							</div>
							<h2 class="mb-0"> Perbaiki Sekarang!</h2>
						</div>
						<a href="{{url('login')}}" class="bg-primary btn-custom d-flex align-items-center"><span>Service Sekarang</span></a>
					</div>
				</div>
			</div>	
		</section>

    <section class="ftco-section">
    	<div class="container">
    		<div class="row justify-content-center pb-5 mb-3">
				<div class="col-md-7 heading-section text-center ftco-animate">
					<span class="subheading">Kami Menawarkan Beberapa </span>
					<h2>Layanan Service</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 services ftco-animate">
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-car-service"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Oli</h3>
						<p>Ganti oli adalah servis rutin terpenting untuk setiap mobil. Oli mesin berfungsi sebagai pelumas untuk semua komponen didalam mesin.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>
					</div>
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-battery"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Aki</h3>
						<p>Aki mobil Anda adalah sumber daya listrik utama untuk pengoperasian seluruh komponen elektrikal mobil seperti: starter, radio, lampu dll. Pada saat mobil diperasikan, alternator akan mengisi atau mencharge tenaga aki agar aki mobil bisa bekerja dengan baik.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service Sekarang</a></p>
					</div>
					</div>
				</div>
				<div class="col-md-6 services ftco-animate">
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-car-engine"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Ganti Kampas Rem</h3>
						<p>Kampas rem adalah plat yang digesekkan terhadap piringan rem pada saat rem diaplikasikan, sehingga kecepatan mobil melambat.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service</a></p>
					</div>
					</div> 
					<div class="d-block d-flex">
					<div class="icon d-flex justify-content-center align-items-center">
							<span class="flaticon-repair"></span>
					</div>
					<div class="media-body pl-3">
						<h3 class="heading">Tone Up Mesin</h3>
						<p>Kampas rem adalah plat yang digesekkan terhadap piringan rem pada saat rem diaplikasikan, sehingga kecepatan mobil melambat.</p>
						<p><a href="{{route('services')}}" class="btn-custom">Service</a></p>
					</div>   
				</div>
        	</div>
    	</div>
    </section>
    {{-- Footer --}}
    @include('User.LandingPage.layouts.footer')
  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


{{-- js     --}}
@include('User.LandingPage.layouts.js')
  </body>
</html>