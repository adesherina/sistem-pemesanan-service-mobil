<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-3">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <img alt="image" src="{{url('user-profil/'. $user->photo)}}"  style="width: 150px;height:150px; border-radius:50%;border:2px">
                                                    </div>
                                                 </div>
                                            </div>
                                            <div class="col-xl-9">
                                                @if (session()->has('statusSuccess1'))
                                                    <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('statusSuccess1') }}
                                                    </div>
                                                    @elseif(session()->has('statusSuccess2'))
                                                    <div class="alert bg-succes text-white alert-succes" role="alert">
                                                    {{ Session::get('statusSuccess2') }}
                                                    </div>
                                                    @endif
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="font-weight-bold m-l-10 mt-3">Profil Saya</h4>
                                                        <hr>
                                                    </div>
                                                    <div class="card-block">
                                                             <form action="{{route('user.update.proses', $user->id)}}" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('patch')
                                                                <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label">Nama Lengkap</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" name="nama_lengkap" value="{{$user->nama_lengkap}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label">Nomor Telepon</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="text" name="no_telp" value="{{$user->no_telp}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label">Jenis Kelamin</label>
                                                                    <div class="form-check col-sm-3 ml-4">
                                                                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios1" <?php if($user->jenis_kelamin == "Laki-laki"){ ?> checked <?php } ?>  value="Laki-laki">
                                                                        <label class="form-check-label" for="exampleRadios1">
                                                                        Laki - Laki
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check col-sm-3">
                                                                        <input class="form-check-input" type="radio" name="jenis_kelamin" id="exampleRadios2" <?php if($user->jenis_kelamin == "Perempuan"){ ?> checked <?php } ?>  value="Perempuan">
                                                                        <label class="form-check-label" for="exampleRadios2">
                                                                        Perempuan
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label">Alamat</label>
                                                                    <div class="col-sm-9">
                                                                        <textarea rows="5" cols="5" name="alamat" class="form-control">{{$user->alamat}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-3 col-form-label">Photo</label>
                                                                    <div class="col-sm-9">
                                                                        <input type="file" class="form-control" value="" name="photo">
                                                                        <input type="hidden" name="oldPhoto" value="{{$user->photo}}">
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn waves-effect waves-light btn-primary  mb-4 float-right">Simpan</button>
                                                             </form>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
</body>

</html>
