<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-xl-3 col-md-3">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <img alt="image" src="{{url('assets/images/user.jpg')}}"  style="width: 150px;height:150px; border-radius:50%;border:2px">
                                                    </div>
                                                 </div>
                                            </div>
                                            <div class="col-xl-9">
                                                     @if (session()->has('editpw'))
                                                    <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('editpw') }}
                                                    </div>
                                                    @elseif(session()->has('failedpw'))
                                                    <div class="alert bg-danger text-white alert-danger" role="alert">
                                                    {{ Session::get('failedpw') }}
                                                    </div>
                                                    @endif
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4 class="font-weight-bold m-l-10 mt-3">Ganti Password</h4>
                                                        <hr>
                                                    </div>
                                                    <div class="card-block">
                                                             <form action="{{route('user.updatePassword', $user->id)}}" method="POST" enctype="multipart/form-data">
                                                                @csrf
                                                                @method('patch')
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Password Baru</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="password" name="newPassword" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group row">
                                                                    <label class="col-sm-4 col-form-label">Konfirmasi Password</label>
                                                                    <div class="col-sm-8">
                                                                        <input type="password" name="confirmPassword" class="form-control">
                                                                    </div>
                                                                </div>
                                                                <button type="submit" class="btn waves-effect waves-light btn-primary  mb-4 float-right">Simpan</button>
                                                             </form>
                                                        </div>
                                                    </div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
</body>

</html>
