<!DOCTYPE html>
<html lang="en">
<head>
    @include('User.layout.head')

    <style>
        #style-7::-webkit-scrollbar-track
        {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
            border-radius: 10px;
        }

        #style-7::-webkit-scrollbar
        {
            width: 10px;
            background-color: #F5F5F5;
        }

        #style-7::-webkit-scrollbar-thumb
        {
            border-radius: 10px;
            background-image: -webkit-gradient(linear,
            left bottom,
            left top,
            color-stop(0.44, rgb(122,153,217)),
            color-stop(0.72, rgb(73,125,189)),
            color-stop(0.86, rgb(28,58,148)));
        }
    </style>
</head>
  <body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">

                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('User.layout.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('User.layout.sidebar')
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header end -->
                                    <div class="page-body">
                                        <div class="row">
                                            @if($service->count() == 0)
                                            <div class="col-md-4">
                                                <div class="card text-center order-visitor-card" style="background-image: url('assets/images/queue.png');background-repeat: no-repeat; background-position: 10px; background-size: 80px 80px;">
                                                    <div class="card-block">
                                                        <h6 class="m-b-0">Antrian Saat Ini</h6>
                                                       
                                                        <h4 class="m-t-15 m-b-15"> @if ($antrian)
                                                            {{$antrian->no_antrian}}
                                                        @else
                                                            0
                                                        @endif</h4>
                                                        <p class="m-b-0">Harap Menunggu</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="card text-center order-visitor-card" id="style-7" style="height:140px; overflow-y:scroll">
                                                    <div class="card-block">
                                                        <div class="gambar1 float-left mb-3">
                                                            <img src="{{url('assets/images/s1.jpg')}}" style="width: 170px; height:90px;">
                                                        </div>
                                                        <div class="isi1 float-right text-left">
                                                            <h4 class="">Tone Up Mobil</h4>
                                                            <p class="">Meningkatkan performa mobil, Harga kompetitif, Respon cepat</p>
                                                            <a href="{{route('services.tambah')}}" class="" style="color: blue; text-decoration:none">Service Sekarang</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($kendaraan->isEmpty())
                                                <div class="col-sm-12">
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <div class="card-header-right">
                                                                <ul class="list-unstyled card-option">
                                                                    <li><i class="icofont icofont-simple-left "></i></li>
                                                                    <li><i class="icofont icofont-maximize full-card"></i></li>
                                                                    <li><i class="icofont icofont-minus minimize-card"></i></li>
                                                                    <li><i class="icofont icofont-refresh reload-card"></i></li>
                                                                    <li><i class="icofont icofont-error close-card"></i></li>
                                                                </ul>
                                                            </div>
                                                            {{-- <hr> --}}
                                                        </div>
                                                        
                                                        <div class="card-block">
                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                <img src="{{url('assets/images/car.svg')}}" class="mb-3" style="width: 100px; height:100px;">
                                                            </div>
                                                            <p class="m-0 text-center">Halaman ini berisi daftar mobil anda</p>
                                                            <p class="m-0 text-center">Tambah Mobil Sekarang </p>
                                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                                                <a href="{{route('garasi.tambahdata')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 mt-3 text-center"><i class="fa fa-plus"></i>Tambah Mobil</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @else
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="card table-card">
                                                        <div class="card-header">
                                                            <h4>Daftar Mobil Anda</h4>
                                                                <div class="card-header-right">
                                                                <ul class="list-unstyled card-option">
                                                                    <li><i class="icofont icofont-simple-left "></i></li>
                                                                    <li><i class="icofont icofont-maximize full-card"></i></li>
                                                                    <li><i class="icofont icofont-minus minimize-card"></i></li>
                                                                    <li><i class="icofont icofont-refresh reload-card"></i></li>
                                                                    <li><i class="icofont icofont-error close-card"></i></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="card-block">
                                                            <div class="table-responsive">
                                                                <table class="table table-hover m-b-0 without-header">
                                                                    <tbody>
                                                                        @foreach ($kendaraan as $item)
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-inline-block align-middle">
                                                                                    <img src="{{url('model-mobil/' .$item->models->photo)}}" alt="user image" class="img-radius img-80 align-top m-r-30">
                                                                                        <div class="d-inline-block">
                                                                                            <h4 class="m-b-1">{{$item->merks->nama_merk}} {{$item->models->nama_model}}</h4>
                                                                                            <h6 class="text-muted m-b-1 m-t-2">{{$item->plat_nomor}}</h6>
                                                                                            <p class="text-muted m-b-1">Service Terakhir : {{$item->updated_at->isoFormat('dddd, D MMMM Y')}}</p>
                                                                                        </div>
                                                                                </div>
                                                                            </td>
                                                                            <td class="text-right">
                                                                                <h6 class="f-w-700">Tahun {{$item->tahun_keluar}}</h6>
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                            @elseif($service->count() > 0)
                                            <div class="col-md-4">
                                                <div class="card text-center order-visitor-card" style="background-image: url('assets/images/queue.png');background-repeat: no-repeat; background-position: 10px; background-size: 80px 80px;">
                                                    <div class="card-block">
                                                        <h6 class="m-b-0">Nomor Antrian Saat Ini</h6>
                                                       
                                                        <h4 class="m-t-15 m-b-15"> 
                                                        @if ($antrian)
                                                            {{$antrian->no_antrian}}
                                                        @else
                                                            0
                                                        @endif</h4>
                                                        <p class="m-b-0">Harap Menunggu</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card text-center order-visitor-card" style="background-image: url('assets/images/add-to-queue.svg');background-repeat: no-repeat; background-position: 10px; background-size: 80px 80px;">
                                                    <div class="card-block">
                                                        <h6 class="m-b-0">Nomor Antrian Anda </h6>
                                                       @foreach ($service as $service1)
                                                        <h4 class="m-t-15 m-b-15"> 
                                                            @if ($service1->antrianRef->status == "Proses")
                                                                {{$service1->antrianRef->no_antrian}}
                                                            @else
                                                                0
                                                            @endif
                                                        </h4>
                                                        @endforeach
                                                        <p class="m-b-0">Harap Menunggu</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card text-center order-visitor-card" style="background-image: url('assets/images/car.svg');background-repeat: no-repeat; background-position: 10px; background-size: 80px 80px;">
                                                    <div class="card-block">
                                                        <h6 class="m-b-0">Jumlah Mobil Anda</h6>
                                                        <h4 class="m-t-15 m-b-15"> 
                                                            {{$kendaraan->count()}}
                                                        </h4>
                                                        <p class="m-b-0">Harap Menunggu</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8 col-8">
                                                <div class="card table-card">
                                                    <div class="card-header">
                                                        <h4>Daftar Mobil Anda</h4>
                                                            <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="icofont icofont-simple-left "></i></li>
                                                                <li><i class="icofont icofont-maximize full-card"></i></li>
                                                                <li><i class="icofont icofont-minus minimize-card"></i></li>
                                                                <li><i class="icofont icofont-refresh reload-card"></i></li>
                                                                <li><i class="icofont icofont-error close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover m-b-0 without-header">
                                                                <tbody>
                                                                    @foreach ($kendaraan as $item)
                                                                    <tr>
                                                                        <td>
                                                                            <div class="d-inline-block align-middle">
                                                                                <img src="{{url('model-mobil/' .$item->models->photo)}}" alt="user image" class="img-radius img-80 align-top m-r-30">
                                                                                    <div class="d-inline-block">
                                                                                        <h4 class="m-b-1">{{$item->merks->nama_merk}}</h4>
                                                                                        <h6 class="text-muted m-b-1 m-t-2">{{$item->plat_nomor}}</h6>
                                                                                        <p class="text-muted m-b-1">Service Terakhir : {{$item->updated_at->isoFormat('dddd, D MMMM Y')}}</p>
                                                                                    </div>
                                                                            </div>
                                                                        </td>
                                                                        <td class="text-right">
                                                                            <h6 class="f-w-700">Tahun {{$item->tahun_keluar}}</h6>
                                                                        </td>
                                                                    </tr>
                                                                    @endforeach
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                @foreach ($service as $service)
                                                <div class="card">
                                                    <table class="table table-hover table-bordered tabel-responsive">
                                                        <tbody>
                                                            <tr style="background-color:#6ba9ff;" class="text-white">
                                                                <td>
                                                                    <p class="font-weight-bold text-center mt-2">Daftar Pesanan <br> {{$service->kendaraanRef->merks->nama_merk}} {{$service->kendaraanRef->models->nama_model}}</p> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p class="font-weight-bold">Total Harga <br>
                                                                        <span class="text-primary">
                                                                            Rp. {{number_format($service->pembayaranRef->total,'0','.','.')}}
                                                                        </span><br>
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p class="font-weight-bold">Jenis Layanan <br>
                                                                    @foreach ($service->detailServiceRef as $item)
                                                                        <span class="text-primary">
                                                                            {{$item->jasaRef->nama_jasa}}
                                                                        </span><br>
                                                                    @endforeach
                                                                    @foreach ($service->detailServiceRef as $item)
                                                                        @if ($item->sparepartRef != null)
                                                                            <span class="text-secondary" style="font-size: 10px;">{{$item->sparepartRef->nama}}</span>
                                                                        @endif
                                                                    @endforeach
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p class="font-weight-bold">Status Service <br>
                                                                        @if($service->status == "0")
                                                                            <span class="text-primary">Mohon Tunggu Konfirmasi dari Admin</span>
                                                                        @elseif($service->status == "1")
                                                                            <span class="text-primary">Pemesanan service anda telah terkonfirmasi </span>
                                                                        @elseif($service->status == "2")
                                                                            <span class="text-primary">Mobil Anda Sedang di Service</span>
                                                                        @elseif($service->status == "3")
                                                                            <span class="text-primary">Mobil Anda Telah Selesai di Service</span>
                                                                        @endif
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Required Jquery -->
@include('User.layout.js')
</body>

</html>
