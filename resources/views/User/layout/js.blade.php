<script type="text/javascript" src="{{url('assets/user/js/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/user/js/jquery-ui/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/user/js/popper.js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/user/js/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{url('assets/user/js/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{url('assets/user/js/modernizr/modernizr.js')}}"></script>
<!-- am chart -->
<script src="{{url('assets/user/pages/widget/amchart/amcharts.min.js')}}"></script>
<script src="{{url('assets/user/pages/widget/amchart/serial.min.js')}}"></script>
<!-- Todo js -->
<script type="text/javascript " src="{{url('assets/user/pages/todo/todo.js')}} "></script>
<!-- Custom js -->
<script type="text/javascript" src="{{url('assets/user/pages/dashboard/custom-dashboard.js')}}"></script>
<script type="text/javascript" src="{{url('assets/user/js/script.js')}}"></script>
<script type="text/javascript " src="{{url('assets/user/js/SmoothScroll.js')}}"></script>
<script src="{{url('assets/user/js/pcoded.min.js')}}"></script>
<script src="{{url('assets/user/js/demo-12.js')}}"></script>
<script src="{{url('assets/user/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="https://kit.fontawesome.com/f6e00a0293.js" crossorigin="anonymous"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
var $window = $(window);
var nav = $('.fixed-button');
    $window.scroll(function(){
        if ($window.scrollTop() >= 200) {
         nav.addClass('active');
     }
     else {
         nav.removeClass('active');
     }
 });
</script>