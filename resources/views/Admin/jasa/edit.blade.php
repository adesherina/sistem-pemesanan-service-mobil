<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Jasa</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-users"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Jasa</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="float-left">Edit Data Jasa</h3>
                                                        <a href="{{route('jasa.list')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 float-right" style="padding:7px 10px !important">Kembali</a>                                                        
                                                    </div>
                                                    <div class="card-block">
                                                        <form action="{{route('jasa.updatedata',$jasa->id)}}" method="POST" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('post')
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Nama Jasa</label>
                                                                <div class="col-sm-10">
                                                                    <input readonly type="text" class="form-control" name="nama_jasa" value="{{$jasa->nama_jasa}}">
                                                                    @if ($errors->has('nama_jasa'))
                                                                    <span class="text-danger">{{ $errors->first('nama_jasa') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Kategori</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $kategori = DB::table('kategori_jasa')->get();    
                                                                    @endphp
                                                                    <select readonly name="kategori_id" class="form-control">
                                                                        <option value="{{$jasa->kategoris->id}}" selected>{{$jasa->kategoris->nama_kategori}}</option>
                                                                        @foreach ($kategori as $item)
                                                                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('kategori_id'))
                                                                    <span class="text-danger">{{ $errors->first('kategori_id') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Harga Jasa</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="harga_jasa" value="{{$jasa->harga_jasa}}">
                                                                    @if ($errors->has('harga_jasa'))
                                                                    <span class="text-danger">{{ $errors->first('harga_jasa') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                             <button class="btn waves-effect waves-light btn-primary mb-3 float-right">Simpan </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
