<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Jasa</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-users"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Jasa</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        @if (Session::get('role') === 'Admin')
                                        <a href="{{route('jasa.tambahdata')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3"><i class="fa fa-plus"></i>Tambah Data</a>
                                        @else

                                        @endif    
                                            @if (session()->has('status'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status') }}
                                                </div>
                                            @elseif(session()->has('status2'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status2') }}
                                                </div>
                                            @elseif(session()->has('status3'))
                                                <div class="alert bg-danger text-white alert-danger" role="alert">
                                                    {{ Session::get('status3') }}
                                                </div>
                                            @endif
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Data Jasa</h4>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table" id="datatabel">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama Jasa</th>
                                                                <th>Estimasi Waktu Pengerjaan</th>
                                                                <th>Kategori</th>
                                                                <th>Harga</th>
                                                                <th>Status</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($jasa as $items)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$items->nama_jasa}}</td>
                                                                <td>{{$items->estimasi}} Menit</td>
                                                                <td>
                                                                    @if (isset($items->kategoris))
                                                                        {{$items->kategoris->nama_kategori}}
                                                                    @endif
                                                                </td>
                                                                <td>Rp. {{number_format($items->harga_jasa,'0','.','.')}}</td>
                                                                <td>
                                                                    @if ($items->status == '0')
                                                                        <a href="" onclick="changeStatusAktif('{{$items->id}}', 1)" class="btn btn-danger btn-sm waves-effect waves-light" style="min-width: 85px !important">Tidak Aktif</a>
                                                                    @elseif($items->status == '1')
                                                                        <a href="" onclick="changeStatusTAktif('{{$items->id}}', 0)" class="btn btn-primary btn-sm waves-effect waves-light" style="min-width: 85px !important"> Aktif</a>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @if (Session::get('role') === 'Admin')
                                                                    <a href="{{route('jasa.editdata', $items->id)}}" class="btn waves-effect waves-light btn-primary"><i class="fa fa-edit"></i>
                                                                    </a>
                                                                    {{-- <form action="{{route('jasa.deletedata', $items->id)}}" id="delete{{$items->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                                    @method('delete')
                                                                    <button class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                                                    @csrf
                                                                    </form> --}}
                                                                    @else
                                                                    <a href="#" data-toggle="tooltip" data-placement="right" title="detail" class="btn waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        function changeStatusAktif(id, status) {
            event.preventDefault();
            console.log(id, status);
            Swal.fire({
                title: 'Apakan anda yakin?',
                text: "Data Jasa Aktif",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Berhasil',
                    'Data Jasa Aktif',
                    'success'
                    )
                    console.log('hiyaa');
                    var url = '{{ url("jasa/changestatusaktif/") }}/'+id;
                    console.log(url);
                    $.ajax({
                        url: url,
                        type: "POST",
                        data:{
                            status:status
                        },
                        success: function (data) {
                            console.log(data);
                            if(data.status == 1){
                                location.reload();
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
            });
        };
        function changeStatusTAktif(id, status) {
            event.preventDefault();
            console.log(id, status);
            Swal.fire({
                title: 'Apakan anda yakin?',
                text: "Data Jasa Ini Tidak Aktif",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                    'Berhasil',
                    'Data Jasa Tidak Aktif',
                    'success'
                    )
                    console.log('hiyaa');
                    var url = '{{ url("jasa/changestatustidakaktif/") }}/'+id;
                    console.log(url);
                    $.ajax({
                        url: url,
                        type: "POST",
                        data:{
                            status:status
                        },
                        success: function (data) {
                            console.log(data);
                            if(data.status == 0){
                                location.reload();
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
            });
        };
    </script>
    
</body>

</html>
