<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">User</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">User</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                 <h5>Detail User</h5>
                                                    <div class="card-header-right">
                                                        <a href="{{route('user.list')}}" class="btn waves-effect waves-light btn-primary btn-sm">Kembali</a>
                                                    </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Nama User</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$user->nama_lengkap}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Nomor Telepon</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$user->no_telp}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Username</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$user->username}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Jenis Kelamin</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$user->jenis_kelamin}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Alamat</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$user->alamat}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="styleSelector"></div>
        </div>
    </div>
</div>
</div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
