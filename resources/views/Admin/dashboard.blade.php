<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Dashboard</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-home"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/businessman.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Admin</h5>
                                                        @php
                                                            use App\Model\AdminLogin;
                                                            $admins = AdminLogin::where('role', 'Admin')->get();
                                                            $count = $admins->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/user.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">User</h5>
                                                        @php
                                                            use App\User;
                                                            $users = User::get();
                                                            $count = $users->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/testimonial.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Kritik dan Saran</h5>
                                                        @php
                                                            use App\Model\KritikSaran;
                                                            $ks = KritikSaran::get();
                                                            $count = $ks->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/group.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Jasa</h5>
                                                        @php
                                                            use App\Model\Jasa;
                                                            $jasa = Jasa::get();
                                                            $count = $jasa->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/cog.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Sparepart</h5>
                                                        @php
                                                            use App\Model\Sparepart;
                                                            $sp = Sparepart::get();
                                                            $count = $sp->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/car.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Kendaraan</h5>
                                                        @php
                                                            use App\Model\Kendaraan;
                                                            $kd = Kendaraan::get();
                                                            $count = $kd->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/queue.png')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Antrian</h5>
                                                        @php
                                                            use App\Model\Antrian;
                                                            $antrian = Antrian::get();
                                                            $count = $antrian->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/customer-support.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Service</h5>
                                                        @php
                                                            use App\Model\Services;
                                                            $sv = Services::get();
                                                            $count = $sv->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-4">
                                                <div class="card text-center order-visitor-card">
                                                    <div class="card-block">
                                                        <img src="{{url('assets/images/transaction.svg')}}" class="float-left" alt="" style="width: 75px; height:75px;">
                                                        <h5 class="m-b-0">Transaksi Service</h5>
                                                        @php
                                                            use App\Model\Pembayaran;
                                                            $ts = Pembayaran::get();
                                                            $count = $ts->count();
                                                        @endphp
                                                        <h4 class="m-t-15 m-b-15">{{$count}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
