<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Transaksi Service</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top "></a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Transaksi Service</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Data Transaksi Service</h4>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    @if (Session::get('role') === 'Admin')
                                                    <table class="datatabel" id="datatabel">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nota</th>
                                                                <th>Nama User</th>
                                                                <th>Kendaraan</th>
                                                                <th>Total Harga</th>
                                                                <th>Status</th>
                                                                <th>Tanggal Pemesanan</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($ts as $item)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$item->nota}}</td>
                                                                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->serviceRef->kendaraanRef->merks->nama_merk}} {{$item->serviceRef->kendaraanRef->models->nama_model}}</td>
                                                                <td>Rp. {{number_format($item->total,'0','.','.')}}</td>
                                                                <td>
                                                                    @if ($item->status == '0')
                                                                        <a href="" onclick="changeStatus('{{$item->nota}}', 1)" class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Belum Bayar</a>
                                                                    @elseif($item->status == '1')
                                                                        <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Lunas</button>
                                                                    @endif
                                                                </td>
                                                                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
                                                                <td>
                                                                    <form action="" id="" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                                    @method('delete')
                                                                    <button class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                                                    @csrf
                                                                    </form>
                                                                    <a href="{{route('transaksi.detaildata', $item->nota)}}" class="btn waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @else
                                                    <table class="table" id="tabel-transaksi">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nota</th>
                                                                <th>Nama User</th>
                                                                <th>Kendaraan</th>
                                                                <th>Total Harga</th>
                                                                <th>Status</th>
                                                                <th>Tanggal</th>
                                                                <th>tanggalexcel</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($ts as $item)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$item->nota}}</td>
                                                                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->serviceRef->kendaraanRef->merks->nama_merk}} {{$item->serviceRef->kendaraanRef->models->nama_model}}</td>
                                                                <td>Rp. {{number_format($item->total,'0','.','.')}}</td>
                                                                <td>
                                                                    @if ($item->status == '0')
                                                                        <button type="button"  class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Belum Bayar</button>
                                                                    @elseif($item->status == '1')
                                                                        <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Lunas</button>
                                                                    @endif
                                                                </td>
                                                                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
                                                                <td>{{ date("d/m/Y", strtotime($item->created_at)) }}</td>
                                                                <td>
                                                                    {{-- <form action="" id="" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                                    @method('delete')
                                                                    <button class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                                                    @csrf
                                                                    </form> --}}
                                                                    <a href="{{route('transaksi.detaildata', $item->nota)}}" data-toggle="tooltip" data-placement="right" title="detail" class="btn waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="8" style="text-align:right">Total :</th>
                                                                <th></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
        <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var start_date;
        var end_date;
        var bulan = ['', 'Januari', 'Februari', 'Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        var start_month;
        var end_month;

    var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
        var dateStart = parseDateValue(start_date);
        var dateEnd = parseDateValue(end_date);
        var evalDate= parseDateValue(aData[7]);
       
        if ( ( isNaN( dateStart ) && isNaN( dateEnd ) ) ||
            ( isNaN( dateStart ) && evalDate <= dateEnd ) ||
            ( dateStart <= evalDate && isNaN( dateEnd ) ) ||
            ( dateStart <= evalDate && evalDate <= dateEnd ) )
        {
            return true;
        }
        return false;
    });

    function parseDateValue(rawDate) {
        var dateArray= rawDate.split("/");
        var parsedDate= new Date(dateArray[2], parseInt(dateArray[1])-1, dateArray[0]);  // -1 because months are from 0 to 11   
        return parsedDate;
    }   

    function convertStart(start_date){
        start_date = start_date.split("/").reverse().join("-");
        var parts_start = start_date.split("-");
        start_month = parts_start[2] + " " + bulan[parseInt(parts_start[1])] + " "+ parts_start[0];
        console.log(start_month, parseInt(parts_start[1]));
    }


    function convertEnd(end_date){
        end_date = end_date.split("/").reverse().join("-");
        var parts_end = end_date.split("-");
        end_month = parts_end[2] + " " + bulan[parseInt(parts_end[1])] + " "+ parts_end[0];
    }

    $(document).ready(function () {
        var $dTable = $('#tabel-transaksi').DataTable({
            "dom":"<'row'<'col-sm-3'l>B<'col-sm-3' <'datesearchbox'>><'col-sm-'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-6'i><'col-sm-6'p>>",
                columnDefs:[
                    {
                        "targets":[7],
                        "visible":false,
                        "searchable":true
                    }
                ],
                buttons: [ 
                    // 'colvis',    
                    {
                        text: 'Download Excel',
                        extend: 'excelHtml5',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        },
                        customize: function ( xlsx ) {
                            var sheet = xlsx.xl.worksheets['sheet1.xml'];
                            if(start_date != null && end_date != null){
                                convertStart(String(start_date));
                                convertEnd(String(end_date));
                                $('c[r=A1] t', sheet).text( 'Data Transaksi '+ start_month + " - "+ end_month);
                                $('row:first c', sheet).attr( 's', '51', '2' ); // first row is bold
                            }else{
                                $('c[r=A1] t', sheet).text( 'Data Transaksi');
                                $('row:first c', sheet).attr( 's', '51', '2' ); // first row is bold
                            }
                        }
                    },
                    {
                        text: 'Download Pdf',
                        extend:'pdfHtml5',
                        download: 'open',
                        filename: 'laporan_bengkel_tiga_saudara_pdf',
                        orientation:  'portrait',
					    pageSize: 'A4',
                        exportOptions: {
                            columns: [0, 1, 2, 3, 4, 5, 6]
                        },
                        customize: function ( doc ) {
                            console.log(doc.content)
                            doc.content.splice(0, 0, {
                                margin: [12, 0, 0, 12],
                                alignment: "center",
                            });
                            var tblBody = doc.content.splice(0,1);
                            if(start_date != null && end_date != null){
                                convertStart(String(start_date));
                                convertEnd(String(end_date));
                                $('c[r=A1] t', tblBody).text( 'Data Transaksi '+ start_month + " - "+ end_month);
                                $('row:first c', tblBody).attr( 's', '51', '2' ); // first row is bold
                            }else{
                                $('c[r=A1] t', tblBody).text( 'Data Transaksi');
                                $('row:first c', tblBody).attr( 's', '51', '2' ); // first row is bold
                            }
                            
                            doc.styles.tableHeader.fontSize = 10;
                            doc['header']=(function() {
							return {
								columns: [
									{
										alignment: 'center',
										fontSize: 14,
										text: 'Laporan Data Transaksi Service'
									}
								],
								margin: 20
							}
						    });
                        }
                    }
                ],
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;
                console.log(api);
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\Rp.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                var rupiah = function (angka){
                    var reverse = angka.toString().split('').reverse().join(''),
                    ribuan = reverse.match(/\d{1,3}/g);
                    ribuan = ribuan.join('.').split('').reverse().join('');
                    return ribuan;
                }

                // Total over all pages
                total = api
                    .column( 4 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Total over this page
                pageTotal = api
                    .column( 4, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
    
                // Update footer
                var parse_rupiah = rupiah(total);
                $( api.column( 4 ).footer() ).html(
                    'Total : Rp.'+ parse_rupiah
                );
            }
        });

        //menambahkan daterangepicker di dalam datatables
        $("div.datesearchbox").html('<div class="input-group mb-3"> <div class="input-group-addon"> <i class="glyphicon glyphicon-calendar"></i> </div><input type="text" class="form-control pull-right" id="datesearch" placeholder="Search by date range.."> </div>');

        document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";

        $('#datesearch').daterangepicker({
            autoUpdateInput: false,
            useCurrent: false

        });

        $('#datesearch').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            start_date=picker.startDate.format('DD/MM/YYYY');
            end_date=picker.endDate.format('DD/MM/YYYY');
            $.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
            $dTable.draw();
        });

        $('#datesearch').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            start_date='';
            end_date='';
            $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
            $dTable.draw();
        });
    });
          function changeStatus(nota, status) {
                event.preventDefault();
                console.log(nota, status);
                Swal.fire({
                    title: 'Apakan anda yakin?',
                    text: "Pembayaran ini akan menjadi lunas",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya'
                }).then((result) => {
                    if (result.isConfirmed) {
                        Swal.fire(
                        'Berhasil',
                        'Pembayaran ini telah Lunas',
                        'success'
                        )
                        console.log('hiyaa');
                        var url = '{{ url("transaksi/changestatus/") }}/'+nota;
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "POST",
                            data:{
                                status:status
                            },
                            success: function (data) {
                                console.log(data);
                                if(data.status == 1){
                                    location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                                // swal({
                                //     title: 'Opps...',
                                //     text: error.message,
                                //     type: 'error',
                                //     timer: '1500'
                                // })
                            }
                        });
                    }
                });
            };
        </script>
    
</body>

</html>
