<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Transaksi Service</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top "></a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Transaksi Service</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <form action="{{route('cari.laporan.belum.lunas')}}" method="get" enctype="multipart/form-data">
                                                          
                                                            @method('get')
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Dari</label>
                                                                <div class="col-sm-6">
                                                                    <input type="date" class="form-control" value="{{$from}}" name="from" placeholder="Masukan Nama Jasa">
                                                            
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Sampai</label>
                                                                <div class="col-sm-6">
                                                                    <input type="date" class="form-control" value="{{$to}}" name="to" placeholder="Masukan Nama Jasa">
                                                                    {{-- @if ($errors->has('nama_jasa'))
                                                                    <span class="text-danger">{{ $errors->first('nama_jasa') }}</span>
                                                                    @endif --}}
                                                                </div>
                                                            </div>
                                                            <button class="btn waves-effect waves-light btn-primary mb-3"><i class="fa fa-search m-r-10"></i>Cari</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Data Transaksi Service Belum Lunas</h4>
                                                <div class="card-header-right">
                                                    <a href="{{route('transaksi.laporan.lunas')}}" class="btn waves-effect waves-light btn-success btn-sm mr-2">Lunas</a>
                                                    <a href="{{route('transaksi.laporan.belum.lunas')}}" class="btn waves-effect waves-light btn-warning btn-sm mr-2">Belum Lunas</a>
                                                    <a href="{{route('export.laporan.belum.lunas', ['from'=>$from,'to' => $to])}}" class="btn waves-effect waves-light btn-primary btn-sm">Download Excel</a>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table" id="service-table">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nota</th>
                                                                <th>Nama User</th>
                                                                <th>Kendaraan</th>
                                                                <th>Total Harga</th>
                                                                <th>Status</th>
                                                                <th>Tanggal</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($ts as $item)
                                                            <tr>
                                                                <td scope="row">{{$loop->iteration}}</td>
                                                                <td>{{$item->nota}}</td>
                                                                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->serviceRef->kendaraanRef->merks->nama_merk}} {{$item->serviceRef->kendaraanRef->models->nama_model}}</td>
                                                                <td>Rp.{{number_format($item->total,'0','.','.')}}</td>
                                                                <td>
                                                                    @if ($item->status == '0')
                                                                        <button type="button"  class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Belum Bayar</button>
                                                                    @elseif($item->status == '1')
                                                                        <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Lunas</button>
                                                                    @endif
                                                                </td>
                                                                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
                                                            </tr>
                                                            @endforeach
                                                            
                                                        </tbody>
                                                        {{-- <tfoot>
                                                            <tr>
                                                                <th colspan="6" style="text-align:right">Total:</th>
                                                                <th></th>
                                                            </tr>
                                                        </tfoot> --}}
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')

    <script>
    $(document).ready(function() {
    $('#service-table').DataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            console.log(api);
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\Rp.]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            var rupiah = function (angka){
                var reverse = angka.toString().split('').reverse().join(''),
                ribuan = reverse.match(/\d{1,3}/g);
                ribuan = ribuan.join('.').split('').reverse().join('');
                return ribuan;
            }

            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            var parse_rupiah = rupiah(total);
            $( api.column( 4 ).footer() ).html(
                'Total : Rp.'+ parse_rupiah
            );
        }
    } );
} );
    </script>
    
</body>

</html>
