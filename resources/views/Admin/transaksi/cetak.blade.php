<!DOCTYPE html>
<html lang="en">
<head>
    {{-- <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title> --}}
</head>
<body>
    <h3><b><i>Laporan Transaksi</i></b></h3>
    <table id="table-pemasukan" class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Nota</th>
                <th scope="col">Nama User</th>
                <th scope="col">Kendaraan</th>
                <th scope="col">Total Harga</th>
                <th scope="col">Status</th>
                <th scope="col">Tanggal</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($ts as $item)
            <tr>
                <th scope="row">{{$loop->iteration}}</th>
                <td>{{$item->nota}}</td>
                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                <td>{{$item->serviceRef->kendaraanRef->merks->nama_merk}} {{$item->serviceRef->kendaraanRef->models->nama_model}}</td>
                <td>Rp. {{number_format($item->total,'0','.','.')}}</td>
                <td>
                    @if ($item->status == '0')
                        <button type="button"  class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Belum Bayar</button>
                    @elseif($item->status == '1')
                        <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Lunas</button>
                    @endif
                </td>
                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
            </tr>
            @endforeach
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Total Pemasukan : </td>
                <td><b><i>Rp. {{ number_format($total,0) }}</i></b></td>
            </tr>
        </tbody>
    </table>
</body>
</html>