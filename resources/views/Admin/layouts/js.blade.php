<script type="text/javascript" src="{{url('assets/js/jquery/jquery.min.js ')}}"></script>
<script type="text/javascript" src="{{url('assets/js/jquery-ui/jquery-ui.min.js')}} "></script>
<script type="text/javascript" src="{{url('assets/js/popper.js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/bootstrap/js/bootstrap.min.js')}} "></script>
<!-- waves js -->
<script src="{{url('assets/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{url('assets/js/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{url('assets/js/pcoded.min.js')}}"></script>
<script src="{{url('assets/js/vertical/vertical-layout.min.js')}}"></script>
<script src="{{url('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{url('assets/js/script.min.js')}}"></script>

    <script type="text/javascript" src="{{url('assets/pg-calendar/demo/js/jquery.latest.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/pg-calendar/demo/js/semantic.ui.min.js')}}"></script>
    <script type="text/javascript" src="{{url('assets/pg-calendar/demo/js/prism.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/pg-calendar/dist/js/pignose.calendar.full.min.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.colVis.min.js"></script>
<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
{{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

{{-- <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript">
 $(document).ready(function() {
     $('#nama-pelanggan').select2();
 });
</script>

<script>
    $(document).ready(function() {
    $('#datatabel').DataTable();
    } );
</script>
