    <title>Bengkel Tiga Saudara</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="keywords" content="flat ui, admin Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="Codedthemes" />
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap/css/bootstrap.min.css')}}">
    <!-- waves.css -->
    <link rel="stylesheet" href="{{url('assets/pages/waves/css/waves.min.css')}}" type="text/css" media="all">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/icon/themify-icons/themify-icons.css')}}">
    <!-- feather icon -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/icon/feather/css/feather.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('assets/css/jquery.mCustomScrollbar.css')}}">

    <link rel="shortcut icon" type="image/x-icon" href="{{url('assets/pg-calendar/demo/img/pignose-ico.ico')}}" />
	<link rel="stylesheet" type="text/css" href="{{url('assets/pg-calendar/demo/css/semantic.ui.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{url('assets/pg-calendar/demo/css/prism.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{url('assets/pg-calendar/demo/css/calendar-style.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{url('assets/pg-calendar/demo/css/style.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{url('assets/pg-calendar/dist/css/pignose.calendar.min.css')}}" />
    
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">

    <!--Daterangepicker -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <meta name="csrf-token" content="{{ csrf_token() }}">