<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                @php
                use App\Model\AdminLogin;
                $admin_data = AdminLogin::where('id', Session::get('id'))->first();
                @endphp
                <img class="img-80 img-radius" src="{{url('admin-profil/'.$admin_data->photo)}}" alt="User-Profile-Image">
                <div class="user-details">
            @php
                use Illuminate\Support\Facades\DB;
                use Illuminate\Support\Facades\Session;
                $you = DB::table('admin')->where('id', Session::get('id'))->first();
                
            @endphp
                    <span id="more-details">Hi, {{Session::get('name')}}</span>
                </div>
            </div>
            <div class="main-menu-content">
                {{-- <ul>
                    <li class="more-details">
                        <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                        <a href="#!"><i class="ti-settings"></i>Settings</a>
                        <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
                    </li>
                </ul> --}}
            </div>
        </div>
        @if (Session::get('role') === 'Admin')
        <ul class="pcoded-item pcoded-left-item mt-4">
            <li class="">
                <a href="{{url('admin/dashboard')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('user.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-user"></i><b>D</b></span>
                    <span class="pcoded-mtext">User</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item mt-4">
            <li class="">
                <a href="{{url('kritikSaran/list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/komen.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Kritik dan Saran</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Jasa</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i><b>D</b></span>
                    <span class="pcoded-mtext">Jasa</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('kategori.jasa.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Kategori</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('jasa.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Daftar Jasa</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Sparepart</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-cogs"></i><b>D</b></span>
                    <span class="pcoded-mtext">Sparepart</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('kategori.sparepart.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Kategori</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('merk.sparepart.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Merek</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('Sparepart.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Daftar Sparepart</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Kendaraan</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-car"></i><b>D</b></span>
                    <span class="pcoded-mtext">Kendaraan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('merk.kendaraan.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Merek Mobil</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('model.kendaraan.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Model Mobil</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('kendaraan.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Daftar Kendaraan</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Antrian</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/queue.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Antrian</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('atur.antrian.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Atur Antrian</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('antrian.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-angle-right"></i></span>
                            <span class="pcoded-mtext">Daftar Antrian</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Service</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('service.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/customer-support.png')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('transaksi.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Transaksi Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        @else
        <ul class="pcoded-item pcoded-left-item mt-4">
            <li class="">
                <a href="{{url('admin/dashboard')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('admin.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i><b>D</b></span>
                    <span class="pcoded-mtext">Admin</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item mt-4">
            <li class="">
                <a href="{{url('kritikSaran/list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/komen.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Kritik dan Saran</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('user.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-user"></i><b>D</b></span>
                    <span class="pcoded-mtext">User</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('jasa.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i><b>D</b></span>
                    <span class="pcoded-mtext">Jasa</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('Sparepart.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-cogs"></i><b>D</b></span>
                    <span class="pcoded-mtext">Sparepart</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('kendaraan.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-car"></i><b>D</b></span>
                    <span class="pcoded-mtext">Kendaraan</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('antrian.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/queue.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Antrian</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('service.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/customer-support.png')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigation-label">Transaksi Service</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Transaksi Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
                <ul class="pcoded-submenu">
                    <li class="">
                        <a href="{{route('transaksi.list')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                            <span class="pcoded-mtext">Data Transaksi Service</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="">
                        <a href="{{route('transaksi.laporan')}}" class="waves-effect waves-dark">
                            <span class="pcoded-micon"><i class="ti-list"></i></span>
                            <span class="pcoded-mtext">Laporan Transaksi Service</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        {{-- <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{route('transaksi.list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><img src="{{url('assets/images/transaction2.svg')}}" alt="user image" class=" img-20 align-top m-r-20"><b>D</b></span>
                    <span class="pcoded-mtext">Transaksi Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul> --}}
        @endif
    </div>
</nav>