<table >
    <thead>
        <tr>
            <th>No</th>
            <th>Nota</th>
            <th>Nama User</th>
            <th>Kendaraan</th>
            <th>Total Harga</th>
            <th>Status</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        @php
            $total = 0;
        @endphp
        @foreach ($ts as $item)
            @php
                $total += (int)$item->total;
            @endphp
            <tr>
                <td scope="row">{{$loop->iteration}}</td>
                <td>{{$item->nota}}</td>
                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                <td>{{$item->serviceRef->kendaraanRef->merks->nama_merk}} {{$item->serviceRef->kendaraanRef->models->nama_model}}</td>
                <td>Rp.{{number_format($item->total,'0','.','.')}}</td>
                <td>
                    @if ($item->status == '0')
                        Belum Bayar
                    @elseif($item->status == '1')
                        Lunas
                    @endif
                </td>
                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
            </tr>
        @endforeach
        <tr>
            <th colspan="6" style="text-align:right">Total:</th>
            <th>Rp.{{number_format($total,'0','.','.')}}</th>
        </tr>
    </tbody>
</table>