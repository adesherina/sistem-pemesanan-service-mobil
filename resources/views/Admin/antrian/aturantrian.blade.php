<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Atur Antrian</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/queue.svg')}}" alt="user image" class=" img-20 align-top "> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Antrian</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-12">
                                                 @if (session()->has('status'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status') }}
                                                </div>
                                            @elseif(session()->has('status2'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status2') }}
                                                </div>
                                            @elseif(session()->has('status3'))
                                                <div class="alert bg-danger text-white alert-danger" role="alert">
                                                    {{ Session::get('status3') }}
                                                </div>
                                            @endif
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Sedang Dalam Antrian</h4>
                                                        <div class="card-header-right">
                                                            <a href="{{route('atur.antrian.proses')}}" class="btn btn-primary btn-sm btn-outline-primary waves-effect waves-light mr-2">Lanjut</a>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <tbody>
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Nomor Antrian</th>
                                                                            <th>Nama User</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($antrian as $item)
                                                                        <tr>
                                                                            <td>{{$item->no_antrian}}</td>
                                                                            <td data-toggle="tooltip" data-placement="right" title="{{$item->serviceRef->kd_service}}">{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                            <td>
                                                                                @if($item->status === "Menunggu")
                                                                                <button type="button" class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Menunggu</button>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Antrian Saat Ini</h4>
                                                        <div class="card-header-right">
                                                            <ul class="list-unstyled card-option">
                                                                <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                                <li><i class="fa fa-window-maximize full-card"></i></li>
                                                                <li><i class="fa fa-minus minimize-card"></i></li>
                                                                <li><i class="fa fa-refresh reload-card"></i></li>
                                                                <li><i class="fa fa-trash close-card"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="card-block">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <tbody>
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Nomor Antrian</th>
                                                                            <th>Nama User</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($antrianProses as $item)
                                                                        <tr>
                                                                            <td>{{$item->no_antrian}}</td>
                                                                            <td data-toggle="tooltip" data-placement="right" title="{{$item->serviceRef->kd_service}}">{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                            <td>
                                                                               
                                                                                @if($item->status === "Proses")
                                                                                <div class="btn-group dropright">
                                                                                    <button type="button" style="min-width: 85px !important" class="btn btn-primary btn-sm waves-effect waves-light">
                                                                                        Proses
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-primary btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="sr-only">Toggle Dropright</span>
                                                                                    </button>
                                                                                    <div class="dropdown-menu">
                                                                                        <!-- Dropdown menu links -->
                                                                                        <a class="dropdown-item" href="{{route('atur.antrian.selesai', $item->id)}}">Selesai</a>
                                                                                    </div>
                                                                                </div> 
                                                                                @elseif($item->status ==="Selesai")
                                                                                <div class="btn-group dropright">
                                                                                    <button type="button" style="min-width: 85px !important" class="btn btn-success btn-sm waves-effect waves-light">
                                                                                        Selesai
                                                                                    </button>
                                                                                    <button type="button" class="btn btn-success btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="sr-only">Toggle Dropright</span>
                                                                                    </button>
                                                                                </div>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
