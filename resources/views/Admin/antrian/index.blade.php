<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Antrian</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-car"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Antrian</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="col-lg-12">
                                            <a href="#" id="date-pick-button" type="button" onclick="showDateModal()" class="btn btn-primary mb-2">{{Carbon\Carbon::now()->isoFormat('D MMM Y')}}</a>
                                            <div class="card">
                                            <div class="card-header">
                                                <h4>Data Antrian</h4>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    @if (Session::get('role') === 'Admin')
                                                    <table class="table" id="tabel-antrian">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nomor Antrian</th>
                                                                <th>Nama</th>
                                                                <th>Nota</th>
                                                                <th>Status</th>
                                                                <th>Tanggal Service</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($antrian as $item)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$item->no_antrian}}</td>
                                                                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->serviceRef->kd_service}}</td>
                                                                <td>
                                                                    @if($item->status === "Menunggu")
                                                                    <button type="button" class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Menunggu</button>
                                                                    @elseif($item->status === "Proses")
                                                                    <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="min-width: 85px !important">Proses</button>
                                                                    @elseif($item->status === "Selesai")
                                                                    <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Selesai</button>
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    @php
                                                                        // use Carbon\Carbon;
                                                                        $tgl = Carbon\Carbon::parse($item->tanggal)->translatedFormat('D, d M Y');
                                                                    @endphp
                                                                    {{$tgl}}
                                                                </td>
                                                                <td>
                                                                    <form action="{{route('antrian.deletedata', $item->id)}}" id="delete{{$item->id}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                                    @method('delete')
                                                                    <button class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                                                    @csrf
                                                                    </form>
                                                                    
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @else
                                                    <table class="table" id="tabel-antrian">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nomor Antrian</th>
                                                                <th>Nama</th>
                                                                <th>Nota</th>
                                                                <th>Tanggal Service</th>
                                                                <th>Status</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($antrian as $item)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$item->no_antrian}}</td>
                                                                <td>{{$item->serviceRef->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->serviceRef->kd_service}}</td>
                                                                <td> 
                                                                    @php
                                                                        $tgl = Carbon\Carbon::parse($item->tanggal)->translatedFormat('D, d m y');
                                                                    @endphp
                                                                    {{$tgl}}
                                                                </td>
                                                                <td>
                                                                    @if($item->status === "Menunggu")
                                                                    <button type="button" class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 85px !important">Menunggu</button>
                                                                    @elseif($item->status === "Proses")
                                                                    <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="min-width: 85px !important">Proses</button>
                                                                    @elseif($item->status === "Selesai")
                                                                    <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 85px !important">Selesai</button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="calendar modal" id="dateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"></div>


    <!-- Required Jquery -->
    @include('Admin.layouts.js')

    <script>

        const showDateModal = () => {
            $('#dateModal').modal('show');
        }

        $(function() {
            let table = $('#tabel-antrian').DataTable();

            const searhByDate = (date) => {
                table.search(date).draw(); 
            }

            const convertDateDBtoIndo = (date) => {
                // Cari berdasarkan tanggal
                searhByDate(date);

                bulanIndo = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep' , 'Oct', 'Nov', 'Dec'];
            
                tanggal = date.split("-")[2];
                bulan = date.split("-")[1];
                tahun = date.split("-")[0];

                tglTxt = parseInt(tanggal) + ' ' + bulanIndo[Math.abs(parseInt(bulan))] + ' ' + tahun;

                return $('#date-pick-button').text(tglTxt);
            }

            const onClickHandler = (date, obj) => {

                var $calendar = obj.calendar;
                var $box = $calendar.parent().siblings('.box').show();
                var text = '';

                if(date[0] !== null) {
                    text += date[0].format('YYYY-MM-DD');
                }

                if(date[0] !== null && date[1] !== null) {
                    text += ' ~ ';
                } else if(date[0] === null && date[1] == null) {
                    text += 'nothing';
                }

                if(date[1] !== null) {
                    text += date[1].format('YYYY-MM-DD');
                }

                convertDateDBtoIndo(text)
                $('#dateModal').modal('hide');
            }

            $('.calendar').pignoseCalendar({
                select: onClickHandler
            });
        });
    </script>
    
</body>

</html>
