<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Service</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/customer-support.png')}}" alt="user image" class=" img-20 align-top "> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Service</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="float-left">Tambahan Service</h3>
                                                        <a href="{{route('service.list')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 float-right" style="padding:7px 10px !important">Kembali</a>                                                        
                                                    </div>
                                                    <div class="card-block">
                                                        <form action="" method="" enctype="multipart/form-data">
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Kode Service</label>
                                                                <div class="col-sm-10">
                                                                    <input readonly type="text" class="form-control" name="nama_lengkap"  value="{{$service->kd_service}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Nama Pelanggan</label>
                                                                <div class="col-sm-10">
                                                                    <input readonly type="text" class="form-control" name="nama_lengkap"  value="{{$service->userRef->nama_lengkap}}">
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Layanan</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $jasa = App\Model\Jasa::get();   
                                                                    @endphp
                                                                    <select name="jasa_id" class="form-control">
                                                                        <option selected>Pilih Layanan</option>
                                                                        @foreach ($jasa as $item)
                                                                            <option value="{{$item->id}}">{{$item->nama_jasa}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Sparepart</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $sparepart = App\Model\Sparepart::get();   
                                                                    @endphp
                                                                    <select name="sparepart_id" class="form-control">
                                                                        <option selected>Pilih Sparepart</option>
                                                                        @foreach ($sparepart as $item)
                                                                            <option value="{{$item->id}}">{{$item->nama}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                             <button type="submit" class="btn waves-effect waves-light btn-primary mb-3 float-right">Simpan Data</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
