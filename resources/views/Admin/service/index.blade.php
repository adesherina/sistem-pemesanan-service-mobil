<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Service</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/customer-support.png')}}" alt="user image" class=" img-20 align-top"></a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Service</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <a href="{{route('service.tambahdata')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3"><i class="fa fa-plus"></i>Tambah Service</a>
                                        @if (session()->has('status'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status') }}
                                                </div>
                                            @elseif(session()->has('status2'))
                                                <div class="alert bg-success text-white alert-success" role="alert">
                                                    {{ Session::get('status2') }}
                                                </div>
                                            @elseif(session()->has('status3'))
                                                <div class="alert bg-danger text-white alert-danger" role="alert">
                                                    {{ Session::get('status3') }}
                                                </div>
                                            @endif
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Data Service</h4>
                                                <div class="card-header-right">
                                                    <ul class="list-unstyled card-option">
                                                        <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                        <li><i class="fa fa-window-maximize full-card"></i></li>
                                                        <li><i class="fa fa-minus minimize-card"></i></li>
                                                        <li><i class="fa fa-refresh reload-card"></i></li>
                                                        <li><i class="fa fa-trash close-card"></i></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-block table-border-style">
                                                <div class="table-responsive">
                                                    <table class="table dt-responsive nowrap" id="datatabel">
                                                        <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Nama User</th>
                                                                <th>Merk Mobil</th>
                                                                <th>Status</th>
                                                                <th>Tanggal Pemesanan</th>
                                                                <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($service as $item)
                                                            <tr>
                                                                <th scope="row">{{$loop->iteration}}</th>
                                                                <td>{{$item->userRef->nama_lengkap}}</td>
                                                                <td>{{$item->kendaraanRef->merks->nama_merk}}</td>
                                                                <td>
                                                                    @if (Session::get('role') === 'Admin')
                                                                        @if($item->status == "0")
                                                                            <div class="btn-group dropright" >
                                                                                <button  type="button" style="min-width: 85px !important" class="btn btn-warning btn-sm waves-effect waves-light">
                                                                                    Belum di Konfirmasi
                                                                                </button>
                                                                                {{-- <button type="button"  class="btn btn-warning btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <span class="sr-only">Toggle Dropright</span>
                                                                                </button>
                                                                                <div class="dropdown-menu">
                                                                                    <!-- Dropdown menu links -->
                                                                                    <a class="dropdown-item" href="#" onclick="changeStatus('{{$item->kd_service}}', 1)">Sedang di Proses</a>
                                                                                </div> --}}
                                                                            </div>
                                                                            @elseif($item->status == '1')
                                                                                <div class="btn-group dropright">
                                                                                    <button  type="button" style="min-width: 85px !important" class="btn btn-primary btn-sm waves-effect waves-light">
                                                                                        Terkonfirmasi
                                                                                    </button>
                                                                                    {{-- <button type="button" class="btn btn-primary btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="sr-only">Toggle Dropright</span>
                                                                                    </button>
                                                                                    <div class="dropdown-menu">
                                                                                        <!-- Dropdown menu links -->
                                                                                        <a class="dropdown-item" href="#" onclick="changeStatus('{{$item->kd_service}}', 2)">selesai</a>
                                                                                    </div> --}}
                                                                                </div> 
                                                                            @elseif($item->status == '2')
                                                                                <div class="btn-group dropright">
                                                                                    <button  type="button" style="min-width: 85px !important" class="btn btn-primary btn-sm waves-effect waves-light">
                                                                                        Sedang di Proses
                                                                                    </button>
                                                                                    {{-- <button type="button" class="btn btn-primary btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="sr-only">Toggle Dropright</span>
                                                                                    </button>
                                                                                    <div class="dropdown-menu">
                                                                                        <!-- Dropdown menu links -->
                                                                                        <a class="dropdown-item" href="#" onclick="changeStatus('{{$item->kd_service}}', 2)">selesai</a>
                                                                                    </div> --}}
                                                                                </div> 
                                                                            @elseif($item->status == '3')
                                                                                <div class="btn-group dropright">
                                                                                    <button  type="button" style="min-width: 85px !important" class="btn btn-success btn-sm waves-effect waves-light">
                                                                                        Selesai
                                                                                    </button>
                                                                                    {{-- <button type="button" class="btn btn-success btn-sm waves-effect waves-light dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        <span class="sr-only">Toggle Dropright</span>
                                                                                    </button> --}}
                                                                                </div>  
                                                                            @endif
                                                                    @else
                                                                         @if($item->status == '0')
                                                                            <button type="button" class="btn btn-warning btn-sm waves-effect waves-light" style="min-width: 80px !important" >Belum Dikonfirmasi</button>
                                                                            @elseif($item->status == '1')
                                                                            <button type="button" class="btn btn-info btn-sm waves-effect waves-light" style="min-width: 80px !important" >Dikonfirmasi</button>
                                                                            @elseif($item->status == '2')
                                                                            <button type="button" class="btn btn-primary btn-sm waves-effect waves-light" style="min-width: 80px !important" >Sedang Diperbaiki</button>
                                                                            @elseif($item->status == '3')
                                                                            <button type="button" class="btn btn-success btn-sm waves-effect waves-light" style="min-width: 80px !important" >Selesai</button>
                                                                            @endif
                                                                    @endif
                                                                </td>
                                                                <td>{{$item->created_at->isoFormat('dddd, D MMMM Y')}}</td>
                                                                <td>
                                                                    @if (Session::get('role') === 'Admin')
                                                                    {{-- <form action="{{route('service.deletedata', $item->kd_service)}}" id="delete{{$item->kd_service}}" onsubmit="return confirm('yakin ingin menghapus?')" class="d-inline" method="POST">
                                                                    @method('delete')
                                                                    <button class="btn waves-effect waves-light btn-danger"><i class="fa fa-trash"></i></button>
                                                                    @csrf
                                                                    </form> --}}
                                                                        {{-- @if($item->status == "0")
                                                                        <a href="{{route('service.editdata', $item->kd_service)}}" data-toggle="tooltip" data-placement="right" title="Tambahan Service" class="btn waves-effect waves-light btn-primary"><i class="fa fa-edit"></i></a>
                                                                        @elseif($item->status == "1")
                                                                        <a href="{{route('service.editdata', $item->kd_service)}}" data-toggle="tooltip" data-placement="right" title="Tambahan Service" class="btn waves-effect waves-light btn-primary"><i class="fa fa-edit"></i></a>
                                                                        @elseif($item->status == "2")
                                                                        <a href="{{route('service.editdata', $item->kd_service)}}" data-toggle="tooltip" data-placement="right" title="Tambahan Service" class="btn waves-effect waves-light btn-primary"><i class="fa fa-edit"></i></a>
                                                                        @elseif($item->status == "3")
                                                                        <button  data-toggle="tooltip" data-placement="right" title="edit" class="btn waves-effect waves-light btn-primary"><i class="fa fa-edit"></i></button>
                                                                        @endif --}}
                                                                    <a href="{{route('service.detaildata', $item->kd_service)}}" data-toggle="tooltip" data-placement="right" title="detail" class="btn waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
                                                                    @else
                                                                    <a href="{{route('service.detaildata', $item->kd_service)}}" data-toggle="tooltip" data-placement="right" title="detail" class="btn waves-effect waves-light btn-primary"><i class="fa fa-eye"></i></a>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    {{-- <script>
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
          function changeStatus(kd_service, status) {
                event.preventDefault();
                console.log(kd_service, status);
                Swal.fire({
                    title: 'Apakan anda yakin?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya'
                }).then((result) => {
                    if (result.isConfirmed) {
                        console.log('hiyaa');
                        var url = '{{ url("admin/service/changestatus/") }}/'+kd_service;
                        console.log(url);
                        $.ajax({
                            url: url,
                            type: "POST",
                            data:{
                                status:status
                            },
                            success: function (data) {
                                console.log(data);
                                if(data.status == 1){
                                    location.reload();
                                }
                            },
                            error: function (error) {
                                console.log(error);
                                // swal({
                                //     title: 'Opps...',
                                //     text: error.message,
                                //     type: 'error',
                                //     timer: '1500'
                                // })
                            }
                        });
                    }
                });
            };
        </script> --}}
</body>

</html>
