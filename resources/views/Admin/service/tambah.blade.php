<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Service</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <img src="{{url('assets/images/customer-support.png')}}" alt="user image" class=" img-20 align-top "> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Service</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12 col-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h3 class="float-left">Tambah Service</h3>
                                                        <a href="{{route('service.list')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 float-right" style="padding:7px 10px !important">Kembali</a>                                                        
                                                    </div>
                                                    <div class="card-block px-5">
                                                        <form action="" method="" enctype="multipart/form-data">
                                                            <div class="form-group row">
                                                                <label class="">Nama Pelanggan</label>
                                                                @php
                                                                    $user= DB::table('users')->get();    
                                                                @endphp
                                                                <select name="user_id" id="user-select" class="form-control">
                                                                    <option selected>Pilih Nama Pelanggan</option>
                                                                    @foreach ($user as $item1)
                                                                    <option value="{{$item1->id}}">{{$item1->nama_lengkap}} {{$item1->id}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="">Kendaraan</label>
                                                                <select name="kendaraan_id" id="kendaraan-select" class="form-control">
                                                                    <option selected>Pilih Kendaraan</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="">Tanggal Service</label>
                                                                <input type="text" autocomplete="off" name="tanggal" id="tanggal-inp" class="form-control">
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="">Nomor Telepon</label>
                                                                <input type="text" class="form-control" id="no-telp-inp" name="no_telp">
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="">Catatan Service</label>
                                                                <textarea name="" class="form-control" cols="30" id="catatan-inp" rows="10"></textarea>
                                                            </div>
                                                             <button type="button" onclick="submitForm()" class="btn waves-effect waves-light btn-primary mb-3 float-right">Simpan Data</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12 col-12">
                                                <div class="card">
                                                    <div class="card-body px-5">
                                                        <div class="form-group row">
                                                            <div class="col-8">
                                                                <p class="h5 float-left font-weight-bold mb-3">Pilih Layanan & Sparepart</p>
                                                            </div>
                                                            <div class="col-4">
                                                                <a href="#" onclick="addService()" style="padding: 10px 10px; font-size:12px" class="btn btn-primary float-right">Tambah</a>
                                                            </div>
                                                        </div>
                                                        <div class="service-container">
                                                            <div class="form-group row">
                                                                <label class="">Layanan</label>
                                                                @php
                                                                $jasa = App\Model\Jasa::get();   
                                                                @endphp
                                                                <select name="jasa_id" id="jasa-select-0" data-id="0" class="form-control jasa-select">
                                                                    <option selected>Pilih Layanan</option>
                                                                    @foreach ($jasa as $item)
                                                                        <option value="{{$item->id}}">{{$item->nama_jasa}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="">Sparepart</label>
                                                                <select name="sparepart_id" data-id="0" id="sparepart-select-0" class="sparepart-select form-control">
                                                                    <option selected>Pilih Sparepart</option>
                                                                </select>
                                                            </div>
                                                            <hr class="text-primary" style="border-color:#69a1fe">
                                                        </div>
                                                        <div class="form-group">
                                                            <h5 class="text-left" >
                                                                Total = Rp.<span id="total-text">0</span>
                                                            </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="{{url('assets/js/service.js')}}"></script>
    <script>
        $('#tanggal-inp').datepicker({
            startDate: "now()",
            todayBtn: "linked",
            format: "yyyy-mm-dd",
        });

    $('#tanggal-inp').on('change', function(){
        let tanggal = $(this).val();
        console.log(tanggal);
        if(tanggal){
            getAntrian(tanggal);
        }
    });


    //get antrian
    const getAntrian = (tanggal) => {
        var url = '{{ route("checkout.getantrian") }}';
        $.ajax({
            url: url,
            type: "POST",
            data:{
                tanggal:tanggal
            },
            success: function (data) {
                if(data.antrian < 9 ){
                    Swal.fire({
                        icon: 'success',
                        title: data.antrian,
                        text: 'Nomor antrian Anda',
                    });
                    $('#submit-antrian').prop('disabled', false);
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Oppss',
                        text: 'Antrian sudah penuh, silahkan pilih hari lain',
                    });
                    $('#submit-antrian').prop('disabled', true);
                }
            },
            error: function (error) {
                console.log(error);
            }
        });
    }
    </script>
</body>

</html>
