<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Kendaraan</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-car"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Kendaraan</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="card">
                                            <div class="card-header">
                                                 <h5>Detail Kendaraan</h5>
                                                    <div class="card-header-right">
                                                        <a href="{{route('kendaraan.list')}}" class="btn waves-effect waves-light btn-primary btn-sm">Kembali</a>
                                                    </div>
                                            </div>
                                            <div class="card-block">
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Nama Pelanggan</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$kendaraan->userRef->nama_lengkap}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Merk Kendaraan</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$kendaraan->merks->nama_merk}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Model Kendaraan</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$kendaraan->models->nama_model}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Tahun Keluar</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$kendaraan->tahun_keluar}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Plat Nomor</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    <p class="card-text text-primary"> {{$kendaraan->plat_nomor}}</p>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">TransmLengkapi</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    @if($kendaraan != null)
                                                    <p class="card-text text-primary">Data Belum di Lengkapi</p>
                                                    @else
                                                    <p class="card-text text-primary"> {{$kendaraan->transmisi}}</p>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Bahan Bakar</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    @if($kendaraan != null)
                                                    <p class="card-text text-primary">Data Belum di Lengkapi</p>
                                                    @else
                                                    <p class="card-text text-primary"> {{$kendaraan->bahan_bakar}}</p>
                                                    @endif
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                        <p class="card-text font-weight-bold">Oli Mesin</p>
                                                    </div>
                                                    <div class="col-1">
                                                    :
                                                    </div>
                                                    <div class="col-8">
                                                    @if($kendaraan != null)
                                                    <p class="card-text text-primary">Data Belum di Lengkapi</p>
                                                    @else
                                                    <p class="card-text text-primary"> {{$kendaraan->oli_mesin}}</p>
                                                    @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
