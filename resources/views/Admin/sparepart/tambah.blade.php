<!DOCTYPE html>
<html lang="en">

<head>
    @include('Admin.layouts.head')
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="preloader-wrapper">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            {{-- Navbar --}}
            @include('Admin.layouts.navbar')
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    {{-- Sidebar --}}
                    @include('Admin.layouts.sidebar')
                    <div class="pcoded-content">
                        <!-- Page-header start -->
                        <div class="page-header">
                            <div class="page-block">
                                <div class="row align-items-center">
                                    <div class="col-md-8">
                                        <div class="page-header-title">
                                            <h5 class="m-b-10">Sparepart</h5>
                                            <p class="m-b-0">Lorem Ipsum is simply dummy text of the printing</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="index.html"> <i class="fa fa-cogs"></i> </a>
                                            </li>
                                            <li class="breadcrumb-item"><a href="#!">Sparepart</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Page-header end -->
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                            <h3 class="float-left">Tambah Data Sparepart</h3>
                                                            <a href="{{route('Sparepart.list')}}" class="btn waves-effect waves-light btn-primary btn-outline-primary mb-3 float-right" style="padding:7px 10px !important">Kembali</a>                                                      
                                                    </div>
                                                    <div class="card-block">
                                                        <form action="{{route('Sparepart.simpandata')}}" method="POST" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('post')
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Nama Sparepart</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Sparepart">
                                                                    @if ($errors->has('nama'))
                                                                    <span class="text-danger">{{ $errors->first('nama') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Kategori</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $kategori = DB::table('kategori')->get();    
                                                                    @endphp
                                                                    <select name="kategori_id" class="form-control">
                                                                        <option selected>Pilih Kategori</option>
                                                                        @foreach ($kategori as $item)
                                                                        <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('kategori_id'))
                                                                    <span class="text-danger">{{ $errors->first('kategori_id') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Merk</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $merk = DB::table('merk')->get();    
                                                                    @endphp
                                                                    <select name="merk_id" class="form-control">
                                                                        <option selected>Pilih Merk</option>
                                                                        @foreach ($merk as $item)
                                                                            <option value="{{$item->id}}">{{$item->nama_merk}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('merk_id'))
                                                                    <span class="text-danger">{{ $errors->first('merk_id') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Jasa</label>
                                                                <div class="col-sm-10">
                                                                    @php
                                                                    $jasa = DB::table('jasa')->get();    
                                                                    @endphp
                                                                    <select name="jasa_id" class="form-control">
                                                                        <option selected>Pilih jasa</option>
                                                                        @foreach ($jasa as $item)
                                                                        <option value="{{$item->id}}">{{$item->nama_jasa}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('jasa_id'))
                                                                    <span class="text-danger">{{ $errors->first('jasa_id') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Harga Jual</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="harga_jual" placeholder="Masukan Harga Jual">
                                                                    @if ($errors->has('harga_jual'))
                                                                    <span class="text-danger">{{ $errors->first('harga_jual') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Harga Beli</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="harga_beli" placeholder="Masukan Harga Beli">
                                                                    @if ($errors->has('harga_beli'))
                                                                    <span class="text-danger">{{ $errors->first('harga_beli') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Stok</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="stok" placeholder="Masukan Stok">
                                                                    @if ($errors->has('stok'))
                                                                        <span class="text-danger">{{ $errors->first('stok') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Photo</label>
                                                                <div class="col-sm-10">
                                                                    <input type="file" class="form-control" name="photo" >
                                                                    @if ($errors->has('photo'))
                                                                        <span class="text-danger">{{ $errors->first('photo') }}</span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                             <button type="submit" class="btn waves-effect waves-light btn-primary mb-3 float-right">Simpan </button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="styleSelector"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Required Jquery -->
    @include('Admin.layouts.js')
    
</body>

</html>
