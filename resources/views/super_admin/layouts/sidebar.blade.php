<nav class="pcoded-navbar">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
    <div class="pcoded-inner-navbar main-menu">
        <div class="">
            <div class="main-menu-header">
                <img class="img-80 img-radius" src="{{url('assets/images/avatar-4.jpg')}}" alt="User-Profile-Image">
                <div class="user-details">
                    <span id="more-details">John Doe<i class="fa fa-caret-down"></i></span>
                </div>
            </div>
            <div class="main-menu-content">
                <ul>
                    <li class="more-details">
                        <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                        <a href="#!"><i class="ti-settings"></i>Settings</a>
                        <a href="auth-normal-sign-in.html"><i class="ti-layout-sidebar-left"></i>Logout</a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="pcoded-item pcoded-left-item mt-4">
            <li class="">
                <a href="{{url('admin/dashboard')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                    <span class="pcoded-mtext">Dashboard</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{url('user/list')}}" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-user-secret"></i><b>D</b></span>
                    <span class="pcoded-mtext">Admin</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="index.html" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-cogs"></i><b>D</b></span>
                    <span class="pcoded-mtext">Sparepart</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="index.html" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-users"></i><b>D</b></span>
                    <span class="pcoded-mtext">Jasa</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="index.html" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-car"></i><b>D</b></span>
                    <span class="pcoded-mtext">Antrian</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="index.html" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-cogs"></i><b>D</b></span>
                    <span class="pcoded-mtext">Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="index.html" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="fa fa-money"></i><b>D</b></span>
                    <span class="pcoded-mtext">Transaksi Service</span>
                    <span class="pcoded-mcaret"></span>
                </a>
            </li>
        </ul>
    </div>
</nav>