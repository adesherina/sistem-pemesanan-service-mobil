<script type="text/javascript" src="{{url('assets/js/jquery/jquery.min.js ')}}"></script>
<script type="text/javascript" src="{{url('assets/js/jquery-ui/jquery-ui.min.js')}} "></script>
<script type="text/javascript" src="{{url('assets/js/popper.js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets/js/bootstrap/js/bootstrap.min.js')}} "></script>
<!-- waves js -->
<script src="{{url('assets/pages/waves/js/waves.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{url('assets/js/jquery-slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{url('assets/js/pcoded.min.js')}}"></script>
<script src="{{url('assets/js/vertical/vertical-layout.min.js')}}"></script>
<script src="{{url('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- Custom js -->
<script type="text/javascript" src="{{url('assets/js/script.min.js')}}"></script>