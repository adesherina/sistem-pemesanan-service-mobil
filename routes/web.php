<?php

use FontLib\Table\Type\name;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//==========================USER=====================
//home
Route::get('/', 'user\LandingPageController@index');
//tentang kami
Route::get('tentangKami', 'user\LandingPageController@about');
//services
Route::get('landing/services', 'user\LandingPageController@service')->name('landing.service');
//kontak
Route::get('kontak', 'user\LandingPageController@kontak')->name('user.landing.kontak');
Route::post('kontak/ks/simpan', 'user\LandingPageController@ks')->name('user.ks.simpan');
//login
Route::get('login', 'user\LoginController@index')->name('user.login');
Route::post('login', 'user\LoginController@login')->name('user.login.process');
//logout
Route::get('logout', 'user\LoginController@logout')->name('user.logout');

//register
Route::get('register', 'user\RegisterController@index');
Route::post('register', 'user\RegisterController@addProcess')->name('user.register.process');

//OTP
Route::group(['prefix' => 'otp', 'namespace' => 'user'], function () {
    Route::get('/{no_telp}', 'OTPController@index')->name('otp');
    Route::post('/{no_telp}/verify', 'OTPController@verifyOtp')->name('otp.verify');
});

//dashboard
Route::get('dashboard', 'user\DashboardController@index')->middleware('CheckLoginUser');

//profil
Route::get('user/profil/{id}', 'user\ProfilController@index')->name('user.profil')->middleware('CheckLoginUser');
Route::patch('user/profil/{id}', 'user\ProfilController@updateProcess')->name('user.update.proses');

//update password
Route::get('user/pw/{id}', 'user\ProfilController@updatePw')->name('user.update.pw')->middleware('CheckLoginUser');
Route::patch('user/pw/ubah/{id}', 'user\ProfilController@updatePassword')->name('user.updatePassword');


//SMS
Route::get('kirimSMS', 'TwilioSMSController@index')->name('user.kirim.sms')->middleware('CheckLoginUser');

//forgotPW
Route::group(['prefix' => 'forgotpw', 'namespace' => 'user'], function () {
    Route::get('/forgotpw', 'ForgotPWController@index')->name('user.forgotpw');
    Route::post('/forgot/noTelp', 'ForgotPWController@cariTelepon')->name('forgotpw.noTelp');
    Route::patch('/forgot/password', 'ForgotPWController@forgotpw')->name('forgot.password');
});

// Route::get('user/forgotpw', 'user\ForgotPWController@index')->name('user.forgotpw');

//Garasi
Route::group(['prefix' => 'garasi', 'namespace' => 'user'], function () {
    Route::get('/', 'KendaraanController@index')->name('garasi');
    Route::get('/tambah', 'KendaraanController@tambah')->name('garasi.tambahdata')->middleware('CheckLoginUser');
    Route::post('/getModel', 'KendaraanController@getModel')->name('garasi.getmodel')->middleware('CheckLoginUser');
    Route::post('/simpan', 'KendaraanController@simpan')->name('garasi.simpandata');
    Route::get('/{id}/edit', 'KendaraanController@edit')->name('garasi.editdata')->middleware('CheckLoginUser');
    Route::patch('/{id}', 'KendaraanController@update')->name('garasi.updatedata');
    Route::get('/delete/{id}', 'KendaraanController@delete')->name('garasi.deletedata');
    Route::get('/detail/{id}', 'KendaraanController@detail')->name('garasi.detaildata')->middleware('CheckLoginUser');
});

//Service
Route::group(['prefix' => 'services', 'namespace' => 'user'], function () {
    Route::get('/', 'ServiceController@index')->name('services')->middleware('CheckLoginUser');
    Route::get('/tambah', 'ServiceController@tambah')->name('services.tambah')->middleware('CheckLoginUser');
    Route::post('/getservice', 'ServiceController@getService')->name('services.getservice');
    Route::post('/getsparepart', 'ServiceController@getSparepart')->name('services.sparepart');
    Route::post('/save', 'ServiceController@save')->name('user.services.save');
});

//checkout
Route::group(['prefix' => 'checkout', 'namespace' => 'user'], function () {
    Route::get('/detail/{kd_service}', 'CheckoutController@detail')->name('checkout.detail');
    Route::get('/aktif', 'CheckoutController@servicesaktif')->name('checkout.aktif');
    Route::get('/invoice/{kd_service}', 'CheckoutController@invoice')->name('checkout.invoice');
    Route::get('invoice/cetak/{kd_service}', 'CheckoutController@cetakInvoice')->name('chechout.cetak.invoice');
    Route::get('/save/{kd_service}', 'CheckoutController@index')->name('checkout');
    Route::post('/save/{kd_service}', 'CheckoutController@save')->name('checkout.save');
    Route::post('/getantrian', 'CheckoutController@getAntrian')->name('checkout.getantrian');
    Route::get('/delete/{kd_service}', 'CheckoutController@delete')->name('checkout.deletedata');
});

//==============================ADMIN============================

Route::group(['prefix' => 'admin', 'namespace' => 'admin'], function () {
    Route::get('/register', 'RegisterController@index')->name('admin.register');
    Route::post('/register/add', 'RegisterController@addProcess')->name('admin.register.addProses');
    Route::get('/login', 'LoginController@index')->name('admin.login');
    Route::post('/login/proses', 'LoginController@login')->name('admin.login.proses');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');


//Service
Route::group(['prefix' => 'service'], function () {
    Route::get('/list', 'ServiceController@index')->middleware('CheckLoginAdmin')->name('service.list');
    Route::get('/tambah', 'ServiceController@tambah')->middleware('CheckLoginAdmin')->name('service.tambahdata');
    Route::post('/save', 'ServiceController@simpan')->name('sservice.simpandata');
    Route::post('/changestatus/{kd_service}', 'ServiceController@changeStatus')->name('service.changestatus');
    Route::get('/edit/{kd_service}', 'ServiceController@edit')->name('service.editdata');
    Route::post('/{kd_service}', 'ServiceController@update')->name('service.updatedata');
    Route::delete('/delete/{kd_service}', 'ServiceController@delete')->name('service.deletedata');
    Route::get('/detail/{kd_service}', 'ServiceController@detail')->middleware('CheckLoginAdmin')->name('service.detaildata');

    Route::post('/user/getcar', 'ServiceController@getCar')->name('admin.service.getcar');
    Route::get('/user/getalljasa', 'ServiceController@getAllJasa')->name('admin.service.getalljasa');
    Route::post('/user/getbyjasa', 'ServiceController@getJasaById')->name('admin.service.getjasabyid');
    Route::post('/user/getbysparepart', 'ServiceController@getSparepartById')->name('admin.service.getsparepartbyid');
});


});

//forgotPW
Route::group(['prefix' => 'forgotpw', 'namespace' => 'admin'], function () {
    Route::get('/', 'ForgotPWController@index')->name('admin.forgotpw');
    Route::post('/forgot/noTelp', 'ForgotPWController@cariTelepon')->name('admin.forgotpw.noTelp');
    Route::patch('/forgot/password', 'ForgotPWController@forgotpw')->name('admin.forgot.password');
});

//dashboard
Route::get('admin/dashboard', 'admin\LoginController@dashboard')->name('dashboard')->middleware('CheckLoginAdmin');

//profil
Route::get('admin/profil/{id}', 'admin\ProfilController@index')->name('admin.update')->middleware('CheckLoginAdmin');
Route::patch('admin/profil/{id}', 'admin\ProfilController@updateProcess')->name('admin.update.proses');

//update password
Route::get('admin/pw/{id}', 'admin\ProfilController@updatePw')->name('admin.update.pw')->middleware('CheckLoginAdmin');
Route::patch('admin/pw/ubah/{id}', 'admin\ProfilController@updatePassword')->name('admin.updatePassword');

//user
Route::group(['prefix' => 'user', 'namespace' => 'admin'], function () {
    Route::get('/list', 'UserController@index')->name('user.list')->middleware('CheckLoginAdmin');
    Route::get('/Tambah', 'UserController@tambah')->name('user.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'UserController@simpan')->name('user.simpandata');
    Route::delete('/delete/{id}', 'UserController@delete')->name('user.deletedata');
    Route::get('/detail/{id}', 'UserController@detail')->name('user.detaildata')->middleware('CheckLoginAdmin');
});

//admin
Route::group(['prefix' => 'admins', 'namespace' => 'admin'], function () {
    Route::get('/list', 'AdminController@index')->name('admin.list')->middleware('CheckLoginAdmin');
    Route::get('/Tambah', 'AdminController@tambah')->name('admin.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'AdminController@simpan')->name('admin.simpandata');
    Route::delete('/delete/{id}', 'AdminController@delete')->name('admin.deletedata');
    Route::get('/detail/{id}', 'AdminController@detail')->name('admin.detaildata')->middleware('CheckLoginAdmin');
});

//kritik saran
Route::group(['prefix' => 'kritikSaran', 'namespace' => 'admin'], function () {
    Route::get('/list', 'KritikSaranController@index')->name('ks.list')->middleware('CheckLoginAdmin');
    Route::delete('/delete/{id}', 'KritikSaranController@delete')->name('ks.deletedata');
    Route::get('/detail/{id}', 'KritikSaranController@detail')->name('ks.detaildata')->middleware('CheckLoginAdmin');
});

//sparepart
Route::group(['prefix' => 'sparepart', 'namespace' => 'admin'], function () {
    Route::get('/list', 'SparepartController@index')->name('Sparepart.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'SparepartController@tambah')->name('Sparepart.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'SparepartController@simpan')->name('Sparepart.simpandata');
    Route::get('/{id}/edit', 'SparepartController@edit')->name('Sparepart.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'SparepartController@update')->name('Sparepart.updatedata');
    Route::delete('/delete/{id}', 'SparepartController@delete')->name('Sparepart.deletedata');
    Route::get('/detail/{id}', 'SparepartController@detail')->name('Sparepart.detaildata')->middleware('CheckLoginAdmin');
});

//kategori sparepart
Route::group(['prefix' => 'kategorisparepart', 'namespace' => 'admin'], function () {
    Route::get('/list', 'KategoriSparepartController@index')->name('kategori.sparepart.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'KategoriSparepartController@tambah')->name('kategori.sparepart.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'KategoriSparepartController@simpan')->name('kategori.sparepart.simpandata');
    Route::get('/{id}/edit', 'KategoriSparepartController@edit')->name('kategori.sparepart.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'KategoriSparepartController@update')->name('kategori.sparepart.updatedata');
    Route::delete('/delete/{id}', 'KategoriSparepartController@delete')->name('kategori.sparepart.deletedata');
});

//Merk sparepart
Route::group(['prefix' => 'merksparepart', 'namespace' => 'admin'], function () {
    Route::get('/list', 'MerkSparepartController@index')->name('merk.sparepart.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'MerkSparepartController@tambah')->name('merk.sparepart.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'MerkSparepartController@simpan')->name('merk.sparepart.simpandata');
    Route::get('/{id}/edit', 'MerkSparepartController@edit')->name('merk.sparepart.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'MerkSparepartController@update')->name('merk.sparepart.updatedata');
    Route::delete('/delete/{id}', 'MerkSparepartController@delete')->name('merk.sparepart.deletedata');
});

//Jasa
Route::group(['prefix' => 'jasa', 'namespace' => 'admin'], function () {
    Route::get('/list', 'JasaController@index')->name('jasa.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'JasaController@tambah')->name('jasa.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'JasaController@simpan')->name('jasa.simpandata');
    Route::post('/changestatusaktif/{id}', 'JasaController@changeStatusAktif')->name('jasa.changestatus');
    Route::post('/changestatustidakaktif/{id}', 'JasaController@changeStatusTAktif')->name('jasa.changestatust.tidakaktif');
    Route::get('/edit/{id}', 'JasaController@edit')->name('jasa.editdata')->middleware('CheckLoginAdmin');
    Route::post('/{id}', 'JasaController@update')->name('jasa.updatedata');
    Route::delete('/delete/{id}', 'JasaController@delete')->name('jasa.deletedata');
});

//Kategori Jasa
Route::group(['prefix' => 'kategorijasa', 'namespace' => 'admin'], function () {
    Route::get('/list', 'KategoriJasaController@index')->name('kategori.jasa.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'KategoriJasaController@tambah')->name('kategori.jasa.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'KategoriJasaController@simpan')->name('kategori.jasa.simpandata');
    Route::get('/edit/{id}', 'KategoriJasaController@edit')->name('kategori.jasa.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'KategoriJasaController@update')->name('kategori.jasa.updatedata');
    Route::delete('/delete/{id}', 'KategoriJasaController@delete')->name('kategori.jasa.deletedata');
});

//Merk Kendaraan
Route::group(['prefix' => 'merkkendaraan', 'namespace' => 'admin'], function () {
    Route::get('/list', 'MerkKendaraanController@index')->name('merk.kendaraan.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'MerkKendaraanController@tambah')->name('merk.kendaraan.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'MerkKendaraanController@simpan')->name('merk.kendaraan.simpandata');
    Route::get('/{id}/edit', 'MerkKendaraanController@edit')->name('merk.kendaraan.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'MerkKendaraanController@update')->name('merk.kendaraan.updatedata');
    Route::delete('/delete/{id}', 'MerkKendaraanController@delete')->name('merk.kendaraan.deletedata');
});

//Model Kendaraan
Route::group(['prefix' => 'modelkendaraan', 'namespace' => 'admin'], function () {
    Route::get('/list', 'ModelKendaraanController@index')->name('model.kendaraan.list')->middleware('CheckLoginAdmin');
    Route::get('/tambah', 'ModelKendaraanController@tambah')->name('model.kendaraan.tambahdata')->middleware('CheckLoginAdmin');
    Route::post('/Simpan', 'ModelKendaraanController@simpan')->name('model.kendaraan.simpandata');
    Route::get('/{id}/edit', 'ModelKendaraanController@edit')->name('model.kendaraan.editdata')->middleware('CheckLoginAdmin');
    Route::patch('/{id}', 'ModelKendaraanController@update')->name('model.kendaraan.updatedata');
    Route::delete('/delete/{id}', 'ModelKendaraanController@delete')->name('model.kendaraan.deletedata');
});

//kendaraan
Route::group(['prefix' => 'kendaraan', 'namespace' => 'admin'], function () {
    Route::get('/list', 'KendaraanController@index')->name('kendaraan.list')->middleware('CheckLoginAdmin');
    Route::delete('/delete/{id}', 'KendaraanController@delete')->name('kendaraan.deletedata');
    Route::get('/detail/{id}', 'KendaraanController@detail')->name('kendaraan.detaildata')->middleware('CheckLoginAdmin');
});


//Antrian
Route::group(['prefix' => 'antrian', 'namespace' => 'admin'], function () {
    Route::get('/list', 'AntrianController@index')->name('antrian.list');
    Route::get('/tambah', 'AntrianController@tambah')->name('antrian.tambahdata');
    Route::post('/Simpan', 'AntrianController@simpan')->name('antrian.simpandata');
    Route::get('/edit', 'AntrianController@edit')->name('antrian.editdata');
    Route::post('/{id}', 'AntrianController@update')->name('antrian.updatedata');
    Route::delete('/delete/{id}', 'AntrianController@delete')->name('antrian.deletedata');
    Route::get('/detail/{id}', 'AntrianController@detail')->name('antrian.detaildata');
    Route::get('/list/atur', 'AntrianController@aturantrian')->name('atur.antrian.list');
    Route::get('/list/atur/proses', 'AntrianController@aturAntrianProses')->name('atur.antrian.proses');
    Route::get('/list/atur/selesai/{id_antrian}', 'AntrianController@aturAntrianSelesai')->name('atur.antrian.selesai');
});


//Transaksi
Route::group(['prefix' => 'transaksi', 'namespace' => 'admin'], function () {
    Route::get('/list', 'TransaksiController@index')->name('transaksi.list');
    Route::get('/tambah', 'TransaksiController@tambah')->name('transaksi.tambahdata');
    Route::post('/Simpan', 'TransaksiController@simpan')->name('transaksi.simpandata');
    Route::post('/changestatus/{nota}', 'TransaksiController@changeStatus')->name('transaksi.changestatus');
    Route::delete('/delete/{id}', 'TransaksiController@delete')->name('transaksi.deletedata');
    Route::get('/detail/{nota}', 'TransaksiController@detail')->name('transaksi.detaildata');
    //lunas
    Route::get('/laporan/lunas', 'TransaksiController@laporanLunas')->name('transaksi.laporan.lunas');
    Route::get('cari-laporan-lunas','TransaksiController@cariLunas')->name('cari.laporan.lunas');
    Route::get('export-laporan-lunas','TransaksiController@exportLapLunas')->name('export.laporan.lunas');
    //belum lunas
    Route::get('/laporan/belum/lunas', 'TransaksiController@laporanBLunas')->name('transaksi.laporan.belum.lunas');
    Route::get('cari-laporan-belum-lunas','TransaksiController@cariBLunas')->name('cari.laporan.belum.lunas');
    Route::get('export-laporan-belum-lunas','TransaksiController@exportLapBLunas')->name('export.laporan.belum.lunas');
    //keseluruhan
    Route::get('/laporan', 'TransaksiController@laporan')->name('transaksi.laporan');
    Route::get('cari-laporan','TransaksiController@cari')->name('cari.laporan');
    Route::get('export-laporan','TransaksiController@exportLap')->name('export.laporan');
});


//==============================SUPER ADMIN=============================================
//register
Route::get('super_admin/register', 'super_admin\RegisterController@index');
Route::post('super_admin/register/add', 'super_admin\RegisterController@addProcess')->name('register');

//dashboard
Route::get('super_admin/dashboard', 'super_admin\RegisterController@dashboard')->name('dashboard');