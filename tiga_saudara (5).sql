-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 03, 2021 at 03:16 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tiga_saudara`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('Admin','Super Admin') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `no_telp`, `code`, `nama_lengkap`, `alamat`, `jenis_kelamin`, `photo`, `username`, `password`, `created_at`, `updated_at`, `role`) VALUES
(1, '082115806156', NULL, 'Nursuci', 'Indramayu', 'Perempuan', 'girl.png', 'admin', '$2y$10$tXTkpV1/4zlQ62VAA/f0X.EwQo1QAjdljzaI2d/.mHlfSbg4IK2jG', '2021-06-09 22:15:04', '2021-06-09 22:15:04', 'Admin'),
(2, '0898299091', NULL, 'Ayip', 'Bangkaloa', 'Laki-laki', 'images.png', 'superadmin', '$2y$10$Tbl7aFF6xnWnnfDyoeG5wOfqs.zUcyWHznnBeWimkj6vE/H8vSGxu', '2021-07-15 03:09:34', '2021-07-15 03:09:34', 'Super Admin');

-- --------------------------------------------------------

--
-- Table structure for table `antrian`
--

CREATE TABLE `antrian` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kd_service` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_antrian` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('Menunggu','Proses','Selesai') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `antrian`
--

INSERT INTO `antrian` (`id`, `kd_service`, `no_antrian`, `status`, `tanggal`, `created_at`, `updated_at`) VALUES
(50, 'PPWR-0EEJ-WLE0', '1', 'Selesai', '2021-07-22', '2021-07-22 04:32:32', '2021-07-22 07:08:15'),
(55, 'P08E-GCJU-XPY4', '1', 'Selesai', '2021-07-27', '2021-07-27 03:57:04', '2021-07-27 03:57:04'),
(62, 'ZPYE-7WMH-WA05', '5', 'Selesai', '2021-08-14', '2021-08-14 06:53:55', '2021-08-14 06:53:55'),
(63, 'YCBK-PJJ1-AAT1', '6', 'Selesai', '2021-08-14', '2021-08-14 15:07:00', '2021-08-14 15:07:00'),
(67, 'XEEY-BHZ2-FI7T', '1', 'Selesai', '2021-08-19', '2021-08-19 06:42:58', '2021-08-19 06:42:58'),
(68, 'HU0F-6J3S-FUCB', '1', 'Selesai', '2021-08-20', '2021-08-20 00:08:38', '2021-08-20 00:08:38'),
(69, 'B1AU-U1BU-M23Q', '2', 'Selesai', '2021-08-20', '2021-08-20 00:11:27', '2021-08-20 00:11:27'),
(70, '9U6B-70F4-FMBF', '3', 'Selesai', '2021-08-20', '2021-08-20 00:13:54', '2021-08-20 00:13:54'),
(71, 'Y4KL-1G1N-YCR1', '4', 'Selesai', '2021-08-20', '2021-08-20 00:16:13', '2021-08-20 00:16:13'),
(72, 'IBE3-KF5S-5ZFB', '5', 'Selesai', '2021-08-20', '2021-08-20 00:17:31', '2021-08-20 00:17:31'),
(73, '6KPM-XCU5-2FK3', '6', 'Selesai', '2021-08-20', '2021-08-20 00:18:46', '2021-08-20 00:18:46'),
(74, 'S09R-630Y-0Z72', '7', 'Selesai', '2021-08-20', '2021-08-20 00:21:37', '2021-08-20 00:21:37'),
(75, 'LJ5S-DCRZ-NGQR', '8', 'Selesai', '2021-08-20', '2021-08-20 00:33:04', '2021-08-20 00:33:04'),
(76, '808D-PR0A-LDIP', '1', 'Selesai', '2021-08-22', '2021-08-22 04:40:22', '2021-08-22 05:27:52'),
(77, 'JNSN-WWU4-GB4D', '1', 'Selesai', '2021-08-24', '2021-08-22 05:27:15', '2021-08-22 05:27:15'),
(78, 'Z17K-Q12K-RT7G', '2', 'Selesai', '2021-08-22', '2021-08-22 05:35:27', '2021-08-22 05:36:25'),
(79, 'J9JX-K8G2-20Y9', '1', 'Menunggu', '2021-09-02', '2021-09-02 03:43:40', '2021-09-02 03:43:40'),
(80, 'M98E-RPHH-QEP1', '1', 'Proses', '2021-09-03', '2021-09-03 03:41:21', '2021-09-03 06:30:09'),
(81, 'NTUY-NRSP-R9YX', '2', 'Menunggu', '2021-09-03', '2021-09-03 06:23:01', '2021-09-03 06:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `detail_service`
--

CREATE TABLE `detail_service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kd_service` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sparepart_id` bigint(20) UNSIGNED DEFAULT NULL,
  `jasa_id` bigint(20) UNSIGNED NOT NULL,
  `subtotal` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_service`
--

INSERT INTO `detail_service` (`id`, `kd_service`, `sparepart_id`, `jasa_id`, `subtotal`, `created_at`, `updated_at`) VALUES
(55, 'PPWR-0EEJ-WLE0', 6, 4, 155000, '2021-07-22 04:32:32', '2021-07-22 04:32:32'),
(61, 'P08E-GCJU-XPY4', NULL, 5, 80000, '2021-07-27 03:57:04', '2021-07-27 03:57:04'),
(71, 'ZPYE-7WMH-WA05', 25, 12, 171000, '2021-08-14 06:53:55', '2021-08-14 06:53:55'),
(72, 'YCBK-PJJ1-AAT1', 16, 2, 555000, '2021-08-14 15:07:00', '2021-08-14 15:07:00'),
(77, 'XEEY-BHZ2-FI7T', 19, 4, 771000, '2021-08-19 06:42:58', '2021-08-19 06:42:58'),
(78, 'HU0F-6J3S-FUCB', NULL, 1, 345000, '2021-08-20 00:08:38', '2021-08-20 00:08:38'),
(79, 'B1AU-U1BU-M23Q', 20, 6, 228000, '2021-08-20 00:11:26', '2021-08-20 00:11:26'),
(80, '9U6B-70F4-FMBF', 27, 3, 470000, '2021-08-20 00:13:54', '2021-08-20 00:13:54'),
(81, '9U6B-70F4-FMBF', 25, 12, 171000, '2021-08-20 00:13:54', '2021-08-20 00:13:54'),
(82, 'Y4KL-1G1N-YCR1', 18, 11, 136000, '2021-08-20 00:16:13', '2021-08-20 00:16:13'),
(83, 'IBE3-KF5S-5ZFB', 8, 5, 155000, '2021-08-20 00:17:31', '2021-08-20 00:17:31'),
(84, '6KPM-XCU5-2FK3', NULL, 1, 345000, '2021-08-20 00:18:46', '2021-08-20 00:18:46'),
(85, 'S09R-630Y-0Z72', 27, 3, 470000, '2021-08-20 00:21:37', '2021-08-20 00:21:37'),
(87, 'LJ5S-DCRZ-NGQR', NULL, 1, 345000, '2021-08-20 00:33:04', '2021-08-20 00:33:04'),
(88, '808D-PR0A-LDIP', NULL, 1, 350000, '2021-08-22 04:40:22', '2021-08-22 04:40:22'),
(89, 'JNSN-WWU4-GB4D', 16, 2, 555000, '2021-08-22 05:27:15', '2021-08-22 05:27:15'),
(90, 'Z17K-Q12K-RT7G', NULL, 1, 350000, '2021-08-22 05:35:26', '2021-08-22 05:35:26'),
(91, 'Z17K-Q12K-RT7G', 27, 3, 470000, '2021-08-22 05:35:26', '2021-08-22 05:35:26'),
(92, 'Z17K-Q12K-RT7G', 26, 2, 605000, '2021-08-22 05:35:27', '2021-08-22 05:35:27'),
(93, 'J9JX-K8G2-20Y9', NULL, 1, 350000, '2021-09-02 03:43:40', '2021-09-02 03:43:40'),
(94, 'J9JX-K8G2-20Y9', NULL, 13, 200000, '2021-09-02 03:43:40', '2021-09-02 03:43:40'),
(95, 'M98E-RPHH-QEP1', 8, 5, 155000, '2021-09-03 03:41:21', '2021-09-03 03:41:21'),
(96, 'NTUY-NRSP-R9YX', 16, 2, 555000, '2021-09-03 06:23:01', '2021-09-03 06:23:01'),
(97, 'NTUY-NRSP-R9YX', NULL, 1, 350000, '2021-09-03 06:23:01', '2021-09-03 06:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `jasa`
--

CREATE TABLE `jasa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_jasa` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_jasa` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `estimasi` int(11) NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jasa`
--

INSERT INTO `jasa` (`id`, `nama_jasa`, `harga_jasa`, `status`, `estimasi`, `kategori_id`, `created_at`, `updated_at`) VALUES
(1, 'Tone Up', 350000, 1, 60, 2, NULL, '2021-08-25 09:47:12'),
(2, 'Master Rem', 120000, 1, 60, 3, NULL, '2021-08-22 04:00:49'),
(3, 'Ganti Kampas Rem', 320000, 1, 55, 3, NULL, '2021-08-22 04:39:13'),
(4, 'Ganti Aki', 80000, 0, 50, 2, '2021-06-17 16:18:13', '2021-06-17 16:18:13'),
(5, 'Ganti Oli Transmisi', 80000, 1, 60, 4, '2021-06-17 16:19:08', '2021-09-03 03:40:24'),
(6, 'Ganti Oli Gardan', 70000, 0, 40, 2, '2021-07-22 06:35:20', '2021-08-21 10:17:05'),
(7, 'Ganti Kopling', 200000, 1, 70, 4, '2021-08-02 13:12:57', '2021-09-03 03:40:05'),
(8, 'Ganti Oli Mesin', 100000, 0, 30, 2, '2021-08-02 13:15:52', '2021-08-02 13:15:52'),
(9, 'Ganti Busi', 40000, 0, 20, 2, '2021-08-02 22:55:44', '2021-08-02 22:55:44'),
(11, 'Ganti Air Radiator', 45000, 0, 45, 2, '2021-08-02 23:00:10', '2021-08-02 23:00:10'),
(12, 'Minyak Rem', 100000, 0, 20, 3, '2021-08-02 23:40:02', '2021-08-02 23:40:02'),
(13, 'Pembersihan Mesin Injektor/Kalbulator', 200000, 1, 30, 2, '2021-08-14 15:24:46', '2021-08-25 09:38:30'),
(15, 'Kampas Kopling', 250000, 0, 60, 4, '2021-08-21 09:49:53', '2021-08-21 09:49:53');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Aki', '2021-06-17 15:56:41', '2021-06-17 15:56:41'),
(2, 'Oli', '2021-06-17 15:56:55', '2021-08-02 13:30:54'),
(3, 'Rem', '2021-06-17 15:57:15', '2021-08-02 23:24:04'),
(4, 'Radiator', '2021-06-17 15:58:28', '2021-06-17 15:58:28'),
(7, 'Busi', '2021-08-02 22:46:17', '2021-08-02 22:46:17'),
(8, 'Kopling', '2021-08-02 23:22:49', '2021-08-02 23:22:49');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_jasa`
--

CREATE TABLE `kategori_jasa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategori_jasa`
--

INSERT INTO `kategori_jasa` (`id`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(2, 'Service Mesin', '2021-06-09 22:24:33', '2021-06-09 22:24:33'),
(3, 'Rem', '2021-06-09 22:26:15', '2021-06-09 22:26:15'),
(4, 'Transmisi & Kopling', '2021-06-17 16:15:57', '2021-06-17 16:15:57');

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE `kendaraan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `merk_id` bigint(20) UNSIGNED NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `tahun_keluar` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `plat_nomor` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kilometer` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bahan_bakar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `oli_mesin` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`id`, `user_id`, `merk_id`, `model_id`, `tahun_keluar`, `plat_nomor`, `kilometer`, `bahan_bakar`, `oli_mesin`, `created_at`, `updated_at`) VALUES
(5, 8, 1, 2, '2015', 'E5482RZ', '170000', 'Pertamina Pertamax', 'Regular', '2021-06-16 20:39:04', '2021-06-28 03:28:58'),
(6, 8, 2, 3, '2015', 'E25679PAY', NULL, NULL, NULL, '2021-06-27 06:20:34', '2021-06-27 06:20:34'),
(9, 10, 1, 2, '2019', 'E236YT', NULL, NULL, NULL, '2021-07-07 03:14:36', '2021-07-07 03:14:36'),
(15, 13, 1, 7, '2018', 'E3678IJ', NULL, NULL, NULL, '2021-08-20 00:12:03', '2021-08-20 00:12:03'),
(16, 15, 3, 12, '2017', 'E5481RZ', NULL, NULL, NULL, '2021-08-20 00:15:37', '2021-08-20 00:15:37'),
(17, 20, 3, 5, '2020', 'E2457RK', NULL, NULL, NULL, '2021-08-20 00:16:56', '2021-08-20 00:16:56'),
(18, 21, 2, 10, '2020', 'E5789YK', NULL, NULL, NULL, '2021-08-20 00:18:21', '2021-08-20 00:18:21'),
(19, 21, 1, 6, '2018', 'B2789IK', NULL, NULL, NULL, '2021-08-20 00:20:52', '2021-08-20 00:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `kritik_saran`
--

CREATE TABLE `kritik_saran` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_lengkap` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` char(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ks` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kritik_saran`
--

INSERT INTO `kritik_saran` (`id`, `nama_lengkap`, `no_telp`, `ks`, `created_at`, `updated_at`) VALUES
(1, 'Ade Sherina', '082115806156', 'Pelayanan Sangat Ramah', '2021-07-16 02:05:49', '2021-07-16 02:05:49'),
(2, 'Yuni', '080218297828', 'Sangat Menarik', '2021-07-16 02:06:42', '2021-07-16 02:06:42'),
(3, 'Anita', '08762543156', 'Pelayannya Kurang Menarik', '2021-07-16 02:07:42', '2021-07-16 02:07:42'),
(4, 'Sherina', '0825157189', 'Pelayanan Sangat Bagus', '2021-07-16 07:17:45', '2021-07-16 07:17:45');

-- --------------------------------------------------------

--
-- Table structure for table `merk`
--

CREATE TABLE `merk` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_merk` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merk`
--

INSERT INTO `merk` (`id`, `nama_merk`, `created_at`, `updated_at`) VALUES
(1, 'Astra', '2021-06-17 16:00:29', '2021-06-17 16:00:29'),
(2, 'Prestone', '2021-06-17 16:01:50', '2021-06-17 16:01:50'),
(3, 'Faston', '2021-06-17 16:02:26', '2021-06-17 16:02:26'),
(4, 'Sell Helix', '2021-06-17 16:02:52', '2021-06-17 16:02:52'),
(5, 'STP Brake Fluid', '2021-06-17 16:06:17', '2021-06-17 16:06:17'),
(6, 'Pertamina', '2021-08-02 13:27:37', '2021-08-02 13:27:37'),
(7, 'Shell', '2021-08-02 13:27:57', '2021-08-02 13:27:57'),
(8, 'NGK', '2021-08-02 22:47:09', '2021-08-02 22:47:09'),
(9, 'Toyota', '2021-08-02 23:23:15', '2021-08-02 23:23:15'),
(10, 'Honda', '2021-08-02 23:23:35', '2021-08-02 23:23:35'),
(11, 'FCC', '2021-08-19 16:02:19', '2021-08-19 16:04:44');

-- --------------------------------------------------------

--
-- Table structure for table `merk_kendaraan`
--

CREATE TABLE `merk_kendaraan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_merk` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `merk_kendaraan`
--

INSERT INTO `merk_kendaraan` (`id`, `nama_merk`, `created_at`, `updated_at`) VALUES
(1, 'Honda', '2021-06-10 05:50:26', '2021-06-10 05:50:26'),
(2, 'Suzuki', '2021-06-10 05:50:45', '2021-06-10 05:56:15'),
(3, 'Toyota', '2021-06-10 05:51:15', '2021-06-10 05:51:15'),
(4, 'Daihatsu', '2021-06-18 06:05:22', '2021-06-18 06:05:22');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2021_06_06_223041_create_admin_table', 1),
(3, '2021_06_06_224421_create_merk_table', 1),
(4, '2021_06_06_224437_create_kategori_table', 1),
(5, '2021_06_06_224550_create_sparepart_table', 1),
(6, '2021_06_06_224620_create_merk_kendaraan_table', 1),
(7, '2021_06_06_224625_create_model_kendaraan_table', 1),
(8, '2021_06_06_224650_create_kendaraan_table', 1),
(9, '2021_06_06_224716_create_service_table', 1),
(10, '2021_06_06_225021_create_antrian_table', 1),
(11, '2021_06_07_021833_create_pembayaran_table', 1),
(12, '2021_06_07_041439_create_super_admin_table', 1),
(13, '2021_06_07_220149_create_kategori_jasa_table', 1),
(14, '2021_06_07_220316_create_jasa_table', 1),
(15, '2021_06_07_220620_create_detail_service_table', 1),
(16, '2021_06_21_065524_add_role_to_admin_table', 2),
(17, '2021_06_22_030101_add_service_id_to_sparepart_table', 3),
(20, '2021_06_24_062935_create_tmp_service_table', 4),
(21, '2021_06_24_085517_add_catatan_to_service_table', 5),
(22, '2021_07_16_085255_create_kritik_saran_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `model_kendaraan`
--

CREATE TABLE `model_kendaraan` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `merk_id` bigint(20) UNSIGNED NOT NULL,
  `nama_model` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_kendaraan`
--

INSERT INTO `model_kendaraan` (`id`, `merk_id`, `nama_model`, `photo`, `created_at`, `updated_at`) VALUES
(2, 1, 'Jazz', 'honda-jazz.jpg', '2021-06-12 07:01:41', '2021-06-12 07:01:41'),
(3, 2, 'Ertiga', 'ertiga.jpg', '2021-06-12 07:35:08', '2021-06-12 07:35:08'),
(5, 3, 'Rush', 'rushtoyoa.jpg', '2021-06-28 04:20:54', '2021-06-28 04:20:54'),
(6, 1, 'Mobilio', 'mobilio.jpg', '2021-08-03 04:26:06', '2021-08-03 04:26:06'),
(7, 1, 'Brio', 'brio.jpg', '2021-08-03 04:26:29', '2021-08-03 04:26:29'),
(8, 4, 'Ayla', 'ayla.jpg', '2021-08-03 04:27:47', '2021-08-03 04:27:47'),
(9, 4, 'Terios', 'terios.jpg', '2021-08-03 04:29:09', '2021-08-03 04:29:09'),
(10, 2, 'Karimun', 'suzukikarimun.jpg', '2021-08-03 04:31:20', '2021-08-03 04:31:20'),
(11, 2, 'Apv', 'apv.jpg', '2021-08-03 04:32:46', '2021-08-03 04:32:46'),
(12, 3, 'Avanza', 'Toyota-Avanza-F.jpg', '2021-08-03 04:34:44', '2021-08-03 04:34:44'),
(13, 3, 'Kijang Inova', 'kijanginova.png', '2021-08-03 04:35:39', '2021-08-03 04:35:39'),
(14, 4, 'Xenia', 'Daihatsu-Xenia-F.jpg', '2021-08-03 04:36:38', '2021-08-03 04:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `nota` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kd_service` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `antrian_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `total` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`nota`, `kd_service`, `antrian_id`, `status`, `total`, `created_at`, `updated_at`) VALUES
('TS-0010', 'PPWR-0EEJ-WLE0', 50, 1, 155000, '2021-07-22 04:32:32', '2021-07-22 04:32:32'),
('TS-0014', 'P08E-GCJU-XPY4', 55, 1, 80000, '2021-07-27 03:57:04', '2021-08-18 06:41:50'),
('TS-0021', 'ZPYE-7WMH-WA05', 62, 1, 171000, '2021-08-14 06:53:56', '2021-08-18 06:42:26'),
('TS-0022', 'YCBK-PJJ1-AAT1', 63, 1, 555000, '2021-08-14 15:07:01', '2021-08-18 06:42:35'),
('TS-0023', 'XEEY-BHZ2-FI7T', 67, 1, 771000, '2021-08-19 06:42:58', '2021-09-02 04:57:46'),
('TS-0024', 'HU0F-6J3S-FUCB', 68, 1, 345000, '2021-08-20 00:08:38', '2021-09-02 04:57:54'),
('TS-0025', 'B1AU-U1BU-M23Q', 69, 1, 228000, '2021-08-20 00:11:27', '2021-09-02 04:58:06'),
('TS-0026', '9U6B-70F4-FMBF', 70, 1, 641000, '2021-08-20 00:13:54', '2021-09-02 04:58:15'),
('TS-0027', 'Y4KL-1G1N-YCR1', 71, 1, 136000, '2021-08-20 00:16:13', '2021-09-02 04:58:23'),
('TS-0028', 'IBE3-KF5S-5ZFB', 72, 1, 155000, '2021-08-20 00:17:31', '2021-09-02 04:58:39'),
('TS-0029', '6KPM-XCU5-2FK3', 73, 1, 345000, '2021-08-20 00:18:47', '2021-09-02 04:59:05'),
('TS-0030', 'S09R-630Y-0Z72', 74, 1, 470000, '2021-08-20 00:21:37', '2021-09-02 04:59:18'),
('TS-0031', 'LJ5S-DCRZ-NGQR', 75, 1, 345000, '2021-08-20 00:33:04', '2021-09-02 04:59:28'),
('TS-0032', '808D-PR0A-LDIP', 76, 1, 350000, '2021-08-22 04:40:22', '2021-09-02 04:59:39'),
('TS-0033', 'JNSN-WWU4-GB4D', 77, 1, 555000, '2021-08-22 05:27:15', '2021-09-02 04:58:53'),
('TS-0034', 'Z17K-Q12K-RT7G', 78, 1, 1425000, '2021-08-22 05:35:27', '2021-09-02 04:57:37'),
('TS-0035', 'J9JX-K8G2-20Y9', 79, 0, 550000, '2021-09-02 03:43:40', '2021-09-02 03:43:40'),
('TS-0036', 'M98E-RPHH-QEP1', 80, 0, 155000, '2021-09-03 03:41:21', '2021-09-03 03:41:21'),
('TS-0037', 'NTUY-NRSP-R9YX', 81, 0, 905000, '2021-09-03 06:23:01', '2021-09-03 06:23:01');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `kd_service` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `kendaraan_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nama_lengkap` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_hp` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`kd_service`, `user_id`, `kendaraan_id`, `status`, `created_at`, `updated_at`, `nama_lengkap`, `no_hp`, `catatan`) VALUES
('6KPM-XCU5-2FK3', 21, 18, 3, '2021-08-20 00:18:46', '2021-08-20 00:18:46', 'Tuti Sugiarti', '085322477299', 'Service'),
('808D-PR0A-LDIP', 8, 5, 3, '2021-08-22 04:40:22', '2021-08-22 05:27:52', 'Ade Sherina', '082115806156', 'Service'),
('9U6B-70F4-FMBF', 13, 15, 3, '2021-08-20 00:13:54', '2021-08-20 00:13:54', 'Sherina', '081804881551', 'Service'),
('B1AU-U1BU-M23Q', 10, 9, 3, '2021-08-20 00:11:26', '2021-08-20 00:11:26', 'Yuni', '081902697875', 'Service'),
('HU0F-6J3S-FUCB', 8, 6, 3, '2021-08-20 00:08:37', '2021-08-20 00:08:37', 'Ade Sherina', '082115806156', 'Service'),
('IBE3-KF5S-5ZFB', 20, 17, 3, '2021-08-20 00:17:30', '2021-08-20 00:17:30', 'Nana', '0895322445525', 'Service'),
('J9JX-K8G2-20Y9', 15, 16, 3, '2021-09-02 03:43:39', '2021-09-02 03:43:39', 'Rudi', '081222045689', 'cepat yaa'),
('JNSN-WWU4-GB4D', 8, 5, 3, '2021-08-22 05:27:15', '2021-08-22 05:27:15', 'Ade Sherina', '082115806156', 'lorem'),
('LJ5S-DCRZ-NGQR', 8, 5, 3, '2021-08-20 00:33:04', '2021-08-20 00:33:04', 'Ade Sherina', '082115806156', 'sadad'),
('M98E-RPHH-QEP1', 15, 16, 2, '2021-09-03 03:41:20', '2021-09-03 06:30:09', 'Rudi', '081222045689', 'Service'),
('NTUY-NRSP-R9YX', 8, 5, 0, '2021-09-03 06:23:01', '2021-09-03 06:23:01', 'Ade Sherina', '082115806156', 'Service'),
('P08E-GCJU-XPY4', 8, 5, 3, '2021-07-27 03:57:04', '2021-07-27 03:57:04', 'Ade Sherina', '082115806156', 'Catatan'),
('PPWR-0EEJ-WLE0', 8, 6, 3, '2021-07-22 04:32:32', '2021-07-22 07:08:15', 'Ade Sherina', '082115806156', 'Ganti Aki'),
('S09R-630Y-0Z72', 21, 19, 3, '2021-08-20 00:21:36', '2021-08-20 00:21:36', 'Tuti Sugiarti', '085322477299', 'Service'),
('XEEY-BHZ2-FI7T', 8, 5, 3, '2021-08-19 06:42:58', '2021-08-19 06:42:58', 'Ade Sherina', '082115806156', 'Service'),
('Y4KL-1G1N-YCR1', 15, 16, 3, '2021-08-20 00:16:12', '2021-08-20 00:16:12', 'Rudi', '081222045689', 'Service'),
('YCBK-PJJ1-AAT1', 8, 5, 3, '2021-08-14 15:07:00', '2021-08-14 15:07:00', 'Ade Sherina', '082115806156', 'Service Master Rem'),
('Z17K-Q12K-RT7G', 8, 6, 3, '2021-08-22 05:35:26', '2021-08-22 05:36:25', 'Ade Sherina', '082115806156', 'Service'),
('ZPYE-7WMH-WA05', 8, 5, 3, '2021-08-14 06:53:55', '2021-08-14 06:53:55', 'Ade Sherina', '082115806156', 'Service Minyak Rem');

-- --------------------------------------------------------

--
-- Table structure for table `sparepart`
--

CREATE TABLE `sparepart` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `merk_id` bigint(20) UNSIGNED NOT NULL,
  `kategori_id` bigint(20) UNSIGNED NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jasa_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sparepart`
--

INSERT INTO `sparepart` (`id`, `nama`, `merk_id`, `kategori_id`, `stok`, `harga_jual`, `harga_beli`, `photo`, `created_at`, `updated_at`, `jasa_id`) VALUES
(6, 'Aki GS Astra Premium NS40Z', 1, 1, 5, 725000, 714000, 'Aki-GS-Premium-NS40Z.jpg', '2021-06-23 06:29:38', '2021-08-14 06:30:12', 4),
(8, 'Shell Spirax S3 G 80W 1L 1bottle', 4, 2, 1, 75000, 56000, 'shopping2.png', '2021-08-02 22:29:16', '2021-09-03 03:41:20', 5),
(9, 'Busi Spark Plug Mobil Suzuki Ertiga KR6A-10 NGK', 8, 7, 4, 65000, 55000, 'shoppinga.jpg', '2021-08-02 23:08:38', '2021-08-02 23:08:38', 9),
(10, 'Shell Helix HX6 Oli Mesin Mobil 0W-40 4L 1gallon', 4, 2, 7, 229000, 279000, 'shopping1.jpg', '2021-08-02 23:11:14', '2021-08-02 23:11:14', 8),
(11, 'Busi Ngk Standar Zfr6K-11 Honda Jazz', 8, 7, 4, 125000, 115000, 'busi.jpg', '2021-08-02 23:13:36', '2021-08-02 23:13:36', 9),
(12, 'GM SHELL SPIRAX S2 G 80W90 1 LITER', 7, 2, 7, 130000, 120000, 'images (1).jpeg', '2021-08-02 23:17:14', '2021-08-02 23:17:14', 6),
(13, 'Kampas Kopling Clutch Disc Toyota Rush atau Terios', 10, 8, 3, 20000, 19000, 'koplingrush.jpg', '2021-08-02 23:26:23', '2021-08-02 23:26:23', 7),
(16, 'MASTER SENTRAL REM JAZZ S RS GE8', 10, 3, 0, 435000, 425000, 'images (2).jpeg', '2021-08-02 23:38:40', '2021-09-03 06:23:01', 2),
(18, 'Prestone Concentrated Radiator Super Coolant AF-85338 1L', 2, 4, 4, 91000, 81000, 'prestoneradiator.png', '2021-08-03 04:05:32', '2021-08-20 00:16:12', 11),
(19, 'AKI GS ASTRA Hybrid NS40  mobil Toyota Avanza, Rush, Daihatsu Xenia, Terios,', 1, 1, 1, 691000, 681000, 'akihybrid.jpg', '2021-08-03 05:19:36', '2021-08-19 06:42:58', 4),
(20, 'Oli Gardan PERTAMINA RORED EPA 90. 4 Ltr', 6, 2, 3, 158000, 148000, 'images (3).jpeg', '2021-08-03 05:23:56', '2021-08-20 00:11:26', 6),
(21, 'Pertamina Fastron Techno 10w40 Oli Mobil [4 Liter]', 6, 2, 6, 285000, 275000, 'images (4).jpeg', '2021-08-03 05:27:24', '2021-08-03 05:27:24', 8),
(22, 'Prestone Radiator Cool Ready to Use Coolant 4L', 2, 4, 5, 75000, 65000, 'images (5).jpeg', '2021-08-03 05:30:45', '2021-08-03 05:30:45', 11),
(24, 'PRESTONE DOT 3 BRAKE FLUID 1 Liter', 2, 3, 5, 90000, 80000, 'images (7).jpeg', '2021-08-03 05:41:17', '2021-08-03 05:41:17', 12),
(25, 'Minyak Rem Mobil Prestone Dot 4 Hitam', 2, 3, 2, 71000, 61000, 'images (8).jpeg', '2021-08-03 05:44:14', '2021-08-20 00:13:54', 12),
(26, 'Master rem Ayla Agya', 9, 3, 1, 485000, 475000, 'images (6).jpeg', '2021-08-03 05:45:31', '2021-08-22 05:35:27', 2),
(27, 'KAMPAS REM DEPAN HONDA JAZZ RS/FERIO/MOBILIO/BRIO/FREED', 10, 3, 0, 150000, 140000, 'remkampasjazz.jpeg', '2021-08-03 05:47:21', '2021-08-22 05:35:26', 3),
(28, 'Kampas Rem Depan Terios Rush', 9, 3, 3, 390000, 380000, 'rushkampas.jpeg', '2021-08-03 05:48:51', '2021-08-03 05:48:51', 3);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE `super_admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tmp_service`
--

CREATE TABLE `tmp_service` (
  `kd_service` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `kendaraan_id` bigint(20) UNSIGNED NOT NULL,
  `value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`value`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tmp_service`
--

INSERT INTO `tmp_service` (`kd_service`, `user_id`, `kendaraan_id`, `value`, `created_at`, `updated_at`) VALUES
('1CB9-QMPX-QKQ2', 8, 5, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"345000\\\"}\"]', '2021-08-19 05:19:32', '2021-08-19 05:19:32'),
('2K5U-9PMJ-1HG0', 8, 5, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\"]', '2021-07-06 02:28:02', '2021-07-06 02:28:02'),
('5WWH-BXWT-HB6C', 15, 16, '[\"{\\\"id\\\":\\\"3\\\", \\\"nama_jasa\\\":\\\"Ganti Kampas Rem\\\", \\\"sparepart_name\\\":\\\"Kampas Rem Depan Terios Rush\\\", \\\"sparepart_id\\\":\\\"28\\\", \\\"harga_jasa\\\":\\\"710000\\\"}\"]', '2021-09-03 13:15:25', '2021-09-03 13:15:25'),
('6KPM-XCU5-2FK3', 21, 18, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"345000\\\"}\"]', '2021-08-20 00:18:34', '2021-08-20 00:18:34'),
('7BIG-WKMF-II5C', 15, 16, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"350000\\\"}\",\"{\\\"id\\\":\\\"5\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Transmisi\\\", \\\"sparepart_name\\\":\\\"Shell Spirax S3 G 80W 1L 1bottle\\\", \\\"sparepart_id\\\":\\\"8\\\", \\\"harga_jasa\\\":\\\"155000\\\"}\"]', '2021-09-03 06:21:33', '2021-09-03 06:21:33'),
('808D-PR0A-LDIP', 8, 5, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"350000\\\"}\"]', '2021-08-22 04:39:38', '2021-08-22 04:39:38'),
('8QTR-0DIN-BYH4', 8, 6, '[\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"AKI GS ASTRA Hybrid NS40  mobil Toyota Avanza, Rush, Daihatsu Xenia, Terios,\\\", \\\"sparepart_id\\\":\\\"19\\\", \\\"harga_jasa\\\":\\\"771000\\\"}\"]', '2021-08-19 05:29:01', '2021-08-19 05:29:01'),
('8W6T-0Y48-QZBF', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\"]', '2021-07-05 03:05:55', '2021-07-05 03:05:55'),
('9U6B-70F4-FMBF', 13, 15, '[\"{\\\"id\\\":\\\"3\\\", \\\"nama_jasa\\\":\\\"Ganti Kampas Rem\\\", \\\"sparepart_name\\\":\\\"KAMPAS REM DEPAN HONDA JAZZ RS/FERIO/MOBILIO/BRIO/FREED\\\", \\\"sparepart_id\\\":\\\"27\\\", \\\"harga_jasa\\\":\\\"470000\\\"}\",\"{\\\"id\\\":\\\"12\\\", \\\"nama_jasa\\\":\\\"Minyak Rem\\\", \\\"sparepart_name\\\":\\\"Minyak Rem Mobil Prestone Dot 4 Hitam\\\", \\\"sparepart_id\\\":\\\"25\\\", \\\"harga_jasa\\\":\\\"171000\\\"}\"]', '2021-08-20 00:13:40', '2021-08-20 00:13:40'),
('AIH6-G7JP-P6B0', 8, 5, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\"]', '2021-07-03 04:55:38', '2021-07-03 04:55:38'),
('B1AU-U1BU-M23Q', 10, 9, '[\"{\\\"id\\\":\\\"6\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Gardan\\\", \\\"sparepart_name\\\":\\\"Oli Gardan PERTAMINA RORED EPA 90. 4 Ltr\\\", \\\"sparepart_id\\\":\\\"20\\\", \\\"harga_jasa\\\":\\\"228000\\\"}\"]', '2021-08-20 00:11:09', '2021-08-20 00:11:09'),
('G4YX-A7IP-KIG1', 10, 9, '[\"{\\\"id\\\":\\\"3\\\", \\\"nama_jasa\\\":\\\"kampas rem\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"320000\\\"}\",\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"Aki GS Astra Premium NS40Z\\\", \\\"sparepart_id\\\":\\\"6\\\", \\\"harga_jasa\\\":\\\"155000\\\"}\"]', '2021-07-16 05:22:04', '2021-07-16 05:22:04'),
('HU0F-6J3S-FUCB', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"345000\\\"}\"]', '2021-08-20 00:08:19', '2021-08-20 00:08:19'),
('HX0A-KMAW-57XB', 10, 9, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\"]', '2021-08-02 05:13:27', '2021-08-02 05:13:27'),
('IBE3-KF5S-5ZFB', 20, 17, '[\"{\\\"id\\\":\\\"5\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Transmisi\\\", \\\"sparepart_name\\\":\\\"Shell Spirax S3 G 80W 1L 1bottle\\\", \\\"sparepart_id\\\":\\\"8\\\", \\\"harga_jasa\\\":\\\"155000\\\"}\"]', '2021-08-20 00:17:18', '2021-08-20 00:17:18'),
('J9GY-SU3W-AS3D', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"345000\\\"}\"]', '2021-08-20 00:35:26', '2021-08-20 00:35:26'),
('J9JX-K8G2-20Y9', 15, 16, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"350000\\\"}\",\"{\\\"id\\\":\\\"13\\\", \\\"nama_jasa\\\":\\\"Pembersihan Mesin Injektor/Kalbulator\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"200000\\\"}\"]', '2021-09-02 03:43:22', '2021-09-02 03:43:22'),
('JNSN-WWU4-GB4D', 8, 5, '[\"{\\\"id\\\":\\\"2\\\", \\\"nama_jasa\\\":\\\"Master Rem\\\", \\\"sparepart_name\\\":\\\"MASTER SENTRAL REM JAZZ S RS GE8\\\", \\\"sparepart_id\\\":\\\"16\\\", \\\"harga_jasa\\\":\\\"555000\\\"}\"]', '2021-08-22 05:26:56', '2021-08-22 05:26:56'),
('JY8Y-D9ER-091C', 8, 5, '[\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"AKI GS ASTRA Hybrid NS40  mobil Toyota Avanza, Rush, Daihatsu Xenia, Terios,\\\", \\\"sparepart_id\\\":\\\"19\\\", \\\"harga_jasa\\\":\\\"771000\\\"}\",\"{\\\"id\\\":\\\"6\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Gardan\\\", \\\"sparepart_name\\\":\\\"Oli Gardan PERTAMINA RORED EPA 90. 4 Ltr\\\", \\\"sparepart_id\\\":\\\"20\\\", \\\"harga_jasa\\\":\\\"228000\\\"}\"]', '2021-08-19 04:18:53', '2021-08-19 04:18:53'),
('KXLD-UX7S-UFFH', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\"]', '2021-07-15 14:10:30', '2021-07-15 14:10:30'),
('LJ5S-DCRZ-NGQR', 8, 5, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"345000\\\"}\"]', '2021-08-20 00:22:42', '2021-08-20 00:22:42'),
('M98E-RPHH-QEP1', 15, 16, '[\"{\\\"id\\\":\\\"5\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Transmisi\\\", \\\"sparepart_name\\\":\\\"Shell Spirax S3 G 80W 1L 1bottle\\\", \\\"sparepart_id\\\":\\\"8\\\", \\\"harga_jasa\\\":\\\"155000\\\"}\"]', '2021-09-03 03:41:01', '2021-09-03 03:41:01'),
('MATD-ZT92-07PE', 8, 6, '[\"{\\\"id\\\":\\\"2\\\", \\\"nama_jasa\\\":\\\"Master Rem\\\", \\\"sparepart_name\\\":\\\"Master rem Ayla Agya\\\", \\\"sparepart_id\\\":\\\"26\\\", \\\"harga_jasa\\\":\\\"605000\\\"}\"]', '2021-08-14 06:35:06', '2021-08-14 06:35:06'),
('NTUY-NRSP-R9YX', 8, 5, '[\"{\\\"id\\\":\\\"2\\\", \\\"nama_jasa\\\":\\\"Master Rem\\\", \\\"sparepart_name\\\":\\\"MASTER SENTRAL REM JAZZ S RS GE8\\\", \\\"sparepart_id\\\":\\\"16\\\", \\\"harga_jasa\\\":\\\"555000\\\"}\",\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"350000\\\"}\"]', '2021-09-03 06:22:46', '2021-09-03 06:22:46'),
('NUJK-8Z6X-8WU1', 10, 9, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"465000\\\"}\",\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"Oli Gardan AHM Honda\\\", \\\"sparepart_id\\\":\\\"7\\\", \\\"harga_jasa\\\":\\\"240000\\\"}\"]', '2021-07-07 03:15:35', '2021-07-07 03:15:35'),
('P08E-GCJU-XPY4', 8, 5, '[\"{\\\"id\\\":\\\"5\\\", \\\"nama_jasa\\\":\\\"Ganti Oli Transmisi\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"80000\\\"}\"]', '2021-07-27 03:45:40', '2021-07-27 03:45:40'),
('PPWR-0EEJ-WLE0', 8, 6, '[\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"Aki GS Astra Premium NS40Z\\\", \\\"sparepart_id\\\":\\\"6\\\", \\\"harga_jasa\\\":\\\"155000\\\"}\"]', '2021-07-22 04:31:22', '2021-07-22 04:31:22'),
('PXK5-UPW5-A425', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Ganti Mesin\\\", \\\"sparepart_name\\\":\\\"PRESTONE Antifreeze Radiator Coolant ( 1 Gallon - 3.78L)\\\", \\\"sparepart_id\\\":\\\"4\\\", \\\"harga_jasa\\\":\\\"585000\\\"}\"]', '2021-07-26 06:44:52', '2021-07-26 06:44:52'),
('S09R-630Y-0Z72', 21, 19, '[\"{\\\"id\\\":\\\"3\\\", \\\"nama_jasa\\\":\\\"Ganti Kampas Rem\\\", \\\"sparepart_name\\\":\\\"KAMPAS REM DEPAN HONDA JAZZ RS/FERIO/MOBILIO/BRIO/FREED\\\", \\\"sparepart_id\\\":\\\"27\\\", \\\"harga_jasa\\\":\\\"470000\\\"}\"]', '2021-08-20 00:21:23', '2021-08-20 00:21:23'),
('XEEY-BHZ2-FI7T', 8, 5, '[\"{\\\"id\\\":\\\"4\\\", \\\"nama_jasa\\\":\\\"Ganti Aki\\\", \\\"sparepart_name\\\":\\\"AKI GS ASTRA Hybrid NS40  mobil Toyota Avanza, Rush, Daihatsu Xenia, Terios,\\\", \\\"sparepart_id\\\":\\\"19\\\", \\\"harga_jasa\\\":\\\"771000\\\"}\"]', '2021-08-19 06:42:41', '2021-08-19 06:42:41'),
('Y4KL-1G1N-YCR1', 15, 16, '[\"{\\\"id\\\":\\\"11\\\", \\\"nama_jasa\\\":\\\"Ganti Air Radiator\\\", \\\"sparepart_name\\\":\\\"Prestone Concentrated Radiator Super Coolant AF-85338 1L\\\", \\\"sparepart_id\\\":\\\"18\\\", \\\"harga_jasa\\\":\\\"136000\\\"}\"]', '2021-08-20 00:15:59', '2021-08-20 00:15:59'),
('YCBK-PJJ1-AAT1', 8, 5, '[\"{\\\"id\\\":\\\"2\\\", \\\"nama_jasa\\\":\\\"Master Rem\\\", \\\"sparepart_name\\\":\\\"MASTER SENTRAL REM JAZZ S RS GE8\\\", \\\"sparepart_id\\\":\\\"16\\\", \\\"harga_jasa\\\":\\\"555000\\\"}\"]', '2021-08-14 15:06:25', '2021-08-14 15:06:25'),
('Z17K-Q12K-RT7G', 8, 6, '[\"{\\\"id\\\":\\\"1\\\", \\\"nama_jasa\\\":\\\"Tone Up\\\", \\\"sparepart_id\\\":\\\"null\\\", \\\"sparepart_name\\\":\\\"null\\\", \\\"harga_jasa\\\":\\\"350000\\\"}\",\"{\\\"id\\\":\\\"3\\\", \\\"nama_jasa\\\":\\\"Ganti Kampas Rem\\\", \\\"sparepart_name\\\":\\\"KAMPAS REM DEPAN HONDA JAZZ RS/FERIO/MOBILIO/BRIO/FREED\\\", \\\"sparepart_id\\\":\\\"27\\\", \\\"harga_jasa\\\":\\\"470000\\\"}\",\"{\\\"id\\\":\\\"2\\\", \\\"nama_jasa\\\":\\\"Master Rem\\\", \\\"sparepart_name\\\":\\\"Master rem Ayla Agya\\\", \\\"sparepart_id\\\":\\\"26\\\", \\\"harga_jasa\\\":\\\"605000\\\"}\"]', '2021-08-22 05:35:09', '2021-08-22 05:35:09'),
('ZPYE-7WMH-WA05', 8, 5, '[\"{\\\"id\\\":\\\"12\\\", \\\"nama_jasa\\\":\\\"Minyak Rem\\\", \\\"sparepart_name\\\":\\\"Minyak Rem Mobil Prestone Dot 4 Hitam\\\", \\\"sparepart_id\\\":\\\"25\\\", \\\"harga_jasa\\\":\\\"171000\\\"}\"]', '2021-08-14 06:53:08', '2021-08-14 06:53:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_lengkap` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `no_telp`, `code`, `nama_lengkap`, `alamat`, `jenis_kelamin`, `photo`, `username`, `password`, `is_verified`, `verified_at`, `created_at`, `updated_at`) VALUES
(8, '082115806156', NULL, 'Ade Sherina', 'Jl. Jendral Sudirman No.159', 'Perempuan', 'user.jpg', 'ade004', '$2y$10$8czRb0VBDldG3L.Lz90mGeClyvPxJSA.zHM3kn5fTAvGvKjA6/fHi', 1, '2021-06-12 01:02:04', '2021-06-12 01:01:39', '2021-06-12 01:02:04'),
(10, '081902697875', NULL, 'Yuni', NULL, NULL, NULL, 'yuni', '$2y$10$l7OOKAFEIfRZC6OXyhHglelzIq0/YdXvBLm6OfWCm5jNAHVVmWcEe', 1, '2021-07-07 03:13:13', '2021-07-07 03:12:45', '2021-07-07 03:13:13'),
(13, '081804881551', NULL, 'Sherina', NULL, NULL, NULL, 'sherina', '$2y$10$tFSVLhF6o24kqqfSnLxOZuGB1XLDeNaW22QFG2iGAW0ExNViSJWeC', 1, '2021-07-22 00:29:37', '2021-07-22 00:28:22', '2021-07-22 00:29:37'),
(15, '081222045689', NULL, 'Rudi', NULL, NULL, NULL, 'rudi123', '$2y$10$Jplc3EEFGI0v.i5c2busfudmBMIPane0Xa8zO1gmKm16.TI80dnbu', 1, '2021-08-09 10:20:28', '2021-08-09 10:19:57', '2021-08-09 10:20:28'),
(20, '0895322445525', NULL, 'Nana', NULL, NULL, NULL, 'nana24', '$2y$10$vAMa0AQvMc8apnCRx7Msh.FJFLJQ5/JKuo.whyyPFJd3Bxot8dVIi', 1, '2021-08-19 12:01:28', '2021-08-19 12:01:04', '2021-08-19 12:01:28'),
(21, '085322477299', NULL, 'Tuti Sugiarti', NULL, NULL, NULL, 'tutisugiarti', '$2y$10$cgs5/5qba/PLVNC8PsdB.uw/WA0LwldjtfQpSyVCesUrlIAUeQGI.', 1, '2021-08-19 12:05:32', '2021-08-19 12:04:56', '2021-08-19 12:05:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `antrian`
--
ALTER TABLE `antrian`
  ADD PRIMARY KEY (`id`),
  ADD KEY `antrian_kd_service_foreign` (`kd_service`);

--
-- Indexes for table `detail_service`
--
ALTER TABLE `detail_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_service_kd_service_foreign` (`kd_service`),
  ADD KEY `detail_service_sparepart_id_foreign` (`sparepart_id`),
  ADD KEY `detail_service_jasa_id_foreign` (`jasa_id`);

--
-- Indexes for table `jasa`
--
ALTER TABLE `jasa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jasa_kategori_id_foreign` (`kategori_id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_jasa`
--
ALTER TABLE `kategori_jasa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kendaraan_user_id_foreign` (`user_id`),
  ADD KEY `kendaraan_merk_id_foreign` (`merk_id`),
  ADD KEY `kendaraan_model_id_foreign` (`model_id`);

--
-- Indexes for table `kritik_saran`
--
ALTER TABLE `kritik_saran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merk_kendaraan`
--
ALTER TABLE `merk_kendaraan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_kendaraan`
--
ALTER TABLE `model_kendaraan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_kendaraan_merk_id_foreign` (`merk_id`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`nota`),
  ADD KEY `pembayaran_kd_service_foreign` (`kd_service`),
  ADD KEY `pembayaran_antrian_id_foreign` (`antrian_id`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`kd_service`),
  ADD KEY `service_user_id_foreign` (`user_id`),
  ADD KEY `service_kendaraan_id_foreign` (`kendaraan_id`);

--
-- Indexes for table `sparepart`
--
ALTER TABLE `sparepart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sparepart_merk_id_foreign` (`merk_id`),
  ADD KEY `sparepart_kategori_id_foreign` (`kategori_id`),
  ADD KEY `sparepart_jasa_id_foreign` (`jasa_id`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tmp_service`
--
ALTER TABLE `tmp_service`
  ADD PRIMARY KEY (`kd_service`),
  ADD KEY `tmp_service_user_id_foreign` (`user_id`),
  ADD KEY `tmp_service_kendaraan_id_foreign` (`kendaraan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `antrian`
--
ALTER TABLE `antrian`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `detail_service`
--
ALTER TABLE `detail_service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `jasa`
--
ALTER TABLE `jasa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kategori_jasa`
--
ALTER TABLE `kategori_jasa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kendaraan`
--
ALTER TABLE `kendaraan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `kritik_saran`
--
ALTER TABLE `kritik_saran`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `merk`
--
ALTER TABLE `merk`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `merk_kendaraan`
--
ALTER TABLE `merk_kendaraan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `model_kendaraan`
--
ALTER TABLE `model_kendaraan`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sparepart`
--
ALTER TABLE `sparepart`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `antrian`
--
ALTER TABLE `antrian`
  ADD CONSTRAINT `antrian_kd_service_foreign` FOREIGN KEY (`kd_service`) REFERENCES `service` (`kd_service`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_service`
--
ALTER TABLE `detail_service`
  ADD CONSTRAINT `detail_service_jasa_id_foreign` FOREIGN KEY (`jasa_id`) REFERENCES `jasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_service_kd_service_foreign` FOREIGN KEY (`kd_service`) REFERENCES `service` (`kd_service`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_service_sparepart_id_foreign` FOREIGN KEY (`sparepart_id`) REFERENCES `sparepart` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jasa`
--
ALTER TABLE `jasa`
  ADD CONSTRAINT `jasa_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori_jasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kendaraan`
--
ALTER TABLE `kendaraan`
  ADD CONSTRAINT `kendaraan_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merk_kendaraan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kendaraan_model_id_foreign` FOREIGN KEY (`model_id`) REFERENCES `model_kendaraan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `kendaraan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `model_kendaraan`
--
ALTER TABLE `model_kendaraan`
  ADD CONSTRAINT `model_kendaraan_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merk_kendaraan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD CONSTRAINT `pembayaran_antrian_id_foreign` FOREIGN KEY (`antrian_id`) REFERENCES `antrian` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `pembayaran_kd_service_foreign` FOREIGN KEY (`kd_service`) REFERENCES `service` (`kd_service`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_kendaraan_id_foreign` FOREIGN KEY (`kendaraan_id`) REFERENCES `kendaraan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `service_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sparepart`
--
ALTER TABLE `sparepart`
  ADD CONSTRAINT `sparepart_jasa_id_foreign` FOREIGN KEY (`jasa_id`) REFERENCES `jasa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sparepart_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sparepart_merk_id_foreign` FOREIGN KEY (`merk_id`) REFERENCES `merk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tmp_service`
--
ALTER TABLE `tmp_service`
  ADD CONSTRAINT `tmp_service_kendaraan_id_foreign` FOREIGN KEY (`kendaraan_id`) REFERENCES `kendaraan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tmp_service_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
