<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Services;
use App\Model\Antrian;

class Pembayaran extends Model
{
    //
    protected $table = "pembayaran";
    protected $primaryKey = "nota";
    public $incrementing = false;
    public $timestamps = true;

    public function serviceRef()
    {
        return $this->hasOne(Services::class, 'kd_service', 'kd_service');
    }

    public function antrianRef()
    {
        return $this->hasOne(Antrian::class, 'id', 'antrian_id');
    }
}
