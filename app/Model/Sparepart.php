<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use App\Model\MerkSparepart;
use App\Model\Jasa;
use App\Model\KategoriSparepart;

class Sparepart extends Model
{
    //
    protected $table = "sparepart";
    public $timestamps = true;

    public function merks()
    {
        return $this->hasOne(MerkSparepart::class, 'id', 'merk_id');
    }

    public function kategoris()
    {
        return $this->hasOne(KategoriSparepart::class, 'id', 'kategori_id');
    }

    public function jasaRef(){
        return $this->belongsTo(Jasa::class, 'jasa_id','id');
    }
}
