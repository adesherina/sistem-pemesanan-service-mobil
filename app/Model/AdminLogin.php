<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminLogin extends Model
{
    protected $table = "admin";
    protected $fillable = ['nama_lengkap', 'no_telp', 'username', 'password','timestamps',];
    protected $hidden = [
        'password',
    ];
}
