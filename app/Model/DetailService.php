<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Services;
use App\Model\Jasa;
use App\Model\Sparepart;

class DetailService extends Model
{
    //
    protected $table = "detail_service";
    public $timestamps = true;

    public function sparepartRef()
    {
        return $this->hasOne(Sparepart::class, 'id', 'sparepart_id');
    }

    public function serviceRef()
    {
        return $this->belongsTo(Services::class, 'kd_service', 'kd_service');
    }

    public function jasaRef()
    {
        return $this->hasOne(Jasa::class, 'id', 'jasa_id');
    }
}
