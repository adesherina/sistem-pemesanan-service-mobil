<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TmpService extends Model
{
    protected $table = "tmp_service";
    protected $primaryKey = "kd_service";
}
