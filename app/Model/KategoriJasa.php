<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriJasa extends Model
{
    protected $table = "kategori_jasa";
    public $timestamps = true;
}
