<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MerekKendaraan extends Model
{
    protected $table = "merk_kendaraan";
    public $timestamps = true;
}
