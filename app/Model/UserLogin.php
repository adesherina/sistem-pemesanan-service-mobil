<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserLogin extends Model
{
    protected $table = "users";
    public $timestamps = true;
    protected $fillable = ['nama_lengkap', 'no_telp','username', 'password','timestamps',];
    protected $hidden = [
        'password', 
    ];
}
