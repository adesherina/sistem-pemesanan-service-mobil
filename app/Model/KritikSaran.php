<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KritikSaran extends Model
{
    protected $table = "kritik_saran";
    public $timestamps = true;
}
