<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Model\Services;

class Antrian extends Model
{
    //
    protected $table = "antrian";
    public $timestamps = true;

    // public function userRef()
    // {
    //     return $this->belongsTo(User::class, 'user_id', 'id');
    // }

    public function serviceRef()
    {
        return $this->hasOne(Services::class, 'kd_service', 'kd_service');
    }
}
