<?php

namespace App\Model;
use App\User;
use App\Model\Kendaraan;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    //
    protected $table = "service";
    protected $primaryKey = "kd_service";
    public $incrementing = false;
    public $timestamps = true;

    public function userRef()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }
    
    public function kendaraanRef()
    {
        return $this->hasOne(Kendaraan::class,  'id','kendaraan_id');
    }

    public function detailServiceRef(){
        return $this->hasMany(DetailService::class, 'kd_service', 'kd_service');
    }

    public function antrianRef()
    {
        return $this->belongsTo(Antrian::class, 'kd_service','kd_service');
    }

    public function pembayaranRef()
    {
        return $this->belongsTo(Pembayaran::class, 'kd_service','kd_service');
    }
}
