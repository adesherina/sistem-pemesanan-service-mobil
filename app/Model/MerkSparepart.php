<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MerkSparepart extends Model
{
    protected $table = "merk";
    public $timestamps = true;
}
