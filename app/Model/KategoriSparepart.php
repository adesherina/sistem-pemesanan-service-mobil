<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class KategoriSparepart extends Model
{
    protected $table = "kategori";
    public $timestamps = true;
}
