<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\MerekKendaraan;
use App\Model\ModelKendaraan;
use App\User;

class Kendaraan extends Model
{
    //
    protected $table = "kendaraan";
    public $timestamps = true;

    public function merks()
    {
        return $this->hasOne(MerekKendaraan::class, 'id', 'merk_id');
    }

    public function models()
    {
        return $this->hasOne(ModelKendaraan::class, 'id', 'model_id');
    }

    public function userRef()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
