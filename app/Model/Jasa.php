<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\KategoriJasa;

class Jasa extends Model
{
    //
    protected $table = "jasa";
    public $timestamps = true;

    public function kategoris()
    {
        return $this->hasOne(KategoriJasa::class, 'id', 'kategori_id');
    }

    public function spareparts(){
        return $this->hasMany(Sparepart::class,'jasa_id','id');
    }
}
