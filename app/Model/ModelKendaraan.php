<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\MerekKendaraan;

class ModelKendaraan extends Model
{
    protected $table = "model_kendaraan";
    public $timestamps = true;

    public function merks()
    {
        return $this->hasOne(MerekKendaraan::class, 'id', 'merk_id');
    }
}
