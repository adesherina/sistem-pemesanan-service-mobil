<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\AdminLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class ForgotPWController extends Controller
{
    //tampil cari nomor telepon
    public function index()
    {
        return view('Admin.forgotpw.index');
    }

    public function cariTelepon(Request $request)
    {
        $admin = AdminLogin::whereno_telp($request->no_telp)->first();

        if ($admin != null){
            return view('Admin.forgotpw.forgot', ['no_telp' =>$request->no_telp])->with('status', 'Berhasil');
        } else {
            return view('Admin.forgotpw.index')->with('failed', 'Nomor Telepon Tidak Terdaftar');
        }
    }

    //tampil cari nomor telepon
    public function forgotpw(Request $request)
    {
        $newPassword = $request->newPassword;
        $no_telp = $request->no_telp;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword){
            $change = DB::table('admin')
            ->where('no_telp', $no_telp)
            ->update(['password' => Hash::make($newPassword)]);
            
            return redirect('admin/login')->with('successpass', 'Berhasil Mengganti Password');
        } elseif($newPassword != $confirmPassword){

            return view('Admin.forgotpw.forgot', ['no_telp'=>$no_telp])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }
    }
}
