<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Kendaraan;
use App\Model\MerekKendaraan;
use App\Model\ModelKendaraan;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Throwable;


class KendaraanController extends Controller
{
        //tampil data
    public function index()
    {
        $kendaraan = Kendaraan::with('models')
                    ->with('merks')->with('userRef')->get();
        return view('Admin.kendaraan.index', compact('kendaraan'));
    }

    //tambah data
    public function tambah()
    {
        $merk = MerekKendaraan::all();
        $model = ModelKendaraan::all();
        return view('Admin.kendaraan.tambah', compact('merk, model'));
    }

    //proses tambah data
    public function simpan(Request $request)
    {
        $rules = [
            'merk_id' => 'required',
            'model_id' => 'required',
            'tahun_keluar' => 'required',
        ];

        $messages = [
            'merk_id.required'  => 'Merek Mobil wajib diisi',
            'model_id.required'         => 'Model Mobil wajib diisi',
            'tahun_keluar.required'      => 'Tahun Keluar Mobil wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try {
                $kendaraan = new Kendaraan();
                $kendaraan->merk_id = $request->merk_id;
                $kendaraan->model_id = $request->model_id;
                $kendaraan->tahun_keluar = $request->tahun_keluar;
                $kendaraan->save();

               
            return redirect()->route('kendaraan.list')->with('status', 'Berhasil Menambahkan Data  Kendaraan');
        } catch (Throwable $e) {
            return redirect()->route('kendaraan.list')->with('status', 'Berhasil Menambahkan Data  Kendaraan');
        
        }
        // dd($request->all());
    }

    //tampil edit kendaraan
    public function edit($id)
    {
        $kendaraan = Kendaraan::where('id', $id)->first();
        return view('Admin.kendaraan.edit', compact('kendaraan'));
    }

    //proses update data
    public function update(Request $request, $id)
    {
        $kendaraan = Kendaraan::find($request->id);
        $kendaraan->merk_id = $request->merk_id;
        $kendaraan->model_id = $request->model_id;
        $kendaraan->tahun_keluar = $request->stok;

        $kendaraan->save();
        
        return redirect('kendaraan/list')->with('status2', 'Data Berhasil Diedit');
    }


    //tampil detail kendaraan
    public function detail($id)
    {
        $kendaraan = Kendaraan::with('models')
                    ->with('Merks')->with('userRef')->where('id', $id)->first();
        return view('Admin.kendaraan.detail', compact('kendaraan'));
    }

    //hapus data
    public function delete($id)
    {
        DB::table('kendaraan')->where('id', $id)->delete();

        return redirect('kendaraan/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
