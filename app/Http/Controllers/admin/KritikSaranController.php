<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\KritikSaran;
use Illuminate\Http\Request;

class KritikSaranController extends Controller
{
    //tampil data
    public function index()
    {
        $ks = KritikSaran::all();
        return view('Admin.kritik-saran.index', compact('ks'));
    }

    //detail
    public function detail($id)
    {
        $ks = KritikSaran::where('id', $id)->first();
        return view('Admin.kritik-saran.detail', compact('ks'));
    }

    //hapusdata
    public function delete($id)
    {
        KritikSaran::where('id', $id)->delete();

        return redirect('kritikSaran/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
