<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\ModelKendaraan;
use App\Model\MerekKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class ModelKendaraanController extends Controller
{
    //tampil list
    public function index()
    {
        $ModelKendaraan = ModelKendaraan::with('Merks')->get();;
        return view('Admin.kendaraan.model.index', compact('ModelKendaraan'));
    }

    //tampil tambah ModelKendaraan
    public function tambah()
    {
        $merk = MerekKendaraan::all();
        return view('Admin.kendaraan.model.tambah', compact('merk'));
    }

    //proses tambah ModelKendaraan
    public function simpan(Request $request)
    {
        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/model-mobil", $name);

            $ModelKendaraan = new ModelKendaraan();
            $ModelKendaraan->nama_model = $request->nama_model;
            $ModelKendaraan->merk_id = $request->merk_id;
            $ModelKendaraan->photo = $name;
            $ModelKendaraan->save();

            echo "Foto berhasil di upload";
            return redirect()->route('model.kendaraan.list')->with('status1', 'Data Berhasil Ditambahkan');
        } else {
            echo "Gagal upload Foto";
            return redirect()->route('model.kendaraan.list')->with('status2', 'Data Gagal Ditambahkan');
        }
    }

    //edit data
    public function edit($id){
        $ModelKendaraan = ModelKendaraan::where('id', $id)->first();
        return view('Admin.kendaraan.model.edit', compact('ModelKendaraan'));

    }

    //update
    public function update(Request $request, $id)
    {
        if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/model-mobil", $name);

            $ModelKendaraan = ModelKendaraan::find($request->id);
            $ModelKendaraan->nama_model = $request->nama_model;
            $ModelKendaraan->merk_id = $request->merk_id;
            $ModelKendaraan->photo = $name;
            $ModelKendaraan->save();

            echo "Foto berhasil di upload";
            return redirect()->route('model.kendaraan.list')->with('status3', 'Data Berhasil Ditambahkan');
        } else {
            echo "Gagal upload Foto";
            return redirect()->route('model.kendaraan.list')->with('status4', 'Data Gagal Ditambahkan');
        }
    }

    //hapus data
    public function delete($id)
    {
        DB::table('model')->where('id', $id)->delete();

        return redirect('modelkendaraan/list')->with('status5', 'Data Berhasil Dihapus');
    }
}
