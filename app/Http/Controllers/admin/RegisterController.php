<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\AdminLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class RegisterController extends Controller
{
    //tampil Register
    public function index()
    {
        return view('admin.register');
    }

    //add proses register
    public function addProcess(Request $request)
    {
        $timestamps = true;
        //query builder insert
        $rules = [
            'nama_lengkap' => 'required|min:5|max:100',
            'no_telp' => 'required|min:5|max:100',
            'username' => 'required|min:5|max:100',
            'password' => 'required|min:5'
        ];
 
        $messages = [
            'nama_lengkap.required'          => 'Nama wajib diisi.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 8 karakter.',
            'username.required'      => 'Username wajib diisi.',
            'username.min'           => 'Username minimal diisi dengan 5 karakter.',
            'no_telp.required'         => 'Nomor Telepon wajib diisi.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        if($request->password != $request->confirmPassword){
             return redirect()->route('user.register')->with('statusFailed', 'Password Konfirmasi Tidak Sama');
        }else{
            $AdminLogin = new AdminLogin();
            $AdminLogin->nama_lengkap = $request->nama_lengkap;
            $AdminLogin->no_telp = $request->no_telp;
            $AdminLogin->username = $request->username;
            $AdminLogin->password = Hash::make($request->password);


            $AdminLogin->save();
            //
            if($AdminLogin->save()){
                $data = ([
                    'nama_lengkap' => $request->nama_lengkap,
                    'no_telp' => $request->no_telp,
                    'username' => $request->username,
                ]);

                return redirect()->route('admin.login')->with('message', 'Register Berhasil');
            }else{
                return redirect()->route('admin.register')->with('message2', 'Register akun gagal');
            }
        }
    }
}
