<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Antrian;
use App\Model\Services;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AntrianController extends Controller
{
    //tampil daftar antrian
    public function index()
    {
        $antrian = Antrian::with('serviceRef')->get();
        return view('Admin.antrian.index', compact('antrian'));
    }

     //tampil daftar atur antrian
    public function aturantrian()
    {
        $antrian = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('status','Menunggu')->get();
        $antrianProses = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('status','Proses')->get();
        return view('Admin.antrian.aturantrian', compact('antrian','antrianProses'));
    }

    public function aturAntrianProses(){
        $ap = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('status','Proses')->get();
        if($ap->count() < 2){
            $am = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('status','Menunggu')->orderBy('no_antrian', 'ASC')->first();

            if($am){
                $am->status = "Proses";
                $am->save();

               $sevice = Services::where('kd_service', $am->kd_service)
                        ->update(['status' => 2]);

                $this->sendMessage();
                return redirect()->back();
            }else{
                return redirect()->back();
            }
        }else{
            return redirect()->back()->with('status3', 'Load antrian penuh');
        }
    }

    public function aturAntrianSelesai($id_antrian){
        $ap = Antrian::where('id', $id_antrian)->first();
        if($ap){
            $ap->status = 'Selesai';
            $ap->save();

               $sevice = Services::where('kd_service', $ap->kd_service)
                        ->update(['status' => 3]);
            return $this->aturAntrianProses();
        }
        return redirect()->back()->with('status3', 'error');
    }
    
    //tampil detail antrian
    public function detail($id)
    {
        $antrian = DB::table('antrian')->where('id', $id)->first();
        return view('Admin.antrian.detail', compact('antrian'));
    }

    public function sendMessage(){
        $search = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('status','Proses')->orderBy('no_antrian', 'DESC')->first();
        if($search){
            $no_antrian = (int)$search->no_antrian + 2;
            $send = Antrian::with('serviceRef')->where('tanggal', Carbon::today())->where('no_antrian',$no_antrian)->first();
           
            if($send){
                    $num_padded = sprintf("%02d", $search->no_antrian);
                    $msg = "Antrian saat ini sudah nomor ". $num_padded ."\nHarap Untuk segera datang ke Bengkel TIga Saudara";

                    $userkey = 'e8c9bd9e4f8a';
                    $passkey = '7a7b201077486051653c5ada';
                    $telepon = $send->serviceRef->userRef->no_telp;
                    $message = $msg;
                    $url = 'https://console.zenziva.net/reguler/api/sendsms/';
                    $curlHandle = curl_init();
                    curl_setopt($curlHandle, CURLOPT_URL, $url);
                    curl_setopt($curlHandle, CURLOPT_HEADER, 0);
                    curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
                    curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
                    curl_setopt($curlHandle, CURLOPT_POST, 1);
                    curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
                        'userkey' => $userkey,
                        'passkey' => $passkey,
                        'to' => $telepon,
                        'message' => $message
                    ));
                    $results = json_decode(curl_exec($curlHandle), true);
                    curl_close($curlHandle);
            }
        }
    }

    //delete
    public function delete($id)
    {
        DB::table('antrian')->where('id', $id)->delete();

        return redirect('antrian/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
