<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\KategoriJasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class KategoriJasaController extends Controller
{
    //tampil list
    public function index()
    {
        $KategoriJasa = KategoriJasa::all();
        return view('Admin.jasa.kategori.index', compact('KategoriJasa'));
    }

    //tampil tambah KategoriJasa
    public function tambah()
    {
        return view('Admin.jasa.kategori.tambah');
    }

    //proses tambah KategoriJasa
    public function simpan(Request $request)
    {
        try {
            $KategoriJasa = new KategoriJasa();
            $KategoriJasa->nama_kategori = $request->nama_kategori;
            $KategoriJasa->save();

            return redirect()->route('kategori.jasa.list')->with('status', 'Berhasil Menambahkan Data Kategori Jasa');
        } catch (Throwable $e) {
            return redirect()->route('kategori.jasa.list')->with('status', 'Berhasil Menambahkan Data Kategori Jasa');
        }
    }

    //edit data
    public function edit($id){
        $KategoriJasa = DB::table('kategori_jasa')->where('id', $id)->first();
        return view('Admin.jasa.kategori.edit', compact('KategoriJasa'));

    }

    //update
    public function update(Request $request, $id)
    {
        $KategoriJasa = KategoriJasa::find($request->id);
        $KategoriJasa->nama_kategori = $request->nama_kategori;
        $KategoriJasa->save();
        return redirect('kategorijasa/list')->with('status2', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('kategori_jasa')->where('id', $id)->delete();

        return redirect('kategorijasa/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
