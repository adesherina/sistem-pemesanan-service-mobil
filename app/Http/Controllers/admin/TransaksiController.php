<?php

namespace App\Http\Controllers\admin;

use App\Exports\TransaksiServiceExport;
use App\Exports\TransaksiServiceExportLunas;
use App\Exports\TransaksiServiceExportBelumLunas;
use App\Http\Controllers\Controller;
use App\Model\Pembayaran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class TransaksiController extends Controller
{
    //tampil daftar transaksi
    public function index()
    {
        $ts = Pembayaran::with('serviceRef','antrianRef')->get();
        $total = Pembayaran::with('serviceRef','antrianRef')->sum('total');
        return view('Admin.transaksi.index', compact('ts', 'total'));
    }

    //ubah status
    public function changeStatus(Request $req, $nota)
    {
            
            try{
                if($req->status == 0){
                    $status = "Belum Bayar";
                }else if($req->status == 1){
                    $status = "Lunas";
                }


                $bayar = Pembayaran::find($nota);
                $bayar->status = $req->status;
                $bayar->save();

                $success['status'] = 1;
                $success['message'] = $status;
                return response()->json($success, 200);
            }catch(\Exception $e){
                $success['status'] = 0;
                $success['message'] = $e->getMessage();
                return response()->json($success, 500);
            }
    
    }

    //tampil detail transaksi service
    public function detail($nota)
    {
        $ts = Pembayaran::with('serviceRef', 'antrianRef')->where('nota', $nota)->first();
        // dd($sparepart);
            return view('Admin.transaksi.detail', compact('ts'));
    }

    //tampil data laporan
    public function laporan()
    {
        $ts = Pembayaran::with('serviceRef','antrianRef')->get();
        $total = Pembayaran::with('serviceRef','antrianRef')->sum('total');
        $from = null;
        $to = null;

        return view('Admin.transaksi.laporan', compact('ts', 'total', 'from', 'to'));
    }

    //cari laporan
    public function cari(){

        $from = request()->from;
        $to = request()->to;

        $dari = Carbon::parse(request()->from);
        $sampai = Carbon::parse(request()->to)->addDays(1);
 
        if($dari && !$sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,\Carbon\Carbon::now()])->get();
        }elseif($dari && $sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->get();
        }else{
            $ts = Pembayaran::with('serviceRef','antrianRef')->get();
        }
 
        $total = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->sum('total');
 
        return view('Admin.transaksi.laporan',compact('ts','total','from','to'));
    }

    //export
    public function exportLap(){
 
        $from = request()->from;
        $to = request()->to;

        return Excel::download(new TransaksiServiceExport($from, $to), 'Transaksi Service.xlsx');
    }

    //tampil transaksi lunas
     public function laporanLunas()
    {
        $ts = Pembayaran::with('serviceRef','antrianRef')->where('status','1')->get();
        $total = Pembayaran::with('serviceRef','antrianRef')->where('status','1')->sum('total');
        $from = null;
        $to = null;

        return view('Admin.transaksi.transaksi-lunas', compact('ts', 'total', 'from', 'to'));
    }

    //cari laporan lunas
    public function cariLunas(){

        $from = request()->from;
        $to = request()->to;

        $dari = Carbon::parse(request()->from);
        $sampai = Carbon::parse(request()->to)->addDays(1);
 
        if($dari && !$sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,\Carbon\Carbon::now()])->where('status','1')->get();
        }elseif($dari && $sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->where('status','1')->get();
        }else{
            $ts = Pembayaran::with('serviceRef','antrianRef')->where('status','1')->get();
        }
 
        $total = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->where('status','1')->sum('total');
 
        return view('Admin.transaksi.transaksi-lunas',compact('ts','total','from','to'));
    }

    //export Lunas Lunas
    public function exportLapLunas(){
 
        $from = request()->from;
        $to = request()->to;

        return Excel::download(new TransaksiServiceExportLunas($from, $to), 'Transaksi Service.xlsx');
    }

    //tampil transaksi belum lunas
     public function laporanBLunas()
    {
        $ts = Pembayaran::with('serviceRef','antrianRef')->where('status','0')->get();
        $total = Pembayaran::with('serviceRef','antrianRef')->where('status','0')->sum('total');
        $from = null;
        $to = null;

        return view('Admin.transaksi.transaksi-belum-lunas', compact('ts', 'total', 'from', 'to'));
    }

    //cari laporan belum lunas
    public function cariBLunas(){

        $from = request()->from;
        $to = request()->to;

        $dari = Carbon::parse(request()->from);
        $sampai = Carbon::parse(request()->to)->addDays(1);
 
        if($dari && !$sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,\Carbon\Carbon::now()])->where('status','0')->get();
        }elseif($dari && $sampai){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->where('status','0')->get();
        }else{
            $ts = Pembayaran::with('serviceRef','antrianRef')->where('status','0')->get();
        }
 
        $total = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->where('status','0')->sum('total');
 
        return view('Admin.transaksi.transaksi-belum-lunas',compact('ts','total','from','to'));
    }

    //export Belum Lunas
    public function exportLapBLunas(){
 
        $from = request()->from;
        $to = request()->to;

        return Excel::download(new TransaksiServiceExportBelumLunas($from, $to), 'Transaksi Service.xlsx');
    }

    
}