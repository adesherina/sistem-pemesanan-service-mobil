<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\AdminLogin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    //tampil data
    public function index()
    {
        $admins = AdminLogin::where('role', 'Admin')->get();
        return view('Admin.admins.index', compact('admins'));
    }

    //detail
    public function detail($id)
    {
        $admins = AdminLogin::where('id', $id)->first();
        return view('Admin.admins.detail', compact('admins'));
    }

    //hapusdata
    public function delete($id)
    {
        AdminLogin::where('id', $id)->delete();

        return redirect('admin/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
