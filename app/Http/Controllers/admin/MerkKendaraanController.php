<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\MerekKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Throwable;


class MerkKendaraanController extends Controller
{
    //tampil list
    public function index()
    {
        $MerekKendaraan = MerekKendaraan::all();
        return view('Admin.kendaraan.merk.index', compact('MerekKendaraan'));
    }

    //tampil tambah MerekKendaraan
    public function tambah()
    {
        return view('Admin.kendaraan.merk.tambah');
    }

    //proses tambah MerekKendaraan
    public function simpan(Request $request)
    {
        try {
            $MerekKendaraan = new MerekKendaraan();
            $MerekKendaraan->nama_merk = $request->nama_merk;
            $MerekKendaraan->save();

            return redirect()->route('merk.kendaraan.list')->with('status', 'Berhasil Menambahkan Data Merek Kendaraan');
        } catch (Throwable $e) {
            return redirect()->route('merk.kendaraan.list')->with('status', 'Berhasil Menambahkan Data Merek Kendaraan');
        }
    }

    //edit data
    public function edit($id){
        $MerekKendaraan = DB::table('merk_kendaraan')->where('id', $id)->first();
        return view('Admin.kendaraan.merk.edit', compact('MerekKendaraan'));

    }

    //update
    public function update(Request $request, $id)
    {
        $MerekKendaraan = MerekKendaraan::find($request->id);
        $MerekKendaraan->nama_merk = $request->nama_merk;
        $MerekKendaraan->save();
        return redirect()->route('merk.kendaraan.list')->with('status2', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('merk_kendaraan')->where('id', $id)->delete();

        return redirect()->route('merk.kendaraan.list')->with('status3', 'Data Berhasil Dihapus');
    }
}
