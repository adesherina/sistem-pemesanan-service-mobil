<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Jasa;
use App\Model\KategoriJasa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Throwable;



class JasaController extends Controller
{
    //tampil data
    public function index()
    {
        $jasa = Jasa::with('kategoris')->get();
        return view('Admin.jasa.index', compact('jasa'));
    }

    //tambah data
    public function tambah()
    {
        $kategori = Kategorijasa::all();
        return view('Admin.jasa.tambah', compact('kategori'));
    }

    //proses tambah data
    public function simpan(Request $request)
    {
        $rules = [
            'nama_jasa' => 'required',
            'kategori_id' => 'required',
            'harga_jasa' => 'required',
            'estimasi' => 'required',
        ];

        $messages = [
            'nama_jasa.required'  => 'Nama jasa  wajib diisi',
            'kategori_id.required'         => 'Kategori wajib diisi',
            'harga_jasa.required'  => 'Harga Jasa  wajib diisi',
            'estimasi.required'  => 'Estimasi Waktu Pengerjaan wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try {
                $jasa = new Jasa();
                $jasa->nama_jasa = $request->nama_jasa;
                $jasa->kategori_id = $request->kategori_id;
                $jasa->harga_jasa = $request->harga_jasa;
                $jasa->estimasi = $request->estimasi;
                $jasa->status = 1;
                $jasa->save();

               
            return redirect()->route('jasa.list')->with('status', 'Berhasil Menambahkan Data Merek jasa');
        } catch (Throwable $e) {
            return redirect()->route('jasa.list')->with('status', 'Berhasil Menambahkan Data Merek jasa');
        
        }
    }

    //ubah status Aktif
    public function changeStatusAktif(Request $req, $id)
    {
            
            try{
                if($req->status == 1){
                    $status = "Aktif";
                }else if($req->status == 0){
                    $status = "Tidak Aktif";
                }


                $bayar = Jasa::find($id);
                $bayar->status = $req->status;
                $bayar->save();

                $success['status'] = 1;
                $success['message'] = $status;
                return response()->json($success, 200);
            }catch(\Exception $e){
                $success['status'] = 0;
                $success['message'] = $e->getMessage();
                return response()->json($success, 500);
            }
    }

    //ubah status Tidak Aktif
    public function changeStatusTAktif(Request $req, $id)
    {
            
            try{
                if($req->status == 1){
                    $status = "Aktif";
                }else if($req->status == 0){
                    $status = "Tidak Aktif";
                }


                $bayar = Jasa::find($id);
                $bayar->status = $req->status;
                $bayar->save();

                $success['status'] = 0;
                $success['message'] = $status;
                return response()->json($success, 200);
            }catch(\Exception $e){
                $success['status'] = 0;
                $success['message'] = $e->getMessage();
                return response()->json($success, 500);
            }
    }

    //tampil edit jasa
    public function edit($id)
    {
        $jasa = jasa::where('id', $id)->first();
        return view('Admin.jasa.edit', compact('jasa'));
    }

    //proses update data
    public function update(Request $request, $id)
    {
        $jasa = Jasa::find($request->id);
        // $jasa->nama_jasa = $request->nama_jasa;
        // $jasa->kategori_id = $request->kategori_id;
        $jasa->harga_jasa = $request->harga_jasa;

        $jasa->save();
        
        return redirect('jasa/list')->with('status2', 'Data Berhasil Diedit');
    }


    //tampil detail jasa
    public function detail($id)
    {
        $jasa = DB::table('jasa')->where('id', $id)->first();
        return view('Admin.jasa.detail', compact('jasa'));
    }

    //hapus data
    public function delete($id)
    {
        DB::table('jasa')->where('id', $id)->delete();

        return redirect('jasa/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
