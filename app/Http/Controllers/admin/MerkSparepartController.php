<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\MerkSparepart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;


class MerkSparepartController extends Controller
{
    //tampil list
    public function index()
    {
        $MerkSparepart = MerkSparepart::all();
        return view('Admin.sparepart.merk.index', compact('MerkSparepart'));
    }

    //tampil tambah MerkSparepart
    public function tambah()
    {
        return view('Admin.sparepart.merk.tambah');
    }

    //proses tambah MerkSparepart
    public function simpan(Request $request)
    {
        try {
            $MerkSparepart = new MerkSparepart();
            $MerkSparepart->nama_merk = $request->nama_merk;
            $MerkSparepart->save();

            return redirect()->route('merk.sparepart.list')->with('status', 'Berhasil Menambahkan Data Merek Sparepart');
        } catch (Throwable $e) {
            return redirect()->route('merk.sparepart.list')->with('status', 'Berhasil Menambahkan Data Merek Sparepart');
        }
    }

    //edit data
    public function edit($id){
        $MerkSparepart = DB::table('merk')->where('id', $id)->first();
        return view('Admin.sparepart.merk.edit', compact('MerkSparepart'));

    }

    //update
    public function update(Request $request, $id)
    {
        $MerkSparepart = MerkSparepart::find($request->id);
        $MerkSparepart->nama_merk = $request->nama_merk;
        $MerkSparepart->save();
        return redirect('merksparepart/list')->with('status2', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('merk')->where('id', $id)->delete();

        return redirect('merksparepart/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
