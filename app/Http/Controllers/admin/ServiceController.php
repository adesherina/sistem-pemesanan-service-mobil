<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\Jasa;
use App\Model\Kendaraan;
use App\Model\Services;
use App\Model\Antrian;
use App\Model\Sparepart;
use App\Model\TmpService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Throwable;

class ServiceController extends Controller
{
    //tampil daftar service
    public function index()
    {
        $service = Services::with('userRef')->with('kendaraanRef')->get();
        // dd($service);
        return view('Admin.service.index', compact('service'));
    }

    //tambah data
    public function tambah()
    {
        $service = Services::all();
        return view('Admin.service.tambah', compact('service'));
    }

    //proses tambah data
    public function simpan(Request $request)
    {
        $user = User::find($request->customer_id);
        $nama = null;
        if($user){
            $nama = $user->nama_lengkap;
        }
       $kd_service = SN::generate();
       $service = new Services();
       $service->kd_service = $kd_service;
       $service->user_id = $request->customer_id;
       $service->kendaraan_id = $request->car_id;
       $service->status = 0;
       $service->catan = $request->note;
       $service->no_hp = $request->phone;
       $service->nama_lengkap = $nama; 
       $service->save();

       $details = $request->services;
       if(count($details) > 0){
                // Nambah jasa/sparepart ke detail
                foreach($details as $value){
                    $parseValue = json_decode($value);

                    if($parseValue->sparepart_id != "null"){
                        $detail = new DetailService();
                        $detail->kd_service = $kd_service;
                        $detail->sparepart_id = $parseValue->sparepart_id;
                        $detail->jasa_id = $parseValue->id;
                        $detail->subtotal = $parseValue->harga_jasa;

                        $sp = Sparepart::find($parseValue->sparepart_id);
                        if($sp){
                            Sparepart::where('id', $sp->id)->update([
                                'stok' => (int)$sp->stok - 1
                            ]);
                        }
                    }else{
                        $detail = new DetailService();
                        $detail->kd_service = $kd_service;
                        $detail->jasa_id = $parseValue->id;
                        $detail->subtotal = $parseValue->harga_jasa;
                    }
                    $detail->save(); 
                    
                    $total += (int)$parseValue->harga_jasa;
                }
       }
       


    }

    //alert update verified
    public function makeVerified($kd_service)
    {
        $service = Services::find($kd_service);
        $service->status = 'Verified';
        $service->save();

        $success['status'] = '1';
        $success['message'] = 'Config save success';
        return response()->json($success, 200);
    }

    //tampil edit service
    public function edit($kd_service)
    {
        $service = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef', 'userRef')
                    ->where('kd_service', $kd_service)->first();
        return view('Admin.service.edit', compact('service'));
    }

    //proses update data
    public function update(Request $request, $kd_service)
    {
        $service = Services::find($request->kd_service);
        $service->harga_service = $request->harga_service;

        $service->save();
        
        return redirect('service/list')->with('status2', 'Data Berhasil Diedit');
    }


    public function detail($kd_service)
    {
        $service = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
        ->where('kd_service', $kd_service)->first();
        // dd($service);
            return view('Admin.service.detail', compact('service'));
    }

    //hapus data
    public function delete($kd_service)
    {
        DB::table('service')->where('kd_service', $kd_service)->delete();

        return redirect()->route('service.list')->with('status3', 'Data Berhasil Dihapus');
    }

    public function changeStatus(Request $req, $kd_service)
    {
            
            try{
                if($req->status == 1){
                    $status = "Dikonfirmasi";
                }else if($req->status == 2){
                    $status = "Sedang diservice";
                }else if($req->status == 3){
                    $status = "Selesai";
                }
                $service = Services::find($kd_service);
                $service->status = $req->status;
                $service->save();

                $success['status'] = 1;
                $success['message'] = $status;
                return response()->json($success, 200);
            }catch(\Exception $e){
                $success['status'] = 0;
                $success['message'] = $e->getMessage();
                return response()->json($success, 500);
            }

    }

    public function getCar(Request $request){
        $user_id = $request->user_id;

        $cars = collect();
        
        try{
            $cars = Kendaraan::with('merks','models')->where('user_id', $user_id)->get();

            return response()->json([
                'code'=>200,
                'status' => 1,
                'message'=> "Get data success",
                'data'=> $cars
            ]);
        }catch(\Throwable $err){
            return response()->json([
                'code'=>500,
                'status' => 0,
                'message'=> $err,
                'data'=> $cars
            ]);
        }        
    }
//============Tambah Service=====================

//get semua jasa
    public function getAllJasa(){
        $jasas = Jasa::get();
        return response()
        ->json([
            'code'=>200,
            'status' => 1,
            'message'=> "Get data success",
            'data'=> $jasas
        ]);
    }
//get jasa by id
    public function getJasaById(Request $request){
        try{
            $jasa_id = $request->jasa_id;
            $jasa = Jasa::with('spareparts')->find($jasa_id);
            if($jasa){
                return response()->json([
                    'code'=>200,
                    'status' => 1,
                    'message'=> "Get data success",
                    'data'=> $jasa
                ]);
            }else{
                return response()->json([
                    'code'=>404,
                    'status' => 0,
                    'message'=> "Get data not found",
                    'data'=> null
                ]);
            }
        }catch(\Throwable $err){
                return response()->json([
                    'code'=>500,
                    'status' => 1,
                    'message'=> $err,
                    'data'=> null
                ]);
        }
    }
    //get sparepart by id
    public function getSparepartById(Request $request){
        try{
            $sparepart_id = $request->sparepart_id;
            $sparepart = Sparepart::with('jasaRef')->find($sparepart_id);
            if($sparepart){
                return response()->json([
                    'code'=>200,
                    'status' => 1,
                    'message'=> "Get data success",
                    'data'=> $sparepart
                ]);
            }else{
                return response()->json([
                    'code'=>404,
                    'status' => 0,
                    'message'=> "Get data not found",
                    'data'=> null
                ]);
            }
        }catch(\Throwable $err){
                return response()->json([
                    'code'=>500,
                    'status' => 1,
                    'message'=> $err,
                    'data'=> null
                ]);
        }
    }

    //cek nomor antrian
    public function getAntrian(Request $request){
        $last = Antrian::where('tanggal', $request->tanggal)->latest('no_antrian')->first();
        if($last){
            $success['status'] = 200;
            $success['antrian'] = (int)$last->no_antrian + 1;
        }else{
            $success['status'] = 200;
            $success['antrian'] = 1;
        }

        return response()->json($success);
    }

    //tambah service
    public function save(Request $request){
        // dd(json_decode($request->value));
        $kd_service = SN::generate();
        $service = new TmpService();
        $service->kd_service = $kd_service;
        $service->user_id = $request->user_id;
        $service->kendaraan_id = $request->kendaraan;
        $service->catatan= $request->catatan;
        $service->no_hp = $request->no_hp;
        $service->value = $request->value;
        $service->save();

        return redirect()->route('service.list');
    }

    
}
