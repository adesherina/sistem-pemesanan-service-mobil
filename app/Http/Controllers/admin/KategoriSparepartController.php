<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\KategoriSparepart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;



class KategoriSparepartController extends Controller
{
    //tampil list
    public function index()
    {
        $KategoriSparepart = KategoriSparepart::all();
        return view('Admin.sparepart.kategori.index', compact('KategoriSparepart'));
    }

    //tampil tambah KategoriSparepart
    public function tambah()
    {
        return view('Admin.sparepart.kategori.tambah');
    }

    //proses tambah KategoriSparepart
    public function simpan(Request $request)
    {
        try {
            $KategoriSparepart = new KategoriSparepart();
            $KategoriSparepart->nama_kategori = $request->nama_kategori;
            $KategoriSparepart->save();

            return redirect()->route('kategori.sparepart.list')->with('status', 'Berhasil Menambahkan Data Kategori Sparepart');
        } catch (Throwable $e) {
            return redirect()->route('kategori.sparepart.list')->with('status', 'Berhasil Menambahkan Data Kategori Sparepart');
        }
    }

    //edit data
    public function edit($id){
        $KategoriSparepart = DB::table('kategori')->where('id', $id)->first();
        return view('Admin.sparepart.kategori.edit', compact('KategoriSparepart'));

    }

    //update
    public function update(Request $request, $id)
    {
        $KategoriSparepart = KategoriSparepart::find($request->id);
        $KategoriSparepart->nama_kategori = $request->nama_kategori;
        $KategoriSparepart->save();
        return redirect('kategorisparepart/list')->with('status2', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('kategori')->where('id', $id)->delete();

        return redirect('kategorisparepart/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
