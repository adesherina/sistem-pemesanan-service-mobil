<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\AdminLogin;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    //tampil login
    public function index()
    {
        return view('login');
    }
    //tampil Dashboard
    public function dashboard()
    {
        return view('Admin.dashboard');
    }

    //Login
    public function login(Request $request){
        // dd($request->all());

        $data = AdminLogin::where('username', $request->username)->first();
        // dd($data);
        if (!$data){
            return redirect('admin/login')->with('message1', 'username salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('name',$data->nama_lengkap);
                Session::put('id',$data->id);
                Session::put('role', $data->role);
            

                session(['berhasil_login' => true]);
                return redirect('admin/dashboard');
            }
            return redirect('admin/login')->with('message2', 'password salah');
        }
        
    }

    //Logout
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('admin/login');
    }
}
