<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    
    //tampil daftar user
    public function index()
    {
        $users = DB::table('users')->get();
        return view('Admin.user.index', compact('users'));
    }

    //Delete Data User
    public function delete($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect('user/list')->with('status3', 'Data Berhasil Dihapus');
    }

    //Tampil Detail Data User
    public function detail($id){
        $user = DB::table('users')->where('id', $id)->first();
        return view('Admin.user.detail', compact('user'));
    }

}
