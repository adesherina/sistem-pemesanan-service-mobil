<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Model\KategoriSparepart;
use App\Model\MerkSparepart;
use App\Model\Sparepart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Throwable;



class SparepartController extends Controller
{
    //tampil data
    public function index()
    {
        $sparepart = Sparepart::with('kategoris')
                    ->with('Merks')->with('jasaRef')->get();
        return view('Admin.sparepart.index', compact('sparepart'));
    }

    //tambah data
    public function tambah()
    {
        $merk = MerkSparepart::all();
        $kategori = KategoriSparepart::all();
        return view('Admin.sparepart.tambah', compact('kategori', 'merk'));
    }

    //proses tambah data
    public function simpan(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'merk_id' => 'required',
            'kategori_id' => 'required',
            'jasa_id' => 'required',
            'stok' => 'required',
            'harga_jual' => 'required',
            'harga_beli' => 'required',
        ];

        $messages = [
            'nama.required'  => 'Nama Sparepart  wajib diisi',
            'merk_id.required'  => 'Merek wajib diisi',
            'kategori_id.required'         => 'Kategori wajib diisi',
            'jasa_id.required'         => 'Jasa wajib diisi',
            'stok.required'      => 'Stok wajib diisi',
            'harga_jual.required'           => 'Harga Jual wajib diisi',
            'harga_beli.required'  => 'Harga Beli  wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try {
            if ($request->hasFile('photo')) {
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() . "/public/foto-sparepart", $name);
                $Sparepart = new Sparepart();
                $Sparepart->nama = $request->nama;
                $Sparepart->merk_id = $request->merk_id;
                $Sparepart->kategori_id = $request->kategori_id;
                $Sparepart->jasa_id = $request->jasa_id;
                $Sparepart->stok = $request->stok;
                $Sparepart->harga_jual = $request->harga_jual;
                $Sparepart->harga_beli = $request->harga_beli;
                $Sparepart->photo = $name;
                $Sparepart->save();
             echo "Foto berhasil di upload";
                return redirect()->route('Sparepart.list')->with('status', 'Berhasil Menambahkan Sparepart');
            } else {
                echo "Gagal upload Foto";
                return redirect()->route('Sparepart.list')->with('status2', 'Gagal Menambahkan Sparepart');
            }
        } catch (Throwable $e) {
        }
    }

    //tampil edit sparepart
    public function edit($id)
    {
        $sparepart = Sparepart::where('id', $id)->first();
        return view('Admin.sparepart.edit', compact('sparepart'));
    }

    //proses update data
    public function update(Request $request, $id)
    {
        if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/foto-sparepart", $name);
            DB::table('sparepart')->where('id', $id)->update(
                [
                    'nama' => $request->nama,
                    'merk_id' => $request->merk_id,
                    'kategori_id' => $request->kategori_id,
                    'jasa_id' => $request->jasa_id,
                    'stok' => $request->stok,
                    'harga_jual' => $request->harga_jual,
                    'harga_beli' => $request->harga_beli,
                    'photo' => $name
                ]);
            return redirect()->route('Sparepart.list', $id)->with('status', 'Data Berhasil Di Update');
        }else{
             DB::table('sparepart')->where('id', $id )->update(
                [
                    'nama' => $request->nama,
                    'merk_id' => $request->merk_id,
                    'kategori_id' => $request->kategori_id,
                    'jasa_id' => $request->jasa_id,
                    'stok' => $request->stok,
                    'harga_jual' => $request->harga_jual,
                    'harga_beli' => $request->harga_beli,
                    'photo' => $request->oldPhoto
                ]);
            return redirect()->route('Sparepart.list', $id) ->with('status', 'Data Berhasil Di Update');
        }
    }


    //tampil detail sparepart
    public function detail($id)
    {
        $sparepart = Sparepart::with('kategoris')
                    ->with('Merks')->with('jasaRef')->where('id', $id)->first();
        // dd($sparepart);
            return view('Admin.sparepart.detail', compact('sparepart'));
    }

    //hapus data
    public function delete($id)
    {
        DB::table('sparepart')->where('id', $id)->delete();

        return redirect('sparepart/list')->with('status3', 'Data Berhasil Dihapus');
    }
}
