<?php

namespace App\Http\Controllers\super_admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    //tampil Register
    public function index()
    {
        return view('super_admin.register');
    }
    //tampil Dashboard
    public function dashboard()
    {
        return view('super_admin.dashboard');
    }
}
