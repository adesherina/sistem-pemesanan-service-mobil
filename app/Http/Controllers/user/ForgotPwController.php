<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ForgotPwController extends Controller
{
    //tampil cari nomor telepon
    public function index()
    {
        return view('User.forgotpw.index');
    }

    public function cariTelepon(Request $request)
    {
        $user = User::whereno_telp($request->no_telp)->first();

        if ($user != null){
            return view('User.forgotpw.forgot', ['no_telp' =>$request->no_telp])->with('status', 'Berhasil');
        } else {
            return view('User.forgotpw.index')->with('failed', 'Nomor Telepon Tidak Terdaftar');
        }
    }

    //tampil cari nomor telepon
    public function forgotpw(Request $request)
    {
        $newPassword = $request->newPassword;
        $no_telp = $request->no_telp;
        $confirmPassword = $request->confirmPassword;
        //    dd($newPassword, $confirmPassword);
        if ($newPassword === $confirmPassword){
            $change = DB::table('users')
            ->where('no_telp', $no_telp)
            ->update(['password' => Hash::make($newPassword)]);
            
            return redirect('/login')->with('success', 'Berhasil Mengganti Password');
        } elseif($newPassword != $confirmPassword){

            return view('User.forgotpw.forgot', ['no_telp'=>$no_telp])->with('failedPass', 'Password Konfirmasi Tidak Sama');
        }
    }
    //     return view('User.forgotpw.forgot');
    // }
}
