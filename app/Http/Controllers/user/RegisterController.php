<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Twilio\Rest\Client;
use App\User;


class RegisterController extends Controller
{
    //tampil register
    public function index()
    {
        return view('User.register');
    }

    //proses register
    public function addProcess(Request $request)
    {
        $rules = [
            'username' => 'required|min:5',
            'nama_lengkap' => 'required|min:4|max:100',
            'no_telp' => 'required|min:10|unique:users',
            'password' => 'required|min:8',
            'confirmPassword' => 'required',
        ];
 
        $messages = [
            'username.required'  => 'Username wajib diisi.',
            'username.min' => 'Username minimal 5',
            'nama_lengkap.required'  => 'Nama Lengkap wajib diisi.',
            'nama_lengkap.min' => 'Nama Lengkap minimal 4',
            'no_telp.required'         => 'Nomor telepon wajib diisi.',
            'no_telp.unique'           => 'Nomor telepon sudah terdaftar.',
            'password.required'      => 'Password wajib diisi.',
            'password.min'           => 'Password minimal diisi dengan 8 karakter.',
            'confirmPassword.required'  => 'Password Konfirmasi wajib diisi.',
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
         
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        $no_telp  = $request->no_telp;
        // kadang ada penulisan no hp 0811 239 345
        $no_telp = str_replace(" ","",$no_telp);
        // kadang ada penulisan no hp (0274) 778787
        $no_telp = str_replace("(","",$no_telp);
        // kadang ada penulisan no hp (0274) 778787
        $no_telp = str_replace(")","",$no_telp);
        // kadang ada penulisan no hp 0811.239.345
        $no_telp = str_replace(".","",$no_telp);

        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($no_telp))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($no_telp), 0, 3)=='+62'){
                $hp = trim($no_telp);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($no_telp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($no_telp), 1);
            }
        }
    

        $timestamps = true;
        //query builder insert
        if($request->password != $request->confirmPassword){
             return redirect('/register')->with('statusFailed', 'Password Konfirmasi Tidak Sama');
        }else{
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
        $twilio->verify->v2->services($twilio_verify_sid)
            ->verifications
            ->create($hp, "sms");
        // dd($request->all());
        $user = new User();
        $user->nama_lengkap = $request->nama_lengkap;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->no_telp = $request->no_telp;
        $user->is_verified = 0;
        $user->save();
        
       
        // Redirect dengan status 
        // dd($request->all());
        return redirect()->route('otp', $no_telp)->with('statusRegister', 'Register berhasil');
        }
    }
}
