<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\{TmpService, Antrian, DetailService, Kendaraan, Pembayaran, Services, Sparepart};
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;

class CheckoutController extends Controller
{
    //tampil checkout
    public function index($kd_service)
    {
        $tmp = TmpService::findOrFail($kd_service);
        $kendaraan = Kendaraan::find($tmp->kendaraan_id);
        $user = User::where('id', Session::get('id'))->first();
        return view('User.checkout.index', compact('tmp','kendaraan','kd_service','user'));
    }

    public function save(Request $request, $kd_service){
        $tmp = TmpService::find($kd_service);
        $total = 0;

        try{
        $no_telp = User::find($tmp->user_id)->no_telp;

        $service = new Services();
        $service->kd_service = $kd_service;
        $service->user_id = $tmp->user_id;
        $service->kendaraan_id = $tmp->kendaraan_id;
        $service->status = "0";
        $service->nama_lengkap = $request->nama;
        $service->no_hp = $request->no_hp;
        $service->catatan = $request->catatan;
        $service->save();

        $values = json_decode($tmp->value);

        // Nambah jasa/sparepart ke detail
        foreach($values as $value){
            $parseValue = json_decode($value);

            if($parseValue->sparepart_id != "null"){
                $detail = new DetailService();
                $detail->kd_service = $kd_service;
                $detail->sparepart_id = $parseValue->sparepart_id;
                $detail->jasa_id = $parseValue->id;
                $detail->subtotal = $parseValue->harga_jasa;

                $sp = Sparepart::find($parseValue->sparepart_id);
                if($sp){
                    Sparepart::where('id', $sp->id)->update([
                        'stok' => (int)$sp->stok - 1
                    ]);
                }
            }else{
                $detail = new DetailService();
                $detail->kd_service = $kd_service;
                $detail->jasa_id = $parseValue->id;
                $detail->subtotal = $parseValue->harga_jasa;
            }
            $detail->save(); 
            
            $total += (int)$parseValue->harga_jasa;
        }

        // ============= antrian ==================
        $last = Antrian::where('tanggal', $request->tanggal)->latest('no_antrian')->first();
            if($last){
                $antrian = new Antrian();
                $antrian->kd_service = $kd_service;
                $antrian->no_antrian = (int)$last->no_antrian + 1;
                $antrian->status = "Menunggu";
                $antrian->tanggal = $request->tanggal;
                $antrian->save();
            }else{
                $antrian = new Antrian();
                $antrian->kd_service = $kd_service;
                $antrian->no_antrian = 1;
                $antrian->status = "Menunggu";
                $antrian->tanggal = $request->tanggal;
                $antrian->save();
            }

            // Pembayaran
            $latest = Pembayaran::latest()->first();

            if (!$latest) {
                $nota = 'TS-0001';
            }else{
                $string = preg_replace("/[^0-9\.]/", '', $latest->nota);

                $nota = 'TS-' . sprintf('%04d', $string+1);
            }

            $bayar = new Pembayaran();
            $bayar->nota = $nota;
            $bayar->kd_service = $kd_service;
            $bayar->antrian_id = $antrian->id;
            $bayar->status = 0;
            $bayar->total = $total;
            $bayar->save();

        $this->sendMessage($no_telp, $kd_service,$request->tanggal, $antrian->no_antrian );
        
        return redirect()->route('services')->with('success','Berbasil memesan service');
        }catch(\Throwable $err){
            dd($err);        
        }

    }


    //tampil service aktif
    public function servicesaktif()
    {
        return view('User.checkout.serviceaktif');
    }

    //tampil detail
    public function detail($kd_service)
    {
        $detail = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
                    ->where('kd_service', $kd_service)->first();
        
        return view('User.checkout.detail', compact('detail'));
    }

    //hapus data
    // public function delete($kd_service)
    // {
    //     Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
    //     ->where('kd_service', $kd_service)->delete();

    //     return redirect()->route('checkout')->with('status3', 'Data Berhasil Dihapus');
    // }

    //hapus service
    public function delete(Request $req, $kd_service)
    {   
        try{
            Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
            ->where('kd_service', $kd_service)->delete();

            $success['status'] = 1;
            $success['message'] = 'Berhasil dibatalkan';
            return response()->json($success, 200);
        }catch(\Exception $e){
            $success['status'] = 0;
            $success['message'] = $e->getMessage();
            return response()->json($success, 500);
        }
    
    }

    //tampil invoice
    public function invoice($kd_service)
    {
        $invoice = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
                    ->where('kd_service', $kd_service)->first();
        return view('User.checkout.invoice', compact('invoice'));
    }

    //cetak invoice
    public function cetakInvoice($kd_service)
        {
            $invoice = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
                    ->where('kd_service', $kd_service)->first();

            $pdf = PDF::loadview('User/checkout/cetakInvoice', compact('invoice'))->setPaper('A4','landscape');
            return $pdf->stream('invoice.pdf');
        }

    //cek nomor antrian
    public function getAntrian(Request $request){
        $last = Antrian::where('tanggal', $request->tanggal)->latest('no_antrian')->first();
        if($last){
            $success['status'] = 200;
            $success['antrian'] = (int)$last->no_antrian + 1;
        }else{
            $success['status'] = 200;
            $success['antrian'] = 1;
        }

        return response()->json($success);
    }

    //Kirim Sms saat antrian 3 lagi
    public function sendMessage($no_telp, $kd_service, $tgl, $antrian){
        $tgl = Carbon::parse($tgl)->translatedFormat('d F Y');
        $msg = "Pelanggan Terhormat Bengkel Tiga Saudara,\nKode Service Anda ". $kd_service ." pada tanggal ". $tgl." dengan antrian ".$antrian;
        $userkey = 'e8c9bd9e4f8a';
        $passkey = '7a7b201077486051653c5ada';
        $telepon = $no_telp;
        $message = $msg;
        $url = 'https://console.zenziva.net/reguler/api/sendsms/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => $userkey,
            'passkey' => $passkey,
            'to' => $telepon,
            'message' => $message
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }
}
