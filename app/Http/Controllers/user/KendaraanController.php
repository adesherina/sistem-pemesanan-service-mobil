<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Kendaraan;
use App\Model\ModelKendaraan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Throwable;


class KendaraanController extends Controller
{
    //tampil Garasi
    public function index()
    {
        $kendaraan = Kendaraan::with('models')
                    ->with('merks')->with('userRef')
                    ->where('user_id', Session::get('id'))->get();
        return view('User.garasi.index', compact('kendaraan'));
    }
    
    //tampil Tambah garasi
    public function tambah()
    {
        return view('User.garasi.tambah');
    }

    //proses tambah data mobil
    public function simpan(Request $request)
    {
        $rules = [
            'merk_id' => 'required',
            'model_id' => 'required',
            'tahun_keluar' => 'required',
            'plat_nomor' => 'required',
        ];

        $messages = [
            'merk_id.required'  => 'Merek Mobil wajib diisi',
            'model_id.required'         => 'Model Mobil wajib diisi',
            'tahun_keluar.required'      => 'Tahun Keluar wajib diisi',
            'plat_nomor.required'           => 'Plat Nomor wajib diisi',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }

        try {
                $kendaraan = new Kendaraan();
                $kendaraan->user_id = Session::get('id') ;
                $kendaraan->merk_id = $request->merk_id;
                $kendaraan->model_id = $request->model_id;
                $kendaraan->tahun_keluar = $request->tahun_keluar;
                $kendaraan->plat_nomor = $request->plat_nomor;
                $kendaraan->save();

               
            return redirect()->route('garasi')->with('status', 'Berhasil Menambahkan Data Mobil Kendaraan');
        } catch (Throwable $e) {
            return redirect()->route('garasi')->with('status', 'Berhasil Menambahkan Data Mobil Kendaraan');
        }

        // dd($request->all());
    }

    //tampil detail garasi
    public function detail($id)
    {
        $kendaraan = Kendaraan::with('models')
                    ->with('merks')->with('userRef')
                    ->where('id', $id)->first();
        return view('User.garasi.detail', compact('kendaraan'));
    }

    //proses update data
    public function update(Request $request, $id)
    {
        $kendaraan = Kendaraan::find($request->id);
        $kendaraan->kilometer = $request->kilometer;
        $kendaraan->oli_mesin = $request->oli_mesin;
        $kendaraan->bahan_bakar = $request->bahan_bakar;
        // $kendaraan->transmisi = $request->transmisi;
        $kendaraan->save();
        
        return view('User.garasi.detail', compact('kendaraan'))->with('status2', 'Data Berhasil Diedit');
    }

    //hapus data
    public function delete($id)
    {
        DB::table('kendaraan')->where('id', $id)->delete();

        return redirect('garasi')->with('status3', 'Data Berhasil Dihapus');
    }

    public function getModel(Request $request){
            $model = ModelKendaraan::where('merk_id', $request->merk_id)->get()->toJson();
            $decode = json_decode($model);
            if(count($decode) > 0){
                $success['status'] = 200;
                $success['data'] = $model;
            }else{
                 $success['status'] = 404;
            }
            
            return response()->json($success);
    }
}
