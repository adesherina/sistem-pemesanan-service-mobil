<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\User;
use Carbon\Carbon;

class OTPController extends Controller
{
    //tampil Kode OTP
    public function index($no_telp)
    {
        return view('User.otp.index',compact('no_telp'));
    }

    public function verifyOtp(Request $request){
        $data = $request->validate([
            'kodeotp' => ['required', 'numeric'],
            'no_telp' => ['required', 'string'],
        ]);

        $nohp = $request->no_telp;
        // kadang ada penulisan no hp 0811 239 345
        $nohp = str_replace(" ","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace("(","",$nohp);
        // kadang ada penulisan no hp (0274) 778787
        $nohp = str_replace(")","",$nohp);
        // kadang ada penulisan no hp 0811.239.345
        $nohp = str_replace(".","",$nohp);

        // cek apakah no hp mengandung karakter + dan 0-9
        if(!preg_match('/[^+0-9]/',trim($nohp))){
            // cek apakah no hp karakter 1-3 adalah +62
            if(substr(trim($nohp), 0, 3)=='+62'){
                $hp = trim($nohp);
            }
            // cek apakah no hp karakter 1 adalah 0
            elseif(substr(trim($nohp), 0, 1)=='0'){
                $hp = '+62'.substr(trim($nohp), 1);
            }
        }
        $token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_sid = getenv("TWILIO_SID");
        $twilio_verify_sid = getenv("TWILIO_VERIFY_SID");
        $twilio = new Client($twilio_sid, $token);
         $verification = $twilio->verify->v2->services($twilio_verify_sid)
            ->verificationChecks
            ->create($request->kodeotp, array('to' => $hp));
        // is verification valid
        if (!$verification->valid) {
            return redirect()->route('otp', $request->no_telp)->with('failed', 'Password Konfirmasi Tidak Sama');
        }

        $user = User::where('no_telp', $request->no_telp)->update(
                [
                    'is_verified' => 1,
                    'verified_at' => Carbon::now()->toDateTimeString(),
                ]
            );

        return redirect()->route('user.login')->with('success', 'Password Konfirmasi Tidak Sama');
    }
}
