<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


class ProfilController extends Controller
{
    //tampil profil
    public function index($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        // dd($user);

        return view('User.profil.index', compact('user'));
    }

    //simpan update profil
    public function updateProcess(Request $request, $id){
        if($request->hasFile('photo')){
            $resorce = $request->file('photo');
            $name   = $resorce->getClientOriginalName();
            $resorce->move(\base_path() ."/public/user-profil/", $name);
            DB::table('users')->where('id', Session::get('id') )->update(
                [
                    'nama_lengkap' => $request->nama_lengkap,
                    'no_telp'=>$request->no_telp,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $name
                ]);
            return redirect()->route('user.profil', $id)->with('statusSuccess1', 'Data Berhasil Di Update');
        }else{
             DB::table('users')->where('id', Session::get('id') )->update(
                [
                    'nama_lengkap' => $request->nama_lengkap,
                    'no_telp'=>$request->no_telp,
                    'jenis_kelamin' => $request->jenis_kelamin,
                    'alamat' => $request->alamat,
                    'photo' => $request->oldPhoto
                ]);
            return redirect()->route('user.profil', $id) ->with('statusSuccess2', 'Data Berhasil Di Update');
        }   
    }

    //tampil update Password
    public function updatePw($id){
        $user = DB::table('users')->where('id', $id)->first();
        return view('User.profil.gantipw', compact('user'));

    }

    //Update password
    public function updatePassword(Request $request, $id)
    {
        
        $newPassword = $request->newPassword;
        if ($newPassword === $request->confirmPassword) {
            DB::table('users')->where('id', $id)->update([
                'password' => Hash::make($request->newPassword),
            ]);
            return redirect()->back()->with("editpw", "Berhasil Ganti Password");
        } else {
           return redirect()->back()->with("failedpw", "Gagal Ganti Password");
        }
    }
}
