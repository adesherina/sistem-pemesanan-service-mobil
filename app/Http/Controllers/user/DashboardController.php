<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\Antrian;
use App\Model\Kendaraan;
use App\Model\Services;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;




class DashboardController extends Controller
{
    //tampil tentang dashboard
    public function index()
    {
        $service = Services::with('detailServiceRef', 'kendaraanRef', 'antrianRef')
                    ->where('status', 2)->where('user_id', Session::get('id'))->get();

        $antrian = Antrian::with('serviceRef')
                ->where('tanggal', Carbon::today())
                ->where('status','Proses')->orderBy('no_antrian', 'DESC')->first();

        $kendaraan = Kendaraan::with('models')
                    ->with('merks')->with('userRef')
                    ->where('user_id', Session::get('id'))->get();

        return view('User.dashboard', compact('kendaraan', 'service', 'antrian'));
    }
}
