<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use SN;
use App\Model\{DetailService, Jasa, Services, KategoriJasa, User, Kendaraan, Sparepart, TmpService};
use Throwable;

class ServiceController extends Controller
{
    //tampil service
    public function index()
    {
        $service = Services::with('detailServiceRef')
                    ->where('user_id', Session::get('id'))
                    ->where('status', 1) ->get();
        $riwayat = Services::with('detailServiceRef')
                    ->where('user_id', Session::get('id'))
                    ->get();
        return view('User.service.index', compact('service', 'riwayat'));
    }

    public function getService(Request $request){
        $kategori_id = $request->kategori_id;

        $services = Jasa::where('kategori_id', $kategori_id)->where('status','1')->get();
        foreach($services as $service){
            $sparepart = Sparepart::where('jasa_id', $service->id)->first();
            if($sparepart){
                $service->is_sparepart = 1;
            }else{
                 $service->is_sparepart = 0;
            }
        }
        
        // $decode = json_decode($services);
            if(count($services) > 0){
                $success['status'] = 200;
                $success['data'] = json_encode($services);
            }else{
                 $success['status'] = 404;
            }
            
        return response()->json($success);

    }

    public function getSparepart(Request $request){
        $jasa_id = $request->jasa_id;
        $spareparts = Sparepart::with('jasaRef')->where('jasa_id', $jasa_id)->get()->toJson();

            $decode = json_decode($spareparts);
            if(count($decode) > 0){
                $success['status'] = 200;
                $success['data'] = $spareparts;
            }else{
                 $success['status'] = 404;
            }
            
        return response()->json($success);

    }

    //tampil tambah
    public function tambah()
    {
        $kategori_jasa = KategoriJasa::get();
        $jasas = DB::table('jasa')->take(5)->get();
        $service = Services::with('kendaraanRef', 'userRef')->get();

        return view('User.service.tambah', compact('jasas','kategori_jasa', 'service'));
    }

    
    //tambah service
    public function save(Request $request){
        // dd(json_decode($request->value));
        $kd_service = SN::generate();
        $service = new TmpService();
        $service->kd_service = $kd_service;
        $service->user_id = Session::get('id');
        $service->kendaraan_id = $request->kendaraan;
        $service->value = $request->value;
        $service->save();

        return redirect()->route('checkout', $kd_service);
       
    }
    
}
