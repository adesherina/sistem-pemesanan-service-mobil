<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\UserLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    //tampil login
    public function index()
    {
        // dd($request->all());

      return view('User.login');
    }
    
    public function login (Request $request)
    {
          $data = UserLogin::where('username', $request->username)->first();
        // dd($data);
        if (!$data){
            return redirect()->route('user.login')->with('message1', 'username salah');
        }else{
            if(Hash::check($request->password, $data->password)){
                Session::put('name',$data->nama_lengkap);
                Session::put('id',$data->id);
            

                session(['berhasil_login' => true]);
                return redirect('dashboard');
            }
            return redirect()->route('user.login')->with('message2', 'password salah');
        }
    }

    //Logout
    public function logout(Request $request){
        $request->session()->flush();
        return redirect()->route('user.login');
    }

    
}
