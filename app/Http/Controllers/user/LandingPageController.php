<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Model\KritikSaran;
use Illuminate\Http\Request;
use Throwable;


class LandingPageController extends Controller
{
    //tampil index
    public function index()
    {
        return view('User.LandingPage.index');
    }

    //tampil tentang kami
    public function about()
    {
        $ks = KritikSaran::all();
        return view('User.LandingPage.tentangKami', compact('ks'));
    }

    //tampil service
    public function service()
    {
        return view('User.LandingPage.services');
    }

    //tampil kontak
    public function kontak()
    {
        return view('User.LandingPage.kontak');
    }

    //kritik saran tambah
    public function ks(Request $request)
    {
        try {
            $ks = new KritikSaran();
            $ks->nama_lengkap = $request->nama_lengkap;
            $ks->no_telp = $request->no_telp;
            $ks->ks = $request->ks;
            $ks->save();

            return redirect()->route('user.landing.kontak')->with('status', 'Berhasil Mengirim Kritik Saran');
        } catch (Throwable $e) {
            return redirect()->route('user.landing.kontak')->with('status', 'Berhasil Mengirim Kritik Saran');
        }
    }

}
