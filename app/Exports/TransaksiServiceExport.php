<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Model\Pembayaran;

class TransaksiServiceExport implements FromView
{
    public $from;
    public $to;

    public function __construct($from, $to)
    {
        $this->from = $from;
        $this->to = $to;
    }

     public function view(): View
    {
        $dari = date('Y-m-d',strtotime($this->from));
        $sampai = date('Y-m-d',strtotime($this->to));
        
        if($this->from){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,\Carbon\Carbon::now()])->get();
        }elseif($this->from && $this->to){
            $ts = Pembayaran::with('serviceRef','antrianRef')->whereBetween('created_at',[$dari,$sampai])->get();
        }else{
            $ts = Pembayaran::with('serviceRef','antrianRef')->get();
        }
        return view('Admin.exports.excel.transaksi_export', [
            'ts' => $ts
        ]);
    }
    
}
