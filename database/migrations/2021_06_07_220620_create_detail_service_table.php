<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_service', function (Blueprint $table) {
            $table->id();
            $table->string('kd_service', 15);
            $table->unsignedBigInteger('sparepart_id');
            $table->unsignedBigInteger('jasa_id');
            $table->string('keluhan', 225);
            $table->integer('subtotal');
            $table->integer('jumlah');
            $table->timestamps();

            $table->foreign('kd_service')->references('kd_service')->on('service')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('sparepart_id')->references('id')->on('sparepart')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('jasa_id')->references('id')->on('jasa')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_service');
    }
}
