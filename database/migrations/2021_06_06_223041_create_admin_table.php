<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->id();
            $table->string('no_telp', 15);
            $table->string('code')->nullable();
            $table->string('nama_lengkap', 50);
            $table->text('alamat')->nullable();
            $table->string('jenis_kelamin', 10)->nullable();
            $table->string('photo', 255)->nullable();
            $table->string('username', 255);
            $table->string('password', 255);
            $table->boolean('is_verified');
            $table->timestamp('verified', 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
