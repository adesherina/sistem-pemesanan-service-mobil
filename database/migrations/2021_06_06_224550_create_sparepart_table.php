<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSparepartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sparepart', function (Blueprint $table) {
            $table->id();
            $table->string('nama',255);
            $table->unsignedBigInteger('merk_id');
            $table->unsignedBigInteger('kategori_id');
            $table->integer('stok');
            $table->integer('harga_jual');
            $table->integer('harga_beli');
            $table->string('photo',255);
            $table->timestamps();

            $table->foreign('merk_id')->references('id')->on('merk')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kategori_id')->references('id')->on('kategori')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sparepart');
    }
}
