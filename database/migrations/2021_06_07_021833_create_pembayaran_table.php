<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePembayaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pembayaran', function (Blueprint $table) {
            $table->string('nota', 20)->primary();
            $table->string('kd_service', 15);
            $table->unsignedBigInteger('antrian_id');
            $table->date('tanggal_transaksi', 225);
            $table->tinyInteger('status');
            $table->integer('total');
            $table->timestamps();

            $table->foreign('kd_service')->references('kd_service')->on('service')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('antrian_id')->references('id')->on('antrian')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pembayaran');
    }
}
