<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJasaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jasa', function (Blueprint $table) {
            $table->id();
            $table->string('nama_jasa', 50);
            $table->integer('harga_jasa');
            $table->unsignedBigInteger('kategori_id');
            $table->tinyInteger('status')->nullable();
            $table->string('estimasi', 50);
            $table->timestamps();

             $table->foreign('kategori_id')->references('id')->on('kategori_jasa')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jasa');
    }
}
