<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKendaraanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kendaraan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('merk_id');
            $table->unsignedBigInteger('model_id');
            $table->date('tahun_keluar');
            $table->string('plat_nomor', 10)->nullable();
            $table->string('kilometer', 50)->nullable();
            $table->string('transmisi', 50)->nullable();
            $table->string('bahan_bakar', 50)->nullable();
            $table->string('oli_mesin', 50)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('merk_id')->references('id')->on('merk_kendaraan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('model_id')->references('id')->on('model_kendaraan')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kendaraan');
    }
}
