// A $( document ).ready() block.
// $(document).ready(function () {
//     console.log("ready!");
// });
$.ajaxSetup({
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
});

var arrService = [];
var index = 0;

$("#user-select").on("change", function () {
    let user_id = $(this).val();
    getCar(user_id);
});

// $(document).ready(function () {
//     $("#nama-pelanggan").select2();
// });

$(".jasa-select").each(function () {
    $(this).on("change", function () {
        let jasa_id = $(this).val();
        let index_id = $(this).data("id");

        getByJasa(jasa_id, index_id);
    });
});

const removeIndexService = (jasa_id) => {
    arrService.forEach((service, index) => {
        if (service.jasa_id == jasa_id) {
            if (index > -1) {
                array.splice(index, 1);
            }
        }
    });
};

const getCar = (user_id) => {
    var url = "/admin/service/user/getcar";

    $.ajax({
        url: url,
        type: "POST",
        data: {
            user_id: user_id,
        },
        success: function (responses) {
            var options = $("#kendaraan-select");
            console.log(responses);
            options.empty();
            if (responses.status == 1) {
                $.each(responses.data, function () {
                    options.append(
                        $("<option />")
                            .val(this.id)
                            .text(this.models.nama_model)
                    );
                });
            } else {
                options.append($("<option />").text("Pilih Kendaraan"));
            }
        },
        error: function (err) {
            var options = $("#kendaraan-select");
            options.empty();
            options.append($("<option />").text("Pilih Kendaraan"));
        },
    });
};

const addService = () => {
    event.preventDefault();
    index = index + 1;
    var jasas = getAllService();
};

const getAllService = () => {
    var url = "/admin/service/user/getalljasa";

    $.ajax({
        url: url,
        type: "GET",
        success: function (responses) {
            renderServiceItem(responses.data);
        },
        error: function (err) {
            console.log(err);
        },
    });
};

const renderServiceItem = (services) => {
    let html = `
        <div class="form-group row">
            <label class="">Layanan</label>
            <select name="jasa_id" id="jasa-select-${index}" data-id="${index}" class="form-control jasa-select">
                <option selected>Pilih Layanan</option>
            </select>
        </div>
        <div class="form-group row">
            <label class="">Sparepart</label>
            <select name="sparepart_id" id="sparepart-select-${index}" data-id="${index}" class="sparepart-select form-control">
                <option selected>Pilih Sparepart</option>
            </select>
        </div>
        <hr class="text-primary" style="border-color:#69a1fe">
    `;

    $(".service-container").append(html);
    $("#jasa-select-" + index).empty();
    $.each(services, function () {
        $("#jasa-select-" + index).append(
            $("<option />").val(this.id).text(this.nama_jasa)
        );
    });

    changeRefresh();
};

// untuk mengambil jasa berdasarkan id yang nanti akan dimasukan ke arrService
const getByJasa = (jasa_id, index_id) => {
    var url = "/admin/service/user/getbyjasa";

    $.ajax({
        url: url,
        type: "POST",
        data: {
            jasa_id: jasa_id,
        },
        success: function (response) {
            var data = {
                jasa_id: response.data.id,
                harga_jasa: response.data.harga_jasa,
                sparepart_id: null,
                harga_sparepart: 0,
                subtotal: response.data.harga_jasa,
            };
            arrService.push(data);
            if (response.data.spareparts.length > 0) {
                renderSparePartOption(response.data.spareparts, index_id);
            } else {
                totalPrice();
            }
        },
        error: function (err) {
            console.log(err);
        },
    });
};

const renderSparePartOption = (spareparts, index_id) => {
    $("#sparepart-select-" + index_id).empty();
    $("#sparepart-select-" + index_id).prop("required", true);
    $("#sparepart-select-" + index_id).append(
        $("<option />").text("Pilih Sparepart")
    );
    $.each(spareparts, function () {
        $("#sparepart-select-" + index_id).append(
            $("<option />").val(this.id).text(this.nama)
        );
    });

    sparepartRefresh();
};

const changeRefresh = () => {
    $(".jasa-select").each(function () {
        $(this).on("change", function () {
            let jasa_id = $(this).val();
            let index_id = $(this).data("id");

            getByJasa(jasa_id, index_id);
        });
    });
};

const sparepartRefresh = () => {
    $(".sparepart-select").each(function () {
        $(this).on("change", function () {
            let sparepart_id = $(this).val();
            var url = "/admin/service/user/getbysparepart";

            $.ajax({
                url: url,
                type: "POST",
                data: {
                    sparepart_id: sparepart_id,
                },
                success: function (response) {
                    if (response.code == 200) {
                        let sparepart = response.data;
                        addSparepartToArr(sparepart);
                    }
                },
                error: function (err) {
                    console.log(err);
                },
            });
        });
    });
};

const addSparepartToArr = (sparepart) => {
    let jasa_id = sparepart.jasa_ref.id;

    arrService.forEach((service, index) => {
        if (service.jasa_id == jasa_id) {
            service.sparepart_id = sparepart.id;
            service.harga_sparepart = sparepart.harga_jual;
            service.subtotal =
                parseInt(sparepart.harga_jual) + parseInt(service.subtotal);
        }
    });

    totalPrice();
};

const totalPrice = () => {
    let total = 0;
    arrService.forEach((service, index) => {
        console.log(service);
        total += parseInt(service.subtotal);
    });
    $("#total-text").text(total);
};

const submitForm = () => {
    const url = "/admin/service/save";
    let customer_id = $("#user-select").val();
    let car_id = $("#kendaraan-select").val();
    let date = $("#tanggal-inp").val();
    let phone = $("#no-telp-inp").val();
    let note = $("#catatan-inp").val();
    let datas = {
        customer_id: customer_id,
        car_id: car_id,
        date: date,
        phone: phone,
        note: note,
        services: arrService,
    };

    $.ajax({
        url: url,
        type: "POST",
        data: {
            datas: datas,
        },
        success: function (response) {
            console.log(response);
            // if (response.code == 200) {
            //     let sparepart = response.data;
            //     addSparepartToArr(sparepart);
            // }
        },
        error: function (err) {
            console.log(err);
        },
    });
};
